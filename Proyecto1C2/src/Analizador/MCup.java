package Analizador;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LuisAroldo
 */
public class MCup {
    public static void main(String[] args) {
        String[] opciones = new String[7];
        opciones[0] = "-destdir";
        opciones[1] = "src\\Analizador\\";
        opciones[2] = "-parser";
        opciones[3] = "parser";
        opciones[4] = "-symbols";
        opciones[5] = "Simbolos";        
        opciones[6] = "src\\Analizador\\Sintactico.cup";
                
        try{
            java_cup.Main.main(opciones);
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }

    }    
}
