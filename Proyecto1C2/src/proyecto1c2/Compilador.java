/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proyecto1c2;

import Analizador.parser;
import Analizador.scanner;
import Ejecucion.*;
import LSR.NodoS;
import LSR.Sintactico;
import LSR.TAS2;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LuisAroldo
 */
public class Compilador {
    
    
    //<editor-fold desc="Constructor">
    
    public Compilador(){   
        
    }
    
    public void iniciar(){
        this.areadeclaraciones = null;
        this.areagramatica = null;
        this.areacodigo = null;
        this.acciones = "";
    }
    
    //</editor-fold>
    
    
    //<editor-fold desc="Atributos de la Clase">
    
    public Gramatica gramatica;
    
    public static String acciones;
    
    //Lista de Errores
    public static ArrayList<Errores> listaErrores;
    
    //Nodos de areas
    public Nodo areadeclaraciones, areagramatica, areacodigo;
    
    //Para guardar las variabls globales
    //public HashMap<String,Variable> listaLocal;
    
    //Para guardar los metodos y funciones
    public HashMap<String,Clases> listaClases;
    
    //para llevar el nombre de clase
    public String claseactual = "";
    
    public static Boolean tablas = false;
    
    //</editor-fold>
    
   
    // <editor-fold desc="Graficar AST">
    
    int contador; 
    String relacion="";
    public String graphviz="";
    public void graficarAst(Nodo raiz){
        relacion="";    
        contador=0; 
        graphviz="";
        graphviz+="\ndigraph G {\r\nnode [shape=doublecircle, style=filled, color=khaki1, fontcolor=black];\n";
        lNodos(raiz);
        eNodos(raiz);
        graphviz+="}";
        
        
        FileWriter archivo = null;
        @SuppressWarnings("UnusedAssignment")
        PrintWriter printwriter =null;
        try {    
            archivo = new FileWriter("C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\ast.dot");
            printwriter = new PrintWriter(archivo);    
            printwriter.println(graphviz);         
            printwriter.close();
            } catch (IOException ex) {
            }
        
        try{
            String dotPath="C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
            String fileInputPath ="C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\ast.dot";
            String fileOutputPath="C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\ast.jpg";

            String tParam = "-Tjpg";
            String tOParam = "-o";
        
            String[] cmd = new String[5];
            cmd[0] = dotPath;
            cmd[1] = tParam;
            cmd[2] = fileInputPath;
            cmd[3] = tOParam;
            cmd[4] = fileOutputPath;
                  
            Runtime rt = Runtime.getRuntime();
      
            rt.exec( cmd );
      
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
            }
    
    }
    
    public void graficarAstTer(Nodo raiz){
        relacion="";    
        contador=0; 
        graphviz="";
        graphviz+="\ndigraph G {\r\nnode [shape=doublecircle, style=filled, color=khaki1, fontcolor=black];\n";
        lNodos(raiz);
        eNodos(raiz);
        graphviz+="}";
        
        
        FileWriter archivo = null;
        @SuppressWarnings("UnusedAssignment")
        PrintWriter printwriter =null;
        try {    
            archivo = new FileWriter("C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\astter.dot");
            printwriter = new PrintWriter(archivo);    
            printwriter.println(graphviz);         
            printwriter.close();
            } catch (IOException ex) {
            }
        
        try{
            String dotPath="C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
            String fileInputPath ="C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\astter.dot";
            String fileOutputPath="C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\astter.jpg";

            String tParam = "-Tjpg";
            String tOParam = "-o";
        
            String[] cmd = new String[5];
            cmd[0] = dotPath;
            cmd[1] = tParam;
            cmd[2] = fileInputPath;
            cmd[3] = tOParam;
            cmd[4] = fileOutputPath;
                  
            Runtime rt = Runtime.getRuntime();
      
            rt.exec( cmd );
      
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
            }
    
    }
    
     public void lNodos(Nodo praiz){        
        graphviz += "node" + contador + "[label=\"" + praiz.name + "\"];\n";
        praiz.id = contador;  
        contador++;        
        for(Nodo temp:praiz.hijos){
            lNodos(temp);
        }
    } 
    
    public void eNodos(Nodo praiz){        
        for(Nodo temp:praiz.hijos){
            relacion = "\"node" + praiz.id + "\"->";
            relacion += "\"node" + temp.id + "\";";
            graphviz += relacion+"\n";
            eNodos(temp);
        }
    }
    
    public void graficarAST2(NodoS instrucciones){        
        relacion="";    contador=0; graphviz="";
        graphviz+="\ndigraph G {\r\nnode [shape=doublecircle, style=filled, color=khaki1, fontcolor=black];\n";
        listarNodos2(instrucciones);
        enlazarNodos2(instrucciones);
        graphviz+="}";
        
        String fileInputPath = "C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\"+"Arbol Derivacion.txt";
        this.creararchivo(fileInputPath,graphviz);        
        
        String fileOutputPath = "C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\ast.dot"+"Arbol Derivacion.jpg";
        String tParam = "-Tjpg";    String tOParam = "-o";        
        String[] cmd = new String[5];
        cmd[0] = "dot.exe";   cmd[1] = tParam;    cmd[2] = fileInputPath;
        cmd[3] = tOParam;   cmd[4] = fileOutputPath;
        Runtime rt = Runtime.getRuntime();

        try{
            rt.exec( cmd );
        }catch (IOException ex) 
        {Logger.getLogger(Compilador.class.getName()).log(Level.SEVERE, null, ex);}              
    }
    
    private void listarNodos2(NodoS praiz){        
        if(praiz.esterminal==true){
            if(praiz.accion == null){
                graphviz+="node"+contador+"[label=\""+praiz.name+" ("+praiz.valor.toString()+")  "+"\"];\n"; 
            }else{
                graphviz+="node"+contador+"[label=\""+praiz.name+" ("+praiz.valor.toString()+")  "+" {: "+praiz.accion.hijos.get(0).name+" :}"+"\"];\n";             
            }           
              
        }else{
            if(praiz.accion == null && praiz.hijos.get(praiz.hijos.size()-1).accion == null){
                graphviz+="node"+contador+"[label=\""+praiz.name+"\"];\n";
            }else if(praiz.hijos.get(praiz.hijos.size()-1).accion != null){
                int i = praiz.hijos.size()-1;
                graphviz+="node"+contador+"[label=\""+praiz.name+" {: "+praiz.hijos.get(i).accion.hijos.get(0).name  +" :}"+ "\"];\n";
            }else{
                    graphviz+="node"+contador+"[label=\""+praiz.name+"\"];\n";           
            }            
        }
        praiz.id=contador;  contador++;
        for(NodoS temp:praiz.hijos){
            listarNodos2(temp);
        }
    }    
    
    private void enlazarNodos2(NodoS praiz){        
        for(NodoS temp:praiz.hijos){
            relacion="\"node"+praiz.id+"\"->";
            relacion+="\"node"+temp.id+"\";";
            graphviz+=relacion+"\n";
            enlazarNodos2(temp);
        }
    }   
    
    
     //</editor-fold> 
    
    
    
    //<editor-fold desc="Otros">
    
    private void guardarError(String t, int l, int c, String ti, String d){
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        this.listaErrores.add(new Errores(t,l,c,ti,d));
        this.acciones += "" + t + l + c + ti + d + "\n";
    }
    
    //</editor-fold>


    // <editor-fold desc="Recorrer AST">
    
    // <editor-fold desc="Recorrer Cuerpo">
        
        public void AnalizarGramatica(Nodo praiz){
            
            
            this.analizarAreas(praiz);
            
            //analizar area de declaraciones
            if(this.areadeclaraciones != null){
               this.analizarAreaDeclaraciones();
               
                if(this.errorenDeclaraciones()){
                    JOptionPane.showMessageDialog(null, "Area Declaracion, existen errores","FiSintax",1);
                    this.guardarError("Declaraciones", 0, 0,"semantico", "Errores en area de Declaraciones");
                    this.acciones += "Errores en area de Declaraciones " + "\n";
                    return;
                }
               
            }
            
            //analizar area de codigo           
            if(this.areacodigo != null){
                this.analizarAreaCodigo();
                
            }
            //analizar area de gramatica
            if(this.areagramatica != null){
                this.analizarAreaGramatica();
                
            }
        }

        private void analizarAreas(Nodo nodoactual){
            switch (nodoactual.name){
                case "cuerpo":
                    for(Nodo tempo:nodoactual.hijos)
                    {this.analizarAreas(tempo);}
                    break;

                case "declaraciones":
                    this.areadeclaraciones = nodoactual;
                    break;

                case "gramatica":
                    this.areagramatica = nodoactual;
                    break;

                case "codigo":
                    this.areacodigo = nodoactual;
                    break;

                default:
                    System.out.println("! ============================================================ Default, AnalizarAreas");
                    break;
            }
        }

     //</editor-fold> 
   
        
    // <editor-fold desc="Recorrer Declaraciones">
    
    private void analizarAreaDeclaraciones(){
        this.gramatica.iniciarListas();
        this.recorridoAreaDeclaraciones(this.areadeclaraciones);
    }
    
    private void recorridoAreaDeclaraciones(Nodo nodoactual){
        switch (nodoactual.name){
            
            case "dec_terminales":
                String ruta = "C:" + nodoactual.hijos.get(0).name;
                this.AnalizarULX(ruta);   
                break;
            
            case "terminales":
                for(Nodo tempo:nodoactual.hijos)
                {this.recorridoAreaDeclaraciones(tempo);}
                break;
            
            case "terminal":
                this.capturarTerminales(nodoactual);  
                break;
                
            case "dec_Noterminales":
                this.capturarNterminales(nodoactual.hijos.get(0).hijos.get(0));   
                break;
                
            case "dec_precedencia":
                this.recorridoAreaDeclaraciones(nodoactual.hijos.get(0));   
                break;
                
            case "Precedencia":                                
                this.capturarPrecedencia(nodoactual);
                break;
                
            case "dec_asociacion":
                this.recorridoAreaDeclaraciones(nodoactual.hijos.get(0));   
                break;
                
            case "Asociatividad_izq":
                Nodo listaids = nodoactual.hijos.get(0);
                for(Nodo temp:listaids.hijos){
                    this.gramatica.guardarIzq(temp.name.toLowerCase(),temp);
                }
                break;
                
            case "Asociatividad_der":
                Nodo listaids2 = nodoactual.hijos.get(0);
                for(Nodo temp:listaids2.hijos){ 
                    this.gramatica.guardarDer(temp.name.toLowerCase(),temp);
                }
                break;
                
            case "declaraciones":
                for(Nodo tempo:nodoactual.hijos)
                {this.recorridoAreaDeclaraciones(tempo);}
                break;
                
            case "dec_inicio":
                if (!this.gramatica.inicial.equals("")){                    
                    String descri="Ya se ha identificado el No Terminal inicial";
                    this.guardarError(nodoactual.hijos.get(0).name, nodoactual.linea, nodoactual.columna,"semantico", descri);
                    return;
                }
                
                this.gramatica.inicial = nodoactual.hijos.get(0).name.toLowerCase();
                this.acciones += "El Terminal:  " + nodoactual.hijos.get(0).name + "  es el simbolo INICIAL " + "\n";
                
                if(!this.gramatica.listaNterminales.containsKey(gramatica.inicial)){
                    String descri="No Terminal para Inicio, no existe";
                    this.guardarError(nodoactual.hijos.get(0).name, nodoactual.linea, nodoactual.columna,"Semantico", descri);
                    this.gramatica.inicial="";
                }
                break;
                
            default:
                System.out.println("!Default, recorridoDeclaraciones");
                break;                
        }
    }
    
    private void capturarTerminales(Nodo nodoactual){
        if(nodoactual.hijos.size() == 1){
            Nodo listaids = nodoactual.hijos.get(0);        
            for(Nodo temp: listaids.hijos){
                Terminal nuevo_terminal=new Terminal(temp.name);                
                this.gramatica.guardarTerminal(nuevo_terminal, nodoactual);
            }
        }else{
            String stipo = nodoactual.hijos.get(0).name;
            Nodo listaids = nodoactual.hijos.get(1);                    
            for(Nodo temp: listaids.hijos){
                Terminal nuevo_terminal=new Terminal(temp.name, stipo);
                this.gramatica.guardarTerminal(nuevo_terminal, nodoactual);
            }            
        }
    }
    
    private void capturarNterminales(Nodo nodoactual){
        if(nodoactual.hijos.size()== 1){
            Nodo listaids = nodoactual.hijos.get(0);        
            for(Nodo temp: listaids.hijos){
                NoTerminal nuevo = new NoTerminal(temp.name);
                this.gramatica.guardarNterminal(nuevo, nodoactual);
            }
        }else{
            String stipo = nodoactual.hijos.get(0).name;
            Nodo listaids = nodoactual.hijos.get(1);                    
            for(Nodo temp: listaids.hijos){
                NoTerminal nuevo = new NoTerminal(temp.name, stipo);
                this.gramatica.guardarNterminal(nuevo, nodoactual);
            }
        }
    }
    
    public String LeerULX(String Archivo){
        String cad = "";
        try{
            FileInputStream fstream = new FileInputStream(Archivo); // Abrimos el archivo
            DataInputStream entrada = new DataInputStream(fstream); // Creamos el objeto de entrada
            BufferedReader buffer = new BufferedReader(new InputStreamReader(entrada)); // Creamos el Buffer de Lectura
            String strLinea;
            // Leer el archivo linea por linea
            while ((strLinea = buffer.readLine()) != null){
                // Concatena cadena
                cad += strLinea;
            }            
            entrada.close(); // Cerramos el archivo
        }catch (Exception e){ //Catch de excepciones
            System.err.println("Ocurrio un error: " + e.getMessage());
        }
        return cad;
    }
    
    public void AnalizarULX(String ruta){
        String cad = this.LeerULX(ruta);
                
        Reader reader = new StringReader(cad);
        scanner lex = new scanner(reader);
        parser par = new parser(lex);

        try {
            par.parse();
            if(par.raizarchivo != null){
                this.graficarAstTer(par.raizarchivo);
                this.recorridoAreaDeclaraciones(par.raizarchivo);             


            }else if(lex.erroresL.size() != 0 && par.erroresS.size() == 0 ){
                for(Errores tempo:lex.erroresL){
                    this.listaErrores.add(tempo);
                }                
                JOptionPane.showMessageDialog(null, "Archivo ULX con Errores Lexicos");
            }else if(par.erroresS.size() != 0 && lex.erroresL.size() == 0){
                for(Errores tempo:par.erroresS){
                    this.listaErrores.add(tempo);
                } 
                JOptionPane.showMessageDialog(null, "Archivo ULX con Errores Sintacticos");

            }else if(par.erroresS.size() != 0 && lex.erroresL.size() != 0){
                for(Errores tempo:lex.erroresL){
                    this.listaErrores.add(tempo);
                }  
                for(Errores tempo:par.erroresS){
                    this.listaErrores.add(tempo);
                } 
                JOptionPane.showMessageDialog(null, "Archivo ULX con Errores Lexicos y Sintacticos");
            }

        } catch (Exception ex) {
            Logger.getLogger(Compilador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void capturarPrecedencia(Nodo nodoactual){
        int todoscorrectos = 0;
                for(Nodo tempo:nodoactual.hijos.get(0).hijos){
                    if(this.gramatica.listaTerminales.containsKey(tempo.name.toLowerCase())){
                        if(this.gramatica.listaPrecedencia.containsKey(tempo.name.toLowerCase())){                            
                            String descri = "Ya se habia indicado precedencia";
                            this.guardarError(tempo.name, tempo.linea, tempo.columna, "Semantico",descri);
                        }else{
                            this.gramatica.listaPrecedencia.put(tempo.name.toLowerCase(), this.gramatica.nivel_precedencia);
                            todoscorrectos++;
                            this.acciones += "El Terminal:  " + tempo.name + "  Tiene Precedencia: " + this.gramatica.nivel_precedencia + "\n";
                        }
                    }else{
                        String descri="No existe Terminal";
                        this.guardarError(tempo.name, tempo.linea, tempo.columna, "Semantico",descri);
                    }    
                }   
                if(todoscorrectos > 0){
                    this.gramatica.nivel_precedencia++;
                }    
    }
    
    private Boolean errorenDeclaraciones(){
        Boolean reto = false;
        if(gramatica.inicial==null){
            this.guardarError("iniciar", 0, 0, "Semantico","NoTerminal inicial no declarado");
            reto = true;
        }
        if(gramatica.listaNterminales.isEmpty()){
            this.guardarError(" ", 0, 0, "Semantico","No Terminales vacios");
            reto = true;
        }
        if(gramatica.listaTerminales.isEmpty()){
            this.guardarError(" ", 0, 0, "Semantico","Terminales vacios");
            reto = true;
        }        
        return reto;
    }
           
    //</editor-fold>
    
    
    // <editor-fold desc="Recorrer Gramatica">
    
    private void analizarAreaGramatica(){
        this.gramatica.listaProducciones = new HashMap<String, Produccion>();
        this.gramatica.listReglas =  new HashMap<Integer, Regla>();
        this.recorrido_ConstruccionGramatica(this.areagramatica);
        System.out.println("paso1");
    }
    
    //recorrido_ConstruccionGramatica
    private void recorrido_ConstruccionGramatica(Nodo nodoactual){
        switch (nodoactual.name){
            case "gramatica":
                for(Nodo temp:nodoactual.hijos){
                    this.recorrido_ConstruccionGramatica(temp);
                }
                break;
                
            case "superproduccion":
                this.capturarSuperProduccion(nodoactual);
                break;
                
            default:
                System.out.println("Default, recorridoConstruccionG");
                break;
        }
    }
    
    
    private void capturarSuperProduccion(Nodo nodoactual){
        
        //nombre de no terminal del lado izquierdo de la produccion
        String izquierda = nodoactual.hijos.get(0).name.toLowerCase();
        
        //valuar que exista en la lista de no terminales
        NoTerminal temp_noterminal = this.gramatica.getNterminal(izquierda);
        if(temp_noterminal==null){
            String sdescri="NoTerminal sin declarar";
            this.guardarError(izquierda, nodoactual.linea, nodoactual.columna, "Semantico", sdescri);
        }else{
            //evaluar que no exista el No terminal en elementos G
            if(this.gramatica.listaProducciones.containsKey(izquierda)){
                String sdescri="Ya existe reglas asociadas al NoTerminal";
                this.guardarError(izquierda, nodoactual.linea, nodoactual.columna, "Semantico", sdescri);
                return;
            }
            
            //creo la produccion
            Produccion super_produccion = new Produccion(temp_noterminal);            
            Nodo nodo_reglas = nodoactual.hijos.get(1);
            
            //por cada regla en reglas de superproduccion
            for(Nodo nodo_regla: nodo_reglas.hijos){                
                Regla nueva_regla = new Regla();
                nueva_regla.iniciarTodo(izquierda);                
                
                //por cada simbolo en regla
                for(Nodo nodo_simbolo: nodo_regla.hijos){
                    this.capturarSimbolo(nueva_regla, nodo_simbolo);
                }
                //si no hay errores guardo la regla
                if(Compilador.listaErrores.isEmpty()){
                    
                    nueva_regla.ntasociado=temp_noterminal;
                    
                    // aqui enumero la regla :D
                    nueva_regla.id=this.gramatica.contadorReglas+0;
                    super_produccion.listaReglas.add(nueva_regla);
                    this.gramatica.contadorReglas=this.gramatica.contadorReglas+1;
                    
                    //esto es para el parser :D
                    this.gramatica.listReglas.put(nueva_regla.id, nueva_regla);
                }                
            }
            
            //guardo la super produccion
            if(Compilador.listaErrores.isEmpty()){
                //valuar si es Inicial
                if(super_produccion.sizquierda.equals(this.gramatica.inicial)){
                    super_produccion.esInicial=true;
                }
                this.gramatica.listaProducciones.put(izquierda, super_produccion);
            }            
        }
    }

    private void capturarSimbolo(Regla pregla, Nodo nodoactual){        
        String name = nodoactual.hijos.get(0).name.toLowerCase();
        
        //evaluar si existe
        if(this.gramatica.listaNterminales.containsKey(name)){
            //es un no terminal
            Simbolo nuevo_simbolo = new Simbolo(name);
            nuevo_simbolo.es_terminal=false;
            pregla.simbolos.add(nuevo_simbolo);
            if(nodoactual.hijos.size()==2){
                //nuevo_simbolo.accion=nodoactual.hijos.get(1);
                Nodo nodo_de_acciones = nodoactual.hijos.get(1);
                if(nodo_de_acciones.name=="nodo_prime"){
                    nuevo_simbolo.accionprime=nodoactual.hijos.get(1).hijos.get(0);
                }else{
                    nuevo_simbolo.accion=nodoactual.hijos.get(1);
                }
            }
            if(nodoactual.hijos.size()==3){
                nuevo_simbolo.accion=nodoactual.hijos.get(1);
                nuevo_simbolo.accionprime=nodoactual.hijos.get(2).hijos.get(0);
            }
        }else if(this.gramatica.listaTerminales.containsKey(name)){
            //es un terminal
            Simbolo nuevo_simbolo = new Simbolo(name);
            nuevo_simbolo.es_terminal=true;
            pregla.simbolos.add(nuevo_simbolo);
            if(nodoactual.hijos.size()==2){
                //nuevo_simbolo.accion=nodoactual.hijos.get(1);
                Nodo nodo_de_acciones = nodoactual.hijos.get(1);
                if(nodo_de_acciones.name=="nodo_prime"){
                    nuevo_simbolo.accionprime=nodoactual.hijos.get(1).hijos.get(0);
                }else{
                    nuevo_simbolo.accion=nodoactual.hijos.get(1);
                }                
            }
            if(nodoactual.hijos.size()==3){
                nuevo_simbolo.accion=nodoactual.hijos.get(1);
                nuevo_simbolo.accionprime=nodoactual.hijos.get(2).hijos.get(0);
            }            
        }else{
            String descri="Simbolo no declarado";
            this.guardarError(name, nodoactual.linea, nodoactual.columna, "Semantico", descri);
        }
    }
    
    //</editor-fold> 
    
    
    // <editor-fold desc="Recorrer Codigo">
    
     private void analizarAreaCodigo(){     
        
        if(this.areacodigo==null)
        {return;}
        
        //this.listaLocal = new HashMap<String, Variable>();
        this.listaClases = new HashMap<String,Clases>();
        
        this.recorridoAreaCodigo(areacodigo);
    }
    
    private void recorridoAreaCodigo(Nodo nodoactual){
        switch (nodoactual.name){
            
            case "cuerpo":
                this.recorridoAreaCodigo(nodoactual.hijos.get(0));
                break;
                
            case "codigo":
                for(Nodo item:nodoactual.hijos)
                {this.recorridoAreaCodigo(item);}
                break;
                
            case "clas1":
                this.capturarClase(nodoactual);
                break;
            case "clash1":
                this.capturarClase(nodoactual);
                break;
            case "clas2":
                this.capturarClase(nodoactual);
                break;
                
            case "clash2":
                this.capturarClase(nodoactual);
                break;
                
            case "dec_variables":
                String stipo = nodoactual.hijos.get(0).name;
                Nodo listaids = nodoactual.hijos.get(1);
                for(Nodo temp: listaids.hijos){
                    Variable nueva_variable =new Variable(temp.name, stipo);
                    this.guardarVariable(this.claseactual,nueva_variable, nodoactual);
                    this.acciones += "Se guardo la variable "+ nueva_variable.name ;
                }                       
                break;
                
            case "dec_vec_arr":               
                EvaluarE evev = new EvaluarE();
                Object tamanio = evev.evaluarExpresion(nodoactual.hijos.get(1));
                String stipov = nodoactual.hijos.get(2).name;

                if(evev.tipoObjeto(tamanio)==1)
                {
                    Variable nueva_variable = new Variable();
                    nueva_variable.convertirVector(nodoactual.hijos.get(0).name, stipov, tamanio);
                    this.guardarVariable(this.claseactual,nueva_variable, nodoactual);
                    this.acciones += "Se guardo el vector "+ nueva_variable.name ;
                }
                else
                {
                    String descri="Tamanio de vector no es del tipo int";
                    this.guardarError(tamanio.toString(), nodoactual.hijos.get(1).linea, nodoactual.hijos.get(1).columna, "Semantico",descri);
                }
                break;                                      
                
            case "decla_asignacion":
                this.capturarDeclaAsigna(nodoactual);
                break;
                                
            case "metodo_1":
                this.capturarMetodo1(this.claseactual,nodoactual);
                break;
                
            case "metodo_2":
                this.capturarMetodo2(this.claseactual,nodoactual);
                break;
                
            case "funcion_1":
               this.capturarFuncion1(this.claseactual,nodoactual);
                break;
                
            case "funcion_2":                
                this.capturarFuncion2(this.claseactual,nodoactual);
                break;
                
            default:
                System.out.println("! ============================================================ Default, recorridoCodigo");
                break;                
        }
    }
    
    
    private void capturarClase(Nodo nodoactual){
        this.claseactual = nodoactual.hijos.get(0).name;
        Clases clas = new Clases(this.claseactual);  
        this.listaClases.put(this.claseactual,clas);
        switch (nodoactual.name){
            
            case "clas1":
                clas.esHeredada = false;
                break;
                
            case "clash1":
                clas.esHeredada = true;
                clas.heredade = nodoactual.hijos.get(1).name;
                break;
                
            case "clas2":
                clas.esHeredada = false;
                for(Nodo temp: nodoactual.hijos.get(1).hijos){
                    this.recorridoAreaCodigo(temp);
                }                
                break;
                
            case "clash2":
                clas.esHeredada = true;
                for(Nodo temp: nodoactual.hijos.get(1).hijos){
                    this.recorridoAreaCodigo(temp);
                }
                clas.heredade = nodoactual.hijos.get(2).name;
                break;
            
                
            default:
                System.out.println("! ============================================================ Default, recorridoClases");
                break;  
        }
        
 }
    
    
    private void capturarDeclaracion(Nodo nodoactual)
    {
        Nodo listaids = nodoactual.hijos.get(0);
        String stipo = nodoactual.hijos.get(1).name;
        
        for(Nodo temp: listaids.hijos)
        {
            Variable nueva_variable =new Variable(temp.name, stipo);
            this.guardarVariable(this.claseactual,nueva_variable, nodoactual);
        }        
    }        
    
    private void capturarDeclaAsigna(Nodo nodoactual)
    {
        Nodo listaids = nodoactual.hijos.get(1);
        String stipo = nodoactual.hijos.get(0).name;        
        
        for(Nodo temp: listaids.hijos)
        {
            Variable nueva_variable =new Variable(temp.name, stipo);
            this.guardarVariable(this.claseactual,nueva_variable, nodoactual);
            EvaluarE ev = new EvaluarE();
            Object ovalor=ev.evaluarExpresion(nodoactual.hijos.get(2));
            if(ev.correcto(nueva_variable.tipo, ovalor)==false)
            {
                String descri = "Tipos diferentes: "+nueva_variable.stipo+" vs "+ev.stipoObjeto(ovalor);
                this.guardarError(temp.name, nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
                return;
            }            
            if(nueva_variable.tipo==1){nueva_variable.valor=ev.getInt(ovalor);}
            else if(nueva_variable.tipo==2){nueva_variable.valor=ev.getDouble(ovalor);}
            else if(nueva_variable.tipo==3){nueva_variable.valor=ovalor.toString();}
            else if(nueva_variable.tipo==4){nueva_variable.valor=ev.getChar(ovalor);}
            else if(nueva_variable.tipo==5){nueva_variable.valor =ev.getBool(ovalor);} 
            this.acciones += "Se guardo la variable "+ nueva_variable.name ;
        }                    
    }
    
    private void guardarVariable(String clas,Variable vnueva, Nodo nodoactual){
        if(this.listaClases.get(clas).lista_variables.containsKey(vnueva.name.toLowerCase())){
            String descri="variable ya existe";
            this.guardarError(vnueva.name, nodoactual.linea, nodoactual.columna, "Semantico", descri);
        }else{
            this.listaClases.get(clas).lista_variables.put(vnueva.name.toLowerCase(), vnueva);
        }
    }
    
    private void capturarMetodo1(String posclas, Nodo nodoactual)
    {
        //fin rmetod:posi id:idd apar cpar TC_METODO:cuerpo_idfinal
        //h0, id de abrir   //h1, instrucciones     //h2, id de cierre
        String nabrir = nodoactual.hijos.get(0).name.toLowerCase().replace(" ","");
        String ncerrar = nodoactual.hijos.get(2).name.toLowerCase().replace(" ","");
               
        if(nabrir.equals(ncerrar)==false)
        {
            String descri="id para cerrar-abrir, Metodo no son iguales";
            this.guardarError(nabrir+" vs "+ncerrar, nodoactual.linea, nodoactual.columna, "Semantico", descri);
            return;
        }
        Funcion nf = new Funcion(nabrir, nodoactual.hijos.get(1));
        nf.convertirMetodo();
        this.guardarFuncionMetodoCero(posclas, nf, nodoactual);
        this.acciones += "Se guardo el Metodo "+ nf.name ;
    }   
    
    private void capturarMetodo2(String posclas, Nodo nodoactual)
    {
        //fin rmetod:posi id:idd apar LP:parametros cpar TC_METODO:cuerpo_idfinal
        //h0, id de abrir   //h1, lista parametros  //h2, instrucciones //h3, id de cierre
        String nabrir = nodoactual.hijos.get(0).name.toLowerCase().replace(" ","");
        String ncerrar = nodoactual.hijos.get(3).name.toLowerCase().replace(" ","");
        
        if(nabrir.equals(ncerrar)==false)
        {
            String descri="id para cerrar-abrir, Metodo no son iguales";
            this.guardarError(nabrir+" vs "+ncerrar, nodoactual.linea, nodoactual.columna, "Semantico", descri);
            return;
        }
        
        Funcion nf = new Funcion(nabrir, nodoactual.hijos.get(2));
        nf.convertirMetodo();
        //parametrizar
        Nodo nodoparamas = nodoactual.hijos.get(1);        
        for(Nodo nodot:nodoparamas.hijos)
        {
            nf.guardarParametro(nodot);
        }                            
        this.guardarFuncionMetodoMultis(posclas, nf, nodoactual);
        this.acciones += "Se guardo el Metodo "+ nf.name ;
    }
     
    private void capturarFuncion1(String posclas, Nodo nodoactual)
    {
        //fin:posi TIPO:tipo id:idd apar cpar TC_METODO:cuerpo_idfinal
        //h0, id de abrir   //h1, tipo  //h2, instrucciones //h3, id de cierre
        String nabrir = nodoactual.hijos.get(0).name.toLowerCase().replace(" ","");
        String ncerrar = nodoactual.hijos.get(3).name.toLowerCase().replace(" ","");
        if(nabrir.equals(ncerrar)==false)
        {
            String descri="id para cerrar-abrir, Funcion no son iguales";
            this.guardarError(nabrir+" vs "+ncerrar, nodoactual.linea, nodoactual.columna, "Semantico", descri);
            return;
        }           
        Funcion nf = new Funcion(nabrir, nodoactual.hijos.get(2));
        nf.convertirFuncion(nodoactual.hijos.get(1).name);
        this.guardarFuncionMetodoCero(posclas, nf, nodoactual);
        this.acciones += "Se guardo el Metodo "+ nf.name ;
    }
     
    private void capturarFuncion2(String posclas, Nodo nodoactual)
    {
        //fin:posi TIPO:tipo id:idd apar LP:parametros cpar TC_METODO:cuerpo_idfinal
        //h0, id de abrir   //h1, tipo  //h2, parametros    //h3, instrucciones //h4, id de cierre
        String nabrir = nodoactual.hijos.get(0).name.toLowerCase().replace(" ","");
        String ncerrar = nodoactual.hijos.get(4).name.toLowerCase().replace(" ","");
        if(nabrir.equals(ncerrar)==false)
        {
            String descri="id para cerrar-abrir, Funcion no son iguales";
            this.guardarError(nabrir+" vs "+ncerrar, nodoactual.linea, nodoactual.columna, "Semantico", descri);
            return;
        }
        Funcion nf = new Funcion(nabrir, nodoactual.hijos.get(3));
        nf.convertirFuncion(nodoactual.hijos.get(1).name);
        //parametrizar
        Nodo nodoparamas = nodoactual.hijos.get(2);
        for(Nodo nodot:nodoparamas.hijos)
        {
            nf.guardarParametro(nodot);
        }
        this.guardarFuncionMetodoMultis(posclas, nf, nodoactual);
        this.acciones += "Se guardo el Metodo "+ nf.name ;
    }
    
    private void guardarFuncionMetodoCero(String posclas, Funcion pf, Nodo nodoactual)
    {
        for(Funcion fu:this.listaClases.get(posclas).listaFM)
        {
            //if(fu.name==pf.name)
            if(fu.name.equalsIgnoreCase(pf.name))
            {
                String descri="Metodo-funcion, sin parametros ya existe";
                this.guardarError(pf.name, nodoactual.linea, nodoactual.columna, "Semantico", descri);
                return;
            }
        }
        // si paso no existe
        this.listaClases.get(posclas).listaFM.add(pf);
    }
    
    private void guardarFuncionMetodoMultis(String posclas, Funcion fnueva, Nodo nodo)
    {
        for(Funcion item:this.listaClases.get(posclas).listaFM)
        {
            if(item.name.toLowerCase().equals(fnueva.name.toLowerCase()))
            {
                //si existe, validar que los parametros no sean iguales
                if(this.parametrosIguales(fnueva, item)==true)
                {
                    String descri="Ya existe un metodo-funcion igual en nombre y parametros";
                    this.guardarError(item.name, nodo.linea, nodo.columna, "Semantico", descri);
                    return;
                }
            }
        }
        // si paso no existe
        this.listaClases.get(posclas).listaFM.add(fnueva);
    }

    private Boolean parametrosIguales(Funcion f1, Funcion f2)
    {
        if(f1.lista_parametros.size()!=f2.lista_parametros.size())
        {
            return false;
        }        
        //paso validar que los parametros sean iguales
        for (int i = 0; i < f1.lista_parametros.size(); i++) 
        {
            if(f1.lista_parametros.get(i).tipo!=f2.lista_parametros.get(i).tipo)
            {
                return false;
            }
        }                
        return true;
    }
    
        
    //</editor-fold>    
    
    
    //</editor-fold> 
        
   
    
    // <editor-fold desc="Creacion de metodo SLR">
    
    public void AnalisisSLR(){
        gramatica.f1_aumentarGramatica();
        //System.out.println(gramatica.getS_Gramatica());
        //System.out.println(gramatica.getS_ReglasEnumeradas());   
        
        //crear los primeros de cada regla
        gramatica.f1_crearprimeros();
        
        
        
        ////------ESTADO SLR
        gramatica.f2_crearEstado000();                
        gramatica.f2_siguientes000();
        
        
        //crear el estado0
        gramatica.f2_crearEstado0();       
        
        //buscar primero del estado0
        gramatica.f2_primeros0();
        
        //buscar siguientes del estado0
        gramatica.f2_siguientes0();
        //System.out.println(gramatica.getS_EstadosPrimerosSiguientes());                
        
        //System.out.println("aaaaaaaaaa");
        //crear los conjuntos de estados
        gramatica.f3_crearConjuntos();
        //System.out.println("bbbb");
        
        
        gramatica.f3_crearReducciones();
       // System.out.println(gramatica.getS_EstadosPrimerosSiguientesReducciones());      
        //System.out.println("a");
    }
    
    
    
    //</editor-fold> 
    
    public DefaultTableModel dtm;
    Sintactico sintactico;
    
    // <editor-fold desc="Creacion de TABLA LSR">
     
    public void ConstruirTAS(){
        //Instancio la clase de Tabla de Analisis Sintactico
        //paso las listas necesarias
        TAS2 tabla = new TAS2(gramatica.listaTerminales, gramatica.listaNterminales, gramatica.listaEstados);
        
        //Crea la estructura de la tabla
        tabla.crearTabla3();
        
        //Llena los desplazamientos
        tabla.pasarDesplazamientos();
        
        //Llena las reducciones
        tabla.pasarReducciones();
        
        //Imprime el archivo CSV
        //tabla.soutTabla_CSV();
        tabla.soutTabla_CSV2();
        
        this.dtm = tabla.dtm;
        this.sintactico.estadosTAS=tabla.estadosTAS;        
    }
    
    
    //</editor-fold> 
     
    public String entrada_json;
     
    //<editor-fold desc="Inicia el proceso(Pruebas con Cadena)">
    
    public void AnalisisDeCadena(){
        
         //Aqui tengo que actualizar el ejecutador :D, Necesita
         //Lista de variables globales que guardo el traductor
         //Lista de funciones, Metodos
         sintactico.inicial = gramatica.produccionAumentada.sizquierda;
         sintactico.listReglas = gramatica.listReglas;
         sintactico.listaTerminales = gramatica.listaTerminales;
         sintactico.listaNterminales = gramatica.listaNterminales;
         sintactico.listaPrecedencia = gramatica.listaPrecedencia;
         sintactico.listaIzq = gramatica.listaIzq;
         sintactico.listaDer = gramatica.listaDer;
         
         //sintactico.listaLocal = listaLocal;
         sintactico.listaClases = listaClases;
         
    }
    
    
    
    public void AnalizarCad(String entrada){                        
        if(this.sintactico==null){
            JOptionPane.showMessageDialog(null,"Gramatica sin Compilar","FiSintax",1);
            return;
        }
        //inicia analisis para la cadena de entrada
        this.sintactico.Parte_A_Iniciar(entrada, this.dtm);
        
        if(this.sintactico.aceptado==true){
            EjecucionGra eje = new EjecucionGra(this.sintactico.listaLocal, this.sintactico.listaClases );
            eje.Ejecutar_1_iniciar(this.sintactico.nodo_raiz);
        }        
    }
    
    
    //</editor-fold>
    
    
    // <editor-fold desc="HTML de Errores">
         
    public void getErrores(){   
        if(listaErrores.isEmpty()){
            return;
        }        
        String html = "<html><body bgcolor= \"#598DDA\">\n";
        html += "<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"height:220px; width:575px\">\n";
        html += "<tbody>\n";
        html += "<tr>\n";
        html += "<td>\n";
        html += "<span style=\"color:#000000\"><span style=\"font-size:16px\"><span style=\"font-family:arial,helvetica,sans-serif\">";
        html += "Universidad De San Carlos De Guatemala<br />\n";
        html += "Facultad de Ingenier&iacute;a <br />\n";
        html += "Escuela de Ciencias y Sistemas <br>\n";
        html += "Organizaci&oacute;n de Lenguajes y Compiladores 2 &nbsp&nbsp&nbsp<br>\n";
        html += "Secci&oacute;n N <br>\n";
        html += "Vacaciones Diciembre 2015 <br>\n";
        html += "Luis Aroldo Morales Noriega <br>\n";
        html += "201020944 <br>\n";
        html += "Proyecto 1</span></span></span>\n";
        html += "</td>\n";
        html += "<td>\n";
        html += "</td>\n";
        html += "</tr>\n";
        html += "</tbody>\n";
        html += "</table>\n";
        
        html += "<h2 style=\"text-align:center\"><span style=\"font-size:18px\"><span style=\"font-family:arial,helvetica,sans-serif\"><span style=\"color:#000000\">Reporte de Errores</span></span></span></h2>\n";

        html += "<table align=\"center\" border=\"1\" bordercolor=\"#000000\" cellpadding=\"5\" cellspacing=\"0\" bordercolor=\"WHITE\" style=\"border-collapse:collapse\">\n";
        html += "<tbody>\n";
        html += "<tr>\n";
        html += "<td style=\"text-align:center\"><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\"><strong>Simbolo</strong></span></span></span></td>\n";
        html += "<td style=\"text-align:center\"><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\"><strong>Fila</strong></span></span></span></td>\n";
        html += "<td style=\"text-align:center\"><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\"><strong>Columna</strong></span></span></span></td>\n";
        html += "<td style=\"text-align:center\"><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\"><strong>Tipo</strong></span></span></span></td>\n";
        html += "<td style=\"text-align:center\"><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\"><strong>Descripcion</strong></span></span></span></td>\n";
        html += "</tr>\n";
        
        
        for(Errores tempo:listaErrores){
            html += "<tr>\n";
            html += "<td><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\">" + tempo.token + "</span></span></span></td>";
            html += "<td><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\">" + tempo.linea + "</span></span></span></td>";
            html += "<td><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\">" + tempo.columna + "</span></span></span></td>";
            html += "<td><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\">" + tempo.tipoerror + "</span></span></span></td>";
            html += "<td><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\">" + tempo.descripcion + "</span></span></span></td>";
            html += "</tr>";
        }
             
        html += "</tbody>\n";
        html += "</table>\n";

        html += "<br>\n";
        html += "<hr />";
        html += "</body>\n</html>";
        
        creararchivo("Errores.html", html);
        File pathfile = new File("Errores.html");
        if(pathfile.exists()){
            try{
                Desktop.getDesktop().open(pathfile);
            } 
            catch (IOException ex) {Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, ex);}
        }        
    }
    
    public synchronized void creararchivo(String pfichero,String pcontenido){   
        FileWriter archivo = null;
   
        try{archivo = new FileWriter(pfichero);} 
        catch (IOException ex) 
        {Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, ex);}

        File a = new File(pfichero);        
        if (!a.exists()){return;}   
        
        try(PrintWriter printwriter = new PrintWriter(archivo)){
            printwriter.print(pcontenido);
            printwriter.close();
        }
    }
    
    
     //</editor-fold> 
    
}
