/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class Funcion {
    
    //<editor-fold desc="Atributos de la clase">
    
    public String name;
    public Nodo cuerpo;
    public Object retorno;
    public Boolean esMetodo;
    public int tipo;        
    public ArrayList<Variable>lista_parametros;
    public HashMap<String,Variable>lista_variables;
    
    
    //</editor-fold>
    
        
    // <editor-fold desc="Contructores"> 
    
    public Funcion(){}
    
    public Funcion(String pname){
        this.name=pname.toLowerCase();
    }
    
    //constructor de metodosc sin parametros
    public Funcion(String pname, Nodo pcuerpo){
        this.name=pname.toLowerCase();
        this.cuerpo=pcuerpo;
    }    
     
    public void convertirMetodo(){        
        this.esMetodo=true;        
        this.lista_variables=new HashMap<String,Variable>();
        this.lista_parametros=new ArrayList<Variable>();
        this.tipo=0;
    }
    
    public void convertirFuncion(String ptipo){
        this.esMetodo=false;
        this.lista_variables=new HashMap<String,Variable>();
        this.lista_parametros=new ArrayList<Variable>();
        this.actualizarTipo(ptipo);
    }
        
    private void actualizarTipo(String ptipo){
        switch (ptipo.toLowerCase()) {
            case "int":     this.tipo=1; break;
            case "double":  this.tipo=2; break;
            case "string":  this.tipo=3; break;
            case "char":    this.tipo=4; break;
            case "bool":    this.tipo=5; break;
            default:        this.tipo=0; break;
        }
    }
    
    
    // </editor-fold>
    
    
    //<editor-fold desc="Manejo de Listas">
    
    private void guardarError(String t, int l, int c, String ti, String d){
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        Compilador.listaErrores.add(new Errores(t,l,c,ti,d));
    }
    
    public void guardarParametro(Nodo nodo){
        String namepara = nodo.hijos.get(1).name.toLowerCase();
        if(this.lista_variables.containsKey(namepara)){
            String descri = "Parametro ya existe";
            this.guardarError(namepara, nodo.linea, nodo.columna, "Semantico",descri);
            return;
        }
        Variable var = new Variable(namepara, nodo.hijos.get(0).name);
        this.lista_variables.put(namepara, var);
        this.lista_parametros.add(var);
    }        
   
    public Boolean verificarParametrizados(ArrayList<Object> lista_parametrizados){
        if(this.lista_parametros.size()!=lista_parametrizados.size()){
            return false;
        }
        
        for (int i = 0; i < this.lista_parametros.size(); i++){
            if(this.correcto(this.lista_parametros.get(i).tipo, lista_parametrizados.get(i))==false){
                return false;
            }
        }
        //Si llego aqui entonces si estan bien los parametros
        return true;
    }
    
    public void actualizarParametros(ArrayList<Object> parametrizados){
        //hasta este punto todo esta bien
        this.lista_variables=new HashMap<String,Variable>();
        for (int i = 0; i < parametrizados.size(); i++) 
        {
            Object valor_parametro = parametrizados.get(i);
            Variable parametro = this.lista_parametros.get(i);
            parametro.valor=valor_parametro;
            this.lista_variables.put(parametro.name, parametro);
        }
    }
    
    public void duplicarParametro(Variable origen, Variable destino){
        destino.name=origen.name;
        destino.esVector=origen.esVector;
        destino.stipo=origen.stipo;
        destino.tamanio=origen.tamanio;
        destino.tipo=origen.tipo;
        destino.valor=null;
        if(origen.esVector==true){
            origen.vector = new LinkedList<Object>();
            switch (destino.tipo){
                case 1:
                    for (int i = 0; i < destino.tamanio; i++){
                        destino.vector.add(0);
                    }
                    break;
                case 2:
                    for (int i = 0; i < destino.tamanio; i++){
                        destino.vector.add(0);
                    }
                    break;
                case 3:
                    for (int i = 0; i < destino.tamanio; i++){
                        destino.vector.add("");
                    }
                    break;
                case 4:
                    for (int i = 0; i < destino.tamanio; i++){
                        destino.vector.add('a');
                    }
                    break;
                case 5:
                    for (int i = 0; i < destino.tamanio; i++){
                        destino.vector.add(false);
                    }
                    break;
                default:
                    break;
            }   
        }
    }    
    
    
    //</editor-fold>
    
        
    //<editor-fold desc="Otros">
    
    public boolean correcto(int tipo_variable, Object ob){
        int tipo_objeto=this.tipoObjeto(ob);
        
        switch (tipo_variable){
            case 1:
                switch (tipo_objeto){
                    case 1: return true;    //int default
                    case 4: return true;    //char pero en modo ascii
                    case 5: return true;    //booleano -> logico
                    default:    break;
                }break;
            case 2:
                switch (tipo_objeto){
                    case 1: return true;    //int
                    case 2: return true;    //double default
                    default:    break;
                }break;
            case 3:
                if(tipo_objeto==3){return true;}    //string default
                break;
            case 4:
                switch (tipo_objeto){
                    case 1: return true;    //int, de ascii a char
                    case 4: return true;    //char default
                    default:    break;
                }break;
            case 5:
                switch (tipo_objeto){
                    case 1: return true;    //int
                    case 5: return true;    //bool default
                    default:    break;
                }break;
            default:    break;
        }
        return false;
    } 
    
    public int tipoObjeto(Object ob){
        if(ob instanceof Integer){return 1;}
        else if(ob instanceof Double){return 2;}
        else if(ob instanceof String){return 3;}
        else if(ob instanceof Character){return 4;}
        else if(ob instanceof Boolean){return 5;}
        return 0;
    }
    
    
    //</editor-fold>
}
