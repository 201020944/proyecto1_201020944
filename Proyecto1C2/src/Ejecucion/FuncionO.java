/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.ArrayList;

/**
 *
 * @author LuisAroldo
 */
public class FuncionO {
    //public String nombre1, nombre2, nombre3;
    public String clase, nombre;
    public String cadenap1, cadenap2;   //1 SIN "#", TIENE "_"  // 2 CON "#"
    public String rol, tipo, visibilidad;
    public String ambito;
    public int tamanio;
    public ArrayList<VariableO> parametros;
    public ArrayList<VariableO> variables;
    
    public FuncionO(){}
    
    public FuncionO(String sclase, String sfuncion, String rol, String stipo, String svisi){
        this.clase = sclase.trim();
        this.nombre= sfuncion.trim();
        this.cadenap1="";
        this.cadenap2="";
        this.rol="Funcion";
        this.tipo = stipo.trim();
        this.visibilidad = svisi.trim();
        this.ambito= sclase.trim();
        this.tamanio=0;
        this.parametros=new ArrayList<VariableO>();
        this.variables=new ArrayList<VariableO>();
    }
    
    public void setCadenasP(String pcadenap){
        this.cadenap2=pcadenap.trim();
        this.cadenap1=cadenap2.replace("#", "_");
        for(VariableO var:this.parametros){
            var.cadenap1=this.cadenap1;
            var.cadenap2=this.cadenap2;
        }
    }
    
    public void updateTamanio(){
        this.tamanio=this.parametros.size()+this.variables.size()+2;
    }
    
    public String getDatosCSV(){
        StringBuilder salida = new StringBuilder();
        //nombre, tipo, rol, ambito, tmanio, posicion
        salida.append(this.nombre);
        salida.append(",");
        salida.append(this.tipo);
        salida.append(",");
        salida.append(this.rol);
        salida.append(",");
        salida.append(this.ambito);
        salida.append(",");
        salida.append(this.tamanio);
        salida.append(",");
        salida.append(" - ");
        salida.append("\n");
        
        for(VariableO atr:this.parametros){
            salida.append(atr.getDatosCSV());
        }
        for(VariableO atr:this.variables){
            salida.append(atr.getDatosCSV());
        }
        
        return salida.toString();
    }
    
    
    public VariableO getParametoVariable(String nameid){
        for (VariableO parametro : this.parametros){
            if (parametro.nombre.equals(nameid) == true){
                return parametro;
            }                
        }
        for (VariableO variable : this.variables){
            if (variable.nombre.equals(nameid) == true){
                return variable;
            }
        }        
        return null;
    }
}
