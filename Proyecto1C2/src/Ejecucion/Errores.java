/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

/**
 *
 * @author LuisAroldo
 */
public class Errores {
    
    public String token = "";
    public String tipoerror = "";
    public String linea = "-";
    public String columna = "-";
    public String descripcion = "";
    
    public Errores(String ptoken, int plinea, int pcolumna, String ptipo, String desc){
        this.token = ptoken;
        this.linea = String.valueOf(plinea+1);
        this.columna = String.valueOf(pcolumna+1);
        this.tipoerror = ptipo;
        this.descripcion = desc;
    }
    
    public Errores(String ptoken, String tipo, String desc){
        this.token = ptoken;
        this.tipoerror = tipo;
        this.descripcion = desc;
    }
    
    public Errores(){}
    
    public void getSerror(){
        System.out.println("Token: "+token+", Descri: "+descripcion+" - "+tipoerror+", posicion: ["+linea+":"+columna+"]");
    }
    
    
    
    
}
