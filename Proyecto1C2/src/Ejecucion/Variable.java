/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.LinkedList;

/**
 *
 * @author LuisAroldo
 */
public class Variable {
    
    //<editor-fold desc="Atributos de la clase">
    
    public String name, stipo;
    public Object valor;
    public Boolean esVector;    
    public int tipo, tamanio;
    public LinkedList<Object> vector;
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Constructores">
    
    public Variable(){}
    
    public Variable(String pname){
        this.name = pname.toLowerCase();
        valor = null;
        this.esVector = false;
    }
    
    public Variable(String pname, String stipo){
        this.name = pname.toLowerCase();
        this.actualizarTipo(stipo.toLowerCase());
        valor = null;
        this.esVector = false;
    }
    
    public Variable(String pname, Object ovalor){
        this.name = pname.toLowerCase();
        this.valor = ovalor;
    }
    
    public Variable(String pname, String stipo, Object ovalor){
        this.name = pname.toLowerCase();
        this.actualizarTipo(stipo);
        this.valor = ovalor;
    }
    
    public void convertirVector(String pname, String stipo, Object ptamanio){        
        this.name = pname.toLowerCase();
        this.actualizarTipo(stipo);
        this.tamanio = this.getInt(ptamanio);
        this.esVector = true;
        this.vector = new LinkedList<Object>();
        
        switch (this.tipo){
            case 1:
                for (int i = 0; i < this.tamanio; i++){
                    this.vector.add(0);
                }
                break;
            case 2:
                for (int i = 0; i < this.tamanio; i++){
                    this.vector.add(0);
                }
                break;
            case 3:
                for (int i = 0; i < this.tamanio; i++){
                    this.vector.add("");
                }
                break;
            case 4:
                for (int i = 0; i < this.tamanio; i++){
                    this.vector.add('a');
                }
                break;
            case 5:
                for (int i = 0; i < this.tamanio; i++){
                    this.vector.add(false);
                }
                break;
            default:
                break;
        }   
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Para admon. de tipos">
    
    public void setIndex(int index, Object value){
        this.vector.set(index, value);
    }
            
    private void actualizarTipo(String ptipo){
        stipo = ptipo.toLowerCase();
        switch (stipo) {
            case "int":     this.tipo=1;    break;
            case "double":  this.tipo=2;    break;
            case "string":  this.tipo=3;    break;
            case "char":    this.tipo=4;    break;
            case "bool":    this.tipo=5;    break;
            default:        this.tipo=0;    stipo="null";   break;
        }
    }
    
    public int getInt(Object ob){
       switch (tipoObjeto(ob)){
           case 1:
               return (int)ob;
           case 4:
               return (int)((char)ob);
           case 5:
               if ((boolean)ob){
                   return 1;
               }else{
                   return 0;
               }                    
           default:
               System.out.println("Error get INT");
               return 1;
       }
    }
        
    public int tipoObjeto(Object ob){
        if(ob instanceof Integer){return 1;}
        else if(ob instanceof Double){return 2;}
        else if(ob instanceof String){return 3;}
        else if(ob instanceof Character){return 4;}
        else if(ob instanceof Boolean){return 5;}
        return 0;
    }
    
    public int toAscii(char c){
        int ascii = (int)c;
        return ascii; 
    }
    
    //</editor-fold>
    
}
