/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.ArrayList;

/**
 *
 * @author LuisAroldo
 */
public class Nodo {
    
    public String name = "";
    public ArrayList<Nodo> hijos;
    public int id, linea, columna;
    
    public Nodo(){
    }
    
    @SuppressWarnings("Convert2Diamond")
    public Nodo(String pvalor){
        this.name = pvalor;
        this.hijos = new ArrayList<Nodo>();
    }
    
    public Nodo(String pvalor, int plin, int pcol){
        this.name = pvalor;
        this.hijos = new ArrayList<>();
        this.linea = plin;
        this.columna = pcol;
    }
}
