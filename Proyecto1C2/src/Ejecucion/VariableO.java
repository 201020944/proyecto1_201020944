/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

/**
 *
 * @author LuisAroldo
 */
public class VariableO {
    public String clase, funcion, nombre;
    public String cadenap2, cadenap1;
    public String tipo, rol, visi;
    public String ambito;
    public int tamanio, posicion, itipo;    
    
    public VariableO(){
        this.itipo=0;
    }
        
    public void toAtributo(String pclase, String patrib, String ptipo, String pvisi, int ppos){        
        this.clase = pclase.trim();
        this.funcion="";
        this.nombre=patrib.trim();
        this.cadenap1="";
        this.cadenap2="";
        this.tipo = ptipo.trim();
        this.rol = "Atributo";
        this.visi=pvisi.trim();        
        this.ambito = pclase.trim();
        this.tamanio=1;
        this.posicion=ppos;
        this.upTipo(ptipo);
    }
    
    public void upTipo(String stipo){       
        switch (stipo){
            case "int":     this.itipo=1;   break;
            case "double":  this.itipo=2;   break;
            case "string":  this.itipo=3;   break;
            case "char":    this.itipo=4;   break;
            case "bool":    this.itipo=5;   break;
            case "boolean": this.itipo=5;   break;
            default:
                System.out.println("! ============================================================ Defaul upTipo");
        }
    }
    
    public void toParametro(String pclase, String pfuncion, String pparam, String ptipo, int ppos){
        this.clase=pclase.trim();
        this.funcion=pfuncion.trim();
        this.nombre = pparam.trim();
        this.cadenap1="";
        this.cadenap2="";
        this.tipo = ptipo.trim();
        this.rol = "Parametro";
        this.visi="";
        this.ambito = pfuncion.trim();
        this.tamanio=1;
        this.posicion=ppos;
        this.upTipo(ptipo);
    }
    
    public void toVariable(String pclase, String pfuncion, String pvar, String ptipo, int ppos){
        this.clase=pclase.trim();
        this.funcion=pfuncion.trim();
        this.nombre = pvar.trim();
        this.cadenap1="";
        this.cadenap2="";
        this.tipo = ptipo.trim();
        this.rol = "Variable";
        this.visi="";
        this.ambito = pfuncion.trim();
        this.tamanio=1;
        this.posicion=ppos;
        this.upTipo(ptipo);
    }
    
    public String getDatosCSV(){
        StringBuilder salida = new StringBuilder();
        //nombre, tipo, rol, ambito, tmanio, posicion
        salida.append(this.nombre);
        salida.append(",");
        salida.append(this.tipo);
        salida.append(",");
        salida.append(this.rol);
        salida.append(",");
        salida.append(this.ambito);
        salida.append(",");
        salida.append("1");
        salida.append(",");
        salida.append(this.posicion);
        salida.append("\n");
        return salida.toString();
    }
}
