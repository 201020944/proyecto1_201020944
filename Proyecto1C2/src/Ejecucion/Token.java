/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class Token {
    
    //<editor-fold desc="Atributos de la clase">
    
    public String nametoken;
    public int itipo, linea, columna;
    public Object valor;
    public Boolean esterminal;    
      
    
    //</editor-fold>
    
    
    //<editor-fold desc="Constructores">
    
    public Token(){}    
    
    public void convertir_a_nt(String name, int int_linea, int int_columna, int int_tipo){
        this.nametoken=name;
        this.esterminal=false;        
        this.itipo=int_tipo;
        this.linea=int_linea;
        this.columna=int_columna;        
    }
    
    public void convertir_a_fin(){
        this.nametoken="[:fin:]";
        this.esterminal=false;
        this.itipo=7;   this.linea=0;   this.columna=0;
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Funciones para Admon. el Valor">
    
    public void actualizarTipo(String stipo){
        switch (stipo) {
            case "int":     
                this.itipo=1;
                this.valor=this.getInt(valor);
                break;
            case "double":  
                this.itipo=2; 
                this.valor=this.getDouble(valor);
                break;
            case "string":  
                this.itipo=3; 
                this.valor = valor.toString()+"";
                break;
            case "char":    
                this.itipo=4; 
                this.valor=this.getChar(valor);
                break;
            case "bool":    
                this.itipo=5; 
                this.valor=this.getBool(valor);
                break;
            default:        
                this.itipo=0; 
                break;
        }
    }
    
    public int tipoObjeto(Object ob){
        if(ob instanceof Integer){return 1;}
        else if(ob instanceof Double){return 2;}
        else if(ob instanceof String){return 3;}
        else if(ob instanceof Character){return 4;}
        else if(ob instanceof Boolean){return 5;}
        return 0;
    }
              
    public char toCaracter(int c){
        return (char)c;
    }

    public int toAscii(char c){
        int ascii = (int)c;
        return ascii; 
    }
    
    public int getInt(Object ob){
       switch (tipoObjeto(ob)){
           case 1:
               return (int)ob;
           case 4:
               return toAscii((char)ob);
           case 5:
               if ((boolean)ob){
                   return 1;
               }else{
                   return 0;
               }                    
           default:
               return Integer.valueOf(ob.toString());
       }
    }
    
    public double getDouble(Object ob){
        switch (tipoObjeto(ob)){
            case 1:
                return Double.parseDouble(ob.toString());
            case 2:
                return (double)ob;                                
            default:
                return Double.valueOf(ob.toString());
        }
    }
    
    public char getChar(Object ob){
        switch (tipoObjeto(ob)){
            case 1:
                return toCaracter((int)ob);
            case 4:
                return (char)ob;
            case 5:
                if ((boolean)ob){
                    return toCaracter(1);
                }else{
                    return toCaracter(0);
                }
            default:
                char[]abc = ob.toString().toCharArray();
                return abc[0];
        }
    }
    
    public Boolean getBool(Object ob){
        switch (tipoObjeto(ob)){
            case 5:
                return (boolean)ob;
            case 1:
                if ((int)ob == 1){
                    return true;
                }else if ((int)ob == 0){
                    return false;
                }else{
                    String descri = "1, 0 pueden ser logico y bool";
                    Errores eee = new Errores(ob.toString(),0,0, "Ejecucion",descri);
                    Compilador.listaErrores.add(eee);
                }
                return false;
            default: return Boolean.valueOf(ob.toString());
        }
    }
    
    public int toLogico(Object ob){
        if(this.tipoObjeto(ob)==5){
            if((Boolean)ob== true){
                return 1;
            }else if((Boolean)ob==false){
                return 0;
            }
        }        
        return 0;
    }
    
    
    //</editor-fold>
    
}
