/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.HashMap;

/**
 *
 * @author LuisAroldo
 */
public class Primeros {
    public String sizquierda;                   //no terminal asociado
    public HashMap<String, String> listaFirst;  //primeros al no terminal asociado
    
    public Primeros(){
        this.listaFirst=new HashMap<String, String>();
    }
    
    public Primeros(String pizq){
        this.sizquierda=pizq;
        this.listaFirst=new HashMap<String, String>();        
    }
    
    public void guardarFirst(String psimbolo){
        if(!this.listaFirst.containsKey(psimbolo)){
            this.listaFirst.put(psimbolo, psimbolo);
        }
    }
    
    public String getSprimeros(){
        if(this.listaFirst==null){
            return "";
        }
        
        String reto="";
        for(String tempo:this.listaFirst.values()){
            reto+=" "+tempo;
        }        
        return reto;
    }
}
