/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

/**
 *
 * @author LuisAroldo
 */
public class NoTerminal {
    public String name, stipo;
    public int tipo;
        
    public NoTerminal(){}
    
    public NoTerminal(String pname){
        this.name=pname.toLowerCase();
        this.tipo=0;
        this.stipo="null";
    }
    
    public NoTerminal(String pname, String stipo){
        this.name = pname.toLowerCase();
        this.actualizarTipo(stipo);
    }
    
    private void actualizarTipo(String stipo){
        switch (stipo) {
            case "int":     this.tipo=1; break;
            case "double":  this.tipo=2; break;
            case "string":  this.tipo=3; break;
            case "char":    this.tipo=4; break;
            case "bool":    this.tipo=5; break;
            default:        this.tipo=0; break;
        }
        this.stipo =stipo;
        if(tipo==0){stipo="null";}
    }
}
