/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.ArrayList;

/**
 *
 * @author LuisAroldo
 */
public class ClaseO {
    public String nombre, ambito;
    public int tamanio;
    public ArrayList<VariableO> atributos;
    public ArrayList<FuncionO> funciones;
    public ArrayList<FuncionO> constructores; 
    public ArrayList<FuncionO> maines;
    
    public ClaseO(String pnombre){
        this.nombre = pnombre.trim();
        this.ambito = "Global";
        this.atributos= new ArrayList<VariableO>();
        this.funciones = new ArrayList<FuncionO>();
        this.constructores= new ArrayList<FuncionO>();
        this.maines= new ArrayList<FuncionO>();
    }
    
    public void updateTamanio(){
        this.tamanio = this.atributos.size();
    }
    
    public String getDatosCSV(){
        StringBuilder salida = new StringBuilder();
        //nombre, tipo, rol, ambito, tmanio, posicion
        salida.append(this.nombre);
        salida.append(",");
        salida.append(this.nombre);
        salida.append(",");
        salida.append("Clase");
        salida.append(",");
        salida.append("Global");
        salida.append(",");
        salida.append(this.tamanio);
        salida.append(",");
        salida.append(" - ");
        salida.append("\n");
        
        for(VariableO atr:this.atributos){
            salida.append(atr.getDatosCSV());
        }        
        for(FuncionO constt:this.constructores){
            salida.append(constt.getDatosCSV());
        }        
        for(FuncionO funt:this.funciones){
            salida.append(funt.getDatosCSV());
        }        
        return salida.toString();
    }
    
    public VariableO getAtributo_de_Clase(String name){        
        System.out.println("ROUND1");
        System.out.println("en getAtritbuto: <"+name+">");
        for (int i = 0; i < this.atributos.size(); i++) {
            System.out.println("vs : <"+this.atributos.get(i).nombre+">");
            if(this.atributos.get(i).nombre.equals(name)==true){
                return this.atributos.get(i);
            }
        }
        System.out.println("ROUND2");
        System.out.println("en getAtritbuto: <"+name+">");
        String buscado = ""+name+"";
        for(VariableO vvv:this.atributos){
            System.out.println("vs : <"+vvv.nombre+">");
            if(vvv.nombre.equals(buscado))
            {
                return vvv;
            }
        }
        System.out.println("AAAAAAAAAAAAAAAAAAA FIN");
        System.out.println("AAAAAAAAAAAAAAAAAAA FIN");
        System.out.println("AAAAAAAAAAAAAAAAAAA FIN");
        
        return null;
    }
            
    
    public FuncionO getFuncion(String namefuncion, String cadena2){
        namefuncion = namefuncion.replace(" ", "");
        namefuncion = namefuncion.replace("a", "$");
        String name2 = namefuncion.replace("$", "a");
        
        
        for (FuncionO funcione : this.funciones){
            if (funcione.nombre.equals(name2)){
                if ((funcione.cadenap2.replace(" ", "").length() == 0) && cadena2.replace(" ", "").length()==0) {
                    return funcione;
                }else if (funcione.cadenap2.replace(" ", "") == cadena2){
                   return funcione;
                }
            }
        }
        
        String fb=""+name2+"";
        for(FuncionO fff:this.funciones){
            if(fff.nombre.equals(fb)){
                if(cadena2.trim().length()==0 && fff.cadenap2.trim().length()==0){
                    return fff;
                }else if(cadena2==fff.cadenap2){
                    return fff;
                }
            }
        }
        
        for (FuncionO constructore : this.constructores){
            if (constructore.nombre.equals(name2)){
                if ((constructore.cadenap2.trim().length() == 0) && (cadena2.trim().length()==0)){
                    return constructore;
                }else if (constructore.cadenap2 == cadena2){
                    return constructore;
                }
            }
        }
        
        String cb = ""+name2+"";
        for(FuncionO ccc:this.constructores){
            if(ccc.nombre.equals(cb)){
                if(cadena2.trim().length()==0 && ccc.cadenap2.length()==0){
                    return ccc;
                }else if(cadena2==ccc.cadenap2){
                    return ccc;
                }
            }
        }

        for (FuncionO maine : this.maines){
            if (name2.replace("i", ":").replace(":", "i").trim().equals(maine.nombre.replace("i", ":").replace(":", "i"))){
                return maine;
            }
        }
        
        if(this.maines.size()>0){
            if(cb.toLowerCase().equals("main")){
                return this.maines.get(0);
            }
        }
        
        return null;
    }
    
    public FuncionO getFuncion_NPARAMETROS(String lafuncion, int nparams){
        for(FuncionO fff:this.funciones){
            if(fff.nombre.equals(lafuncion)==true){
                if(fff.parametros.size()==nparams){
                    return fff;
                }
            }
        }
        
        for(FuncionO ccc:this.constructores){
            if(ccc.nombre.equals(lafuncion)==true){
                if(ccc.parametros.size()==nparams){
                    return ccc;
                }
            }
        }   
        
        return null;
    }
}
