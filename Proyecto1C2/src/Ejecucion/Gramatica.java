/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import LSR.Desplazamientos;
import LSR.Estado;
import LSR.Reducciones;
import java.util.ArrayList;
import java.util.HashMap;
import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class Gramatica {
    
    //<editor-fold desc="Atributos de la clase">    
    
    //Para el manejo de las producciones
    public HashMap<String, Produccion> listaProducciones;
    public HashMap<Integer, Regla> listReglas;
    
    //Para el manejo de los terminales y no terminales
    public HashMap<String, Terminal> listaTerminales;
    public HashMap<String, NoTerminal> listaNterminales;    
    
    //String nombre del terminal, int = nivel :D
    public HashMap<String, Integer> listaPrecedencia;
    public int nivel_precedencia=0;
    
    //Para manejar el tipo de asocitividad
    public HashMap<String, Terminal> listaIzq;
    public HashMap<String, Terminal> listaDer;
    
    //No Terminal, donde inicia la gramatica
    public String inicial;
    public Integer contadorReglas;
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Constructores">
    
    public Gramatica(){
        this.contadorReglas=1;
    }    
    
    public void iniciarListas(){
        this.listaTerminales = new HashMap<String, Terminal>();
        this.listaNterminales = new HashMap<String, NoTerminal>();
        this.listaPrecedencia = new HashMap<String, Integer>();
        this.listaIzq = new HashMap<String, Terminal>();
        this.listaDer = new HashMap<String, Terminal>();
        this.nivel_precedencia=1;
        this.inicial="";
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Manejo de Listas">
    
    private void guardarError(String t, int l, int c, String ti, String d){
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        Compilador.listaErrores.add(new Errores(t,l,c,ti,d));
        Compilador.acciones += "" + t + l + c + ti + d + "\n";
    }
                
    public void guardarTerminal(Terminal ter, Nodo nodo){       
        
        if(this.listaNterminales.containsKey(ter.name)){
            this.guardarError(ter.name, nodo.linea, nodo.columna, "Semantico", "Ya existe en NoTerminales");
        }else{
            if(this.listaTerminales.containsKey(ter.name)){
                this.guardarError(ter.name, nodo.linea, nodo.columna, "Semantico", "Ya existe en Terminales");
            }else{
                this.listaTerminales.put(ter.name, ter);
                Compilador.acciones += "Se Guardo el Terminal:  " + ter.name + "   De Tipo:  " + ter.tipo + "\n";
            }            
        }
    }
    
    public void guardarNterminal(NoTerminal nter, Nodo nodo){
        if(this.listaTerminales.containsKey(nter.name)){
           this.guardarError(nter.name, nodo.linea, nodo.columna, "Semantico", "Ya existe en Terminales");
        }else{
            if(this.listaNterminales.containsKey(nter.name)){
                this.guardarError(nter.name, nodo.linea, nodo.columna, "Semantico", "Ya existe en NoTerminales");
            }else{
                this.listaNterminales.put(nter.name, nter);
                Compilador.acciones += "Se Guardo el No Terminal:  " + nter.name + "  De Tipo:  " + nter.tipo + "\n";
            }            
        }        
    }
    
    public void guardarIzq(String snombre, Nodo nodo){   
        Terminal tempo = this.getTerminal(snombre);
        if(tempo != null){
            if(this.listaDer.containsKey(tempo.name)){
                String descri="Terminal ya existe en asociatividad derecha";
                this.guardarError(tempo.name, nodo.linea, nodo.columna, "Semantico",descri);
            }else{                
                if(this.listaIzq.containsKey(tempo.name)==false){
                    this.listaIzq.put(tempo.name, tempo);
                    Compilador.acciones += "El Terminal:  " + tempo.name + "  Tiene Asociatividad a la IZQUIERDA  " + "\n";
                }                
            }            
        }else{
            String descri="Terminal no declarado";
            this.guardarError(snombre, nodo.linea, nodo.columna, "Semantico",descri);
        }
    }
    
    public void guardarDer(String snombre, Nodo nodo){
        Terminal tempo = this.getTerminal(snombre);
        if(tempo!=null){
            if(this.listaIzq.containsKey(tempo.name)){
                String descri="Terminal ya existe en asociatividad Izquierda";
                this.guardarError(tempo.name, nodo.linea, nodo.columna, "Semantico",descri);
            }else{
                if(this.listaDer.containsKey(tempo.name)==false){
                    this.listaDer.put(tempo.name, tempo);
                    Compilador.acciones += "El Terminal:  " + tempo.name + "  Tiene Asociatividad a la DERECHA  " + "\n";
                } 
            }            
        }else{
            String descri="Terminal no declarado";
            this.guardarError(snombre, nodo.linea, nodo.columna, "Semantico",descri);
        }
    }
    
    public Terminal getTerminal(String pname)
    {return listaTerminales.get(pname.toLowerCase());}
    
    public NoTerminal getNterminal(String pname)
    {return listaNterminales.get(pname.toLowerCase());}
    
    public Produccion getProduccion(String snombre)
    {return this.listaProducciones.get(snombre.toLowerCase());}
    
    public String getS_Gramatica(){
        String reto ="\n\n! ============================	Gramatica\n";
        reto+=this.produccionAumentada.getSproduccion()+"\n";
        
        for(Produccion tempo:this.listaProducciones.values())
        {
            if(!this.produccionAumentada.izquierda.equals(tempo.izquierda))
            {
                reto+=tempo.getSproduccion()+"\n";
            }            
        }
        return reto;
    }
    
    public String getS_ReglasEnumeradas()
    {
        String reto = "";
        for(Produccion pro:this.listaProducciones.values())
        {
            for(Regla regla:pro.listaReglas)
            {
                reto+=regla.id+"    "+regla.sizquierda+" ->\t";
                reto+=regla.getSregla()+"\t\t\tkernel: "+regla.kernel+"\n";
            }
        }        
        return reto;
    }
    
    public String getS_Estados()
    {
        String reto ="\n\n! ============================	Estados\n";
        for(Estado tempo:this.listaEstados.values())
        {
            reto+=tempo.getSestado()+"\n";
        }        
        return reto;
    }
    
    public String getS_EstadosPrimeros()
    {
        String reto ="\n\n! ============================	Estados, Primeros\n";
        for(Estado tempo:this.listaEstados.values())
        {
            reto+=tempo.getSestado_Primeros()+"\n";
        }        
        return reto;
    }
    
    public String getS_EstadosPrimerosSiguientes()
    {
        String reto ="\n\n! ============================	Estados, Primeros, Siguientes\n";
        for(Estado tempo:this.listaEstados.values())
        {
            reto+=tempo.getSestado_Primeros_Siguientes()+"\n";
        }        
        return reto;
    }
    
    public String getS_EstadosPrimerosSiguientesReducciones()
    {
        String reto ="\n\n! ============================	Estados, Primeros, Siguientes, Reducciones\n";
        for(Estado tempo:this.listaEstados.values())
        {
            reto+=tempo.getSestado_Primeros_Siguientes_Reducciones()+"\n";
        }        
        return reto;
    }
    
    
    //</editor-fold>
    
    
    // <editor-fold desc="LALR Fase1: Aumentar Gramatica">
    
    public HashMap<String, String> buscandoPrimeros;
    public HashMap<String, Primeros> listaPrimeros;
    
    public void iniciaHashes_Primeros(){
        if(this.buscandoPrimeros==null){
            this.buscandoPrimeros= new HashMap<String, String>();
        }
        if(this.listaPrimeros==null){
            this.listaPrimeros = new HashMap<String, Primeros>();
        }        
    }
    
    public void f1_crearprimeros(){
        //  instanciar hashes a utilizar
        this.iniciaHashes_Primeros();
        for(Regla regla_tempo: this.listReglas.values()){
            this.buscarPrimeros(regla_tempo);
        }
    }
    
    public void buscarPrimeros(Regla pregla){
        if(pregla.heredada==false && pregla.kernel==0){
            this.guardar_en_BuscandoPrimeros(pregla);
            this.guardar_en_PrimeroHash(pregla);            
            Simbolo simbolo_tempo = pregla.getSimbolo(0);
            if(simbolo_tempo.es_terminal){
                this.guardarPrimero(pregla, simbolo_tempo);
            }else{
                if(pregla.sizquierda.equals(simbolo_tempo.name)){
                    
                }else{                    
                    if(this.buscandoPrimeros.containsKey(simbolo_tempo.name)){
                        this.copiarPrimeros(simbolo_tempo.name, pregla.sizquierda);
                    }else{
                        for(Regla regla_tempo2:this.listReglas.values())
                        {
                            if(regla_tempo2.sizquierda.equals(simbolo_tempo.name))
                            {
                                this.buscarPrimeros(regla_tempo2);
                                this.copiarPrimeros(simbolo_tempo.name, pregla.sizquierda);
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    public void guardar_en_PrimeroHash(Regla pregla){
        if(this.listaPrimeros.containsKey(pregla.sizquierda)==false){
            this.listaPrimeros.put(pregla.sizquierda,new Primeros(pregla.sizquierda));
        }
    }
    
    public void guardar_en_BuscandoPrimeros(Regla pregla){
        if(this.buscandoPrimeros.containsKey(pregla.sizquierda)==false){
            this.buscandoPrimeros.put(pregla.sizquierda, pregla.sizquierda);
        }
    }
    
    public void guardarPrimero(Regla pregla, Simbolo psimbolo){
        if(this.listaPrimeros.containsKey(pregla.sizquierda)==true){
            Primeros primeros = this.listaPrimeros.get(pregla.sizquierda);
            primeros.guardarFirst(psimbolo.name);
        }
    }
    
    public void copiarPrimeros(String remi, String desti){
        Primeros remitente= this.listaPrimeros.get(remi);
        if(remitente!=null){
            Primeros destinatario = this.listaPrimeros.get(desti);
            if(destinatario!=null){
                for(String tempo:remitente.listaFirst.values()){
                    destinatario.guardarFirst(tempo);
                }
            }
        }
    }
    
    public Produccion produccionAumentada;
    
    public void f1_aumentarGramatica(){
        this.produccionAumentada=new Produccion();
        this.produccionAumentada.sizquierda="s";
        
        if(this.listaNterminales.containsKey(this.produccionAumentada.sizquierda)){
            if(this.listaNterminales.containsKey("salpha")){
                this.produccionAumentada.sizquierda="sbeta";
            }else{
                this.produccionAumentada.sizquierda="salpha";
            }
        }
        
        //valuar que exista la produccion inicial
        Produccion produccion_inicial = this.getProduccion(this.inicial);
        
        if(produccion_inicial==null){
            this.guardarError("inicial",0,0,"Semantico","No existe produccion inicial");
            return;
        }
        
        NoTerminal not_aumentada = new NoTerminal(this.produccionAumentada.sizquierda);
        
        this.produccionAumentada.izquierda=not_aumentada;
        
        this.guardarNterminal(not_aumentada,null);
        
        this.produccionAumentada.listaReglas=new ArrayList<Regla>();
        this.produccionAumentada.esInicial=false;
        
        Regla regla_aumentada = new Regla();
        regla_aumentada.iniciarTodo(this.produccionAumentada.sizquierda);
        regla_aumentada.ntasociado=not_aumentada;
        
        Simbolo simbolo_inicial = new Simbolo(this.inicial);
        simbolo_inicial.es_terminal=false;

        regla_aumentada.simbolos.add(simbolo_inicial);
        
        regla_aumentada.id=0;
        
        this.listReglas.put(regla_aumentada.id, regla_aumentada);
        
        this.produccionAumentada.listaReglas.add(regla_aumentada);
        
        this.listaProducciones.put(this.produccionAumentada.sizquierda, this.produccionAumentada);
        
        System.out.println("\n\nGramatica Aumentada");                
        
    }
    
   // </editor-fold>
    
    
    //<editor-fold desc="LALR Fase2: Crear el Estado 0">
    
    public HashMap<Integer, Estado> listaEstados;
    
    Estado e000;
    public void f2_crearEstado000(){
        //this.listaEstados=new HashMap<Integer, Estado>();
        
        Estado estado_nuevo= new Estado();
        
        estado_nuevo.iniciaHashes_Cerradura(0);   
        
        //this.listaEstados.put(estado_nuevo.id, estado_nuevo);
        
        Regla regla_a = this.produccionAumentada.listaReglas.get(0);
        Regla regla_aumentada = new Regla();
        regla_aumentada.iniciarTodo();

        this.duplicarRegla(regla_a,regla_aumentada,true);                
        
        estado_nuevo.listaReglas.add(regla_aumentada);
               
        estado_nuevo.iniciaHashes_LH();
        
        regla_aumentada.heredada=true;
                
        estado_nuevo.guardar_en_BuscandoSiguiente(regla_aumentada.sizquierda);
        estado_nuevo.guardar_en_SiguienteHash(regla_aumentada.sizquierda);
        LookaHead lh = estado_nuevo.listaSiguientes.get(regla_aumentada.sizquierda);
        if(lh!=null){
            Terminal nuevo_terminal = new Terminal("[:fin:]", "null");
            this.guardarTerminal(nuevo_terminal,null);
            lh.guardarFollow("[:fin:]");
        }
        
        estado_nuevo.guardarExtendida(regla_aumentada.sizquierda);
                
        this.extenderEstado_o_Cerradura000(estado_nuevo);
        
        estado_nuevo.listaPrimeros=this.listaPrimeros;
        
        this.e000 = estado_nuevo;                
        
        System.out.println("\n\n Estado0 Creado :D");                
        
    }    
    
    public void f2_crearEstado0(){
        this.listaEstados=new HashMap<Integer, Estado>();
        
        Estado estado_nuevo= new Estado();
        
        estado_nuevo.iniciaHashes_Cerradura(0);   
        
        this.listaEstados.put(estado_nuevo.id, estado_nuevo);
        
        Regla regla_a = this.produccionAumentada.listaReglas.get(0);
        Regla regla_aumentada = new Regla();
        regla_aumentada.iniciarTodo();

        this.duplicarRegla(regla_a,regla_aumentada,true);                
        
        estado_nuevo.listaReglas.add(regla_aumentada);
               
        estado_nuevo.iniciaHashes_LH();
        
        regla_aumentada.heredada=true;
                
        estado_nuevo.guardar_en_BuscandoSiguiente(regla_aumentada.sizquierda);
        estado_nuevo.guardar_en_SiguienteHash(regla_aumentada.sizquierda);
        LookaHead lh = estado_nuevo.listaSiguientes.get(regla_aumentada.sizquierda);
        if(lh!=null){
            Terminal nuevo_terminal = new Terminal("[:fin:]", "null");
//            this.guardarTerminal(nuevo_terminal,null);
            lh.guardarFollow("[:fin:]");
        }
        
        estado_nuevo.guardarExtendida(regla_aumentada.sizquierda);
                
        this.extenderEstado_o_Cerradura(estado_nuevo);
        
        estado_nuevo.listaPrimeros=this.listaPrimeros;
        
        System.out.println("\n\n Estado0 Creado :D");   
        System.out.println(estado_nuevo.getSestado_Primeros_Siguientes()+"\n");
        
    }
    
    public void f2_primeros0(){
        Estado estado0 = this.listaEstados.get(0);
        if(estado0!=null){
            estado0.buscandoPrimeros=this.buscandoPrimeros;
            estado0.listaPrimeros=this.listaPrimeros;
            //xxxxxxx
////            estado0.calcularPrimeros();            
        }
        
    }   
    
    public void f2_siguientes000(){        
        if(this.e000!=null){
            this.e000.calcularSiguientesNuevo();
        }        
    }
    
    public void f2_siguientes0(){
        Estado estado0 = this.listaEstados.get(0);
        if(estado0!=null){
            
            //this.copiarSiguientes(pregla.sizquierda, simbolo_quiere_siguientes.name);
            //estado0.buscandoSiguientes=this.e000.buscandoSiguientes;
            //estado0.listaSiguientes=this.e000.listaSiguientes;
            //xxxxxxx
            estado0.calcularSiguientesNuevo();
            //this.unirFollows(estado, estado_nuevo);
            this.unirFollows(estado0, this.e000);
        }        
        
        
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="LALR Fase3: Crear Conjuntos de Estados">
    
    public void f3_crearConjuntos(){
        for (int i = 0; i < this.listaEstados.size(); i++){
            Estado estado_tempo = this.listaEstados.get(i);
            if(estado_tempo==null){continue;}
            
            for(Regla regla_estado: estado_tempo.listaReglas){
                if(regla_estado.evaluada){continue;}
                
                // crear simbolo de transicion
                Simbolo simbolo_trancision = regla_estado.getSimbolo(regla_estado.kernel);
                
                // crear un nuevo estado
                Estado estado_nuevo = new Estado();
                
                //crear un shift, guardarlo
                int i_estado_origen = estado_tempo.id;
                estado_nuevo.desplazamientos.guardarShift(i_estado_origen, simbolo_trancision);
                
                // marcar regla como evaluada
                regla_estado.evaluada=true;                
                
                this.pasarRegla(estado_tempo, regla_estado, estado_nuevo);
                
                // comprobar que si hay mas trasiciones de reglas con ese mismo simbolos
                // en las reglas del estado actual = estado_tempo
                this.pasarMasReglas(estado_tempo, estado_nuevo, simbolo_trancision);
                
                // Calcular Cerradura :D
                this.extenderEstado_o_Cerradura(estado_nuevo);
                
                // Calcular Primeros
                estado_nuevo.listaPrimeros=this.listaPrimeros;
                //estado_nuevo.calcularPrimeros();
                
                // calcular siguiente
                estado_nuevo.calcularSiguientesNuevo();
                //estado_nuevo.buscandoSiguientes=this.e000.buscandoSiguientes;
                //estado_nuevo.listaSiguientes=this.e000.listaSiguientes;
                
                this.unirFollows(estado_nuevo, this.e000);
                                
                // comparar si hay estado similar
                this.compararEstados(estado_nuevo, i_estado_origen, simbolo_trancision);
                
                // if estadonuevo. union = false, 
                // guardo el estado nuevo
                if(estado_nuevo.union==false)
                {                    
                    estado_nuevo.id=this.listaEstados.size();
                    this.listaEstados.put(estado_nuevo.id, estado_nuevo);
                    System.out.println("\n\n! ============================  guardar Estado:  "+estado_nuevo.id);
                    System.out.println(estado_nuevo.getSestado_Primeros_Siguientes()+"\n");
                }                
            }
        }
    }        
    
    public void duplicarRegla(Regla remi, Regla desti, Boolean extendiendo){        
        if(extendiendo){
            desti.kernel=0;
        }else{
            desti.kernel=Integer.parseInt(remi.kernel.toString());
        }        
        desti.id=Integer.parseInt(remi.id.toString());
        desti.nReduccion=Integer.parseInt(remi.nReduccion.toString());
        desti.heredada=Boolean.parseBoolean(remi.heredada.toString());
        desti.evaluada=Boolean.parseBoolean(remi.evaluada.toString());
        desti.extendida=Boolean.parseBoolean(remi.extendida.toString());
        desti.esReduccion=Boolean.parseBoolean(remi.esReduccion.toString());

        desti.sizquierda=remi.sizquierda;        
        for(Simbolo tempo:remi.simbolos){
            desti.simbolos.add(tempo);
        }        
    }
    
    public void extenderEstado_o_Cerradura000(Estado estado_nuevo){
        for(Produccion pro_buscada:this.listaProducciones.values()){
            for(Regla regla_pro:pro_buscada.listaReglas){
                Regla regla_nueva_de_prod= new Regla();
                regla_nueva_de_prod.iniciarTodo();                          
                this.duplicarReglaExtendiendo(regla_pro, regla_nueva_de_prod, true);
                estado_nuevo.listaReglas.add(regla_nueva_de_prod);
            }            
        }                        
    }
    
    public void extenderEstado_o_Cerradura(Estado estado_nuevo){
        Regla regla_tempo = this.reglaSinExtender(estado_nuevo);
        while(regla_tempo!=null){
            // evaluar para colocarla como evaluada, reduccion
            if(regla_tempo.kernel==regla_tempo.simbolos.size()){
                regla_tempo.evaluada=true;
                regla_tempo.esReduccion=true;
                regla_tempo.extendida=true;
                regla_tempo = this.reglaSinExtender(estado_nuevo);
                continue;
            }
                        
            //obtener simbolo con index = kernel
            Simbolo simbolo_t = regla_tempo.getSimbolo(regla_tempo.kernel);
            if(simbolo_t.es_terminal){
                regla_tempo.extendida=true;
            }else{
                //si simbolono esta en hash extendidas
                if(estado_nuevo.listaExtendidas.containsKey(simbolo_t.name)){
                    regla_tempo.extendida=true;
                }else{
                    //buscar esa produccion que genera el simbolo
                    Produccion pro_buscada = this.listaProducciones.get(simbolo_t.name);
                    //guardar sus reglas pro_buscada en reglas del estado
                    //clonar con kernel = 0 en reglas del estado.
                    for(Regla regla_pro:pro_buscada.listaReglas){
                        Regla regla_nueva_de_prod= new Regla();
                        regla_nueva_de_prod.iniciarTodo();                           
                        this.duplicarReglaExtendiendo(regla_pro, regla_nueva_de_prod, true);
                        estado_nuevo.listaReglas.add(regla_nueva_de_prod);                        
                    }                        
                    //por que ya se evaluo si extender o no
                    regla_tempo.extendida=true;
                    //guardar simbolo .name en lista extendidas
                    estado_nuevo.guardarExtendida(simbolo_t.name);
                }
            }
            regla_tempo = this.reglaSinExtender(estado_nuevo);
        }
    }
    
    public Regla reglaSinExtender(Estado estado){
        for(Regla regla:estado.listaReglas){
            if(regla.extendida==false){
                return regla;
            }
        }     
        return null;
    }
    
    public void duplicarReglaExtendiendo(Regla remi, Regla desti, Boolean extendiendo){
        if(extendiendo){
            desti.kernel=0;
        }else{
            desti.kernel=Integer.parseInt(remi.kernel.toString());
        }        
        desti.id=Integer.parseInt(remi.id.toString());
        desti.nReduccion=Integer.parseInt(remi.nReduccion.toString());
        desti.heredada=false;
        desti.evaluada=false;
        desti.extendida=false;
        desti.esReduccion=false;
                        
        desti.sizquierda=remi.sizquierda;        
        for(Simbolo tempo:remi.simbolos){
            desti.simbolos.add(tempo);
        }        
    } 

    public void pasarRegla(Estado estado, Regla regla_estado, Estado estado_nuevo){
        // crear regla heredada, se colocara en el nuevo estado
        Regla regla_heredada = new Regla(regla_estado.sizquierda);

        // guardar la regla heredada en estado nuevo
        estado_nuevo.listaReglas.add(regla_heredada);
        
        // duplicar datos regla_estado en regla_heredada
        regla_heredada.convertirHeredada(regla_estado.kernel+1);
        regla_heredada.id=Integer.parseInt(regla_estado.id.toString());
        
        // copiar los simbolos
        this.duplicarRegla_Simbolos(regla_estado, regla_heredada);
        
        // verificar si sera reduccion la nueva regla
        if(regla_heredada.kernel==regla_heredada.simbolos.size()){
            regla_heredada.evaluada=true;
            regla_heredada.esReduccion=true;
            regla_heredada.extendida=true;
        }
        
        estado_nuevo.buscandoSiguientes=this.e000.buscandoSiguientes;
        estado_nuevo.listaSiguientes=this.e000.listaSiguientes;
        
    }
    
    public void pasarMasReglas(Estado estado_tempo, Estado estado_nuevo, Simbolo simbolo_transicion1){
        for(Regla regla_tempo:estado_tempo.listaReglas){
            if(regla_tempo.evaluada==true){continue;}
            
            Simbolo simbolo_transicion2=regla_tempo.getSimbolo(regla_tempo.kernel);
            if(simbolo_transicion2.name.equals(simbolo_transicion1.name)){
                regla_tempo.evaluada=true;
                this.pasarRegla(estado_tempo, regla_tempo, estado_nuevo);
            }
        }
    }
    
    public void duplicarRegla_Simbolos(Regla remi, Regla desti){
        for(Simbolo tempo:remi.simbolos){
            desti.simbolos.add(tempo);
        } 
    }    
    
    @SuppressWarnings({"BoxedValueEquality", "NumberEquality"})
    public void compararEstados(Estado estado_nuevo, int iestado_origen, Simbolo simbolo_transicion){
        estado_nuevo.union =false;
        for(Estado estado: this.listaEstados.values()){
            estado_nuevo.limpiarSimilares();
            estado.limpiarSimilares();
            
            // tamanio de reglas iguales
            if(estado.listaReglas.size()!=estado_nuevo.listaReglas.size())
            {continue;} //continuar al siguente estado
            
            // simbolo de shift iguales
            if(this.compararSimbolosShift(estado.desplazamientos, estado_nuevo.desplazamientos)==false)
            {continue;} //continuar al siguente estado
            
            // validar los kernel en reglas
            for(Regla reglan: estado_nuevo.listaReglas){
                if(reglan.similar==false){
                    for(Regla reglae:estado.listaReglas){
                        if(reglae.similar==false){
                            if(reglan.kernel == reglae.kernel){
                                if(this.compararSimbolos(reglae, reglan)){
                                    reglae.similar=true;
                                    reglan.similar=true;
                                }                                    
                            }
                        }
                    }
                }
            }
            
            // comprobar si exito en todas las reglas
            if(estado.exitoSimilares() && estado_nuevo.exitoSimilares()){       
                estado_nuevo.desplazamientos.guardarShift(iestado_origen, simbolo_transicion);
                estado.desplazamientos.guardarShift(iestado_origen, simbolo_transicion);
                        
                // unir los follows
                // estado_nuevo en estado
                this.unirFollows(estado, estado_nuevo);
                
                // marcar que existe un estado similar
                estado_nuevo.union=true;
                return;
            }                        
        }
    }
    
    public Boolean compararSimbolosShift(Desplazamientos estado, Desplazamientos nestado){
        estado.iniciarSimbolosShift();
        nestado.iniciarSimbolosShift();                        
        for(String simbolo: nestado.simbolosShift.values()){
            if(estado.simbolosShift.containsKey(simbolo)==false)
            {return false;}
        }
        return true;
    }
    
    public Boolean compararSimbolos(Regla reglae, Regla reglan){
        if(reglae.simbolos.size()!=reglan.simbolos.size()){
            return false;
        }        
        
        // desplazar el kernel de uno y si ese existe;
        for (int i = 0; i < reglae.simbolos.size(); i++){
            Simbolo simbolo_reglae = reglae.getSimbolo(i);
            Simbolo simbolo_reglan = reglan.getSimbolo(i);
            if(simbolo_reglae.name!=simbolo_reglan.name){
                return false;
            }
        }        
        return true;
    }
    
    public void unirFollows(Estado estado, Estado nestado){
        for(LookaHead siguientesn: nestado.listaSiguientes.values()){            
            //obtener los siguentes de estado
            for(String ssimbolo: siguientesn.listaFollow.values()){
                estado.guardaSiguiente(ssimbolo, siguientesn.sizquierda);
            }
        }
    }
    
    
   // </editor-fold>
    
    
    // <editor-fold desc="LALR Fase3.2: Crear Reducciones">
    
    public void f3_crearReducciones(){
        for(Estado estado:this.listaEstados.values()){
            estado.reducciones=new Reducciones();
            this.calcularReducciones(estado);
        }
        System.out.println("\n\n! ============================	Reducciones Creadas\n");
    }
    
    public void calcularReducciones(Estado pestado){   
        // recorrer las reglas
        // si la regla es reduccion calcular reduccion
        for(Regla regla: pestado.listaReglas){
            if(regla.esReduccion==true){
                LookaHead lh = pestado.listaSiguientes.get(regla.sizquierda);
                if(lh!=null){
                    for(String ssimbolo:lh.listaFollow.values()){
                        pestado.reducciones.guardarReduce(ssimbolo, regla.id);
                    }
                }                
            }
        }
    }
    
    
    //</editor-fold>
    
}
