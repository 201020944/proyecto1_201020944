/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.Objects;
import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class EvaluarE {
    
    private void guardarError(String t, int l, int c, String ti, String d){
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        Compilador.listaErrores.add(new Errores(t,l,c,ti,d));
    } 
    
    public Object evaluarExpresion(Nodo nodoactual){
        String name = nodoactual.name;
        
        switch (name){
            case "??":  
                return this.ev_l_xor(nodoactual);
            case "||":  
                return this.ev_l_or(nodoactual);            
            case "&&":  
                return this.ev_l_and(nodoactual);
            case "!!":  
                return this.ev_l_not(nodoactual);
            case "==": 
                return this.ev_r_igual2(nodoactual);
            case "!=":  
                return this.ev_r_diferente(nodoactual);
            case ">":   
                return this.ev_r_mayor(nodoactual);
            case "<":   
                return this.ev_r_menor(nodoactual);
            case ">=":  
                return this.ev_r_mayorigual(nodoactual);
            case "<=":  
                return this.ev_r_menorigual(nodoactual);
            case "+":   
                return this.ev_a_suma(nodoactual);
            case "-":   
                return this.ev_a_resta(nodoactual);
            case "*":   
                return this.ev_a_multiplicar(nodoactual);
            case "/":   
                return this.ev_a_dividir(nodoactual);
            case "^":   
                return this.ev_a_potenciar(nodoactual);
            case "++":  
                return this.ev_a_aumento(nodoactual);
            case "--":  
                return this.ev_a_decremento(nodoactual);               
            
            case "valor":
                try{
                    Object retorno_valor =  this.ev_valor(nodoactual);   
                    return retorno_valor;
                }catch(Exception ex){
                    return 1;
                }              
                
            default:
                System.out.println("Default, evaluarExpresion: "+ name);
                break;
        }
        return 1;
    }
    
    private Object ev_l_xor(Nodo nodo)
    {
        Object izq = this.evaluarExpresion(nodo.hijos.get(0));
        Object der = this.evaluarExpresion(nodo.hijos.get(1));
        
        if(this.correcto(5, izq) && this.correcto(5, der))
        {
            Boolean a = this.getBool(izq);
            Boolean b = this.getBool(der);
            return (a==true && b ==false) || (b==true && a ==false);
        }        
        String descri="Critico xor, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no son operandos logicos";
        this.guardarError("??", nodo.linea, nodo.columna, "Semantico", descri);
        return false;
    }
    
    private Object ev_l_or(Nodo nodo)
    {
        Object izq = this.evaluarExpresion(nodo.hijos.get(0));
        Object der = this.evaluarExpresion(nodo.hijos.get(1));                
        if(this.correcto(5, izq) && this.correcto(5, der))
        {
            Boolean a = this.getBool(izq);
            Boolean b = this.getBool(der);
            return (a || b);
        }
        String descri="Critico or, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no son operandos logicos";
        this.guardarError("||", nodo.linea, nodo.columna, "Semantico", descri);
        return false;
    }
    
    private Object ev_l_and(Nodo nodo)
    {
        Object izq = this.evaluarExpresion(nodo.hijos.get(0));
        Object der = this.evaluarExpresion(nodo.hijos.get(1));
        if(this.correcto(5, izq) && this.correcto(5, der))
        {
            Boolean a = getBool(nodo, izq);
            Boolean b = getBool(nodo, der);
            return (a && b);
        }                               
        String descri="Critico and, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no son operandos logicos";
        this.guardarError("&&",nodo.linea,nodo.columna,"Semantico",descri);
        return false;
    }
    
    private Object ev_l_not(Nodo nodoactual)
    { 
        Object uni = this.evaluarExpresion(nodoactual.hijos.get(0));
        if(this.correcto(5, uni))
        {
            Boolean a = this.getBool(nodoactual, uni);
            return !a;
        }        
        String descri="Critico not, "+this.stipoObjeto(uni)+"no es operando logico";
        this.guardarError("!!",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;
    }
    
    private Object ev_r_igual2(Nodo nodoactual)
    {
        System.out.println("*** ev_igual2");
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
                
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, "==", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) == (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) == this.toAscii(der));
        }   
        String descri="Critico igual2, "+this.stipoObjeto(izq)+" vs "+this.stipoObjeto(der)+" no operandos relacionales";        
        this.guardarError("==",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
    
    private Object ev_r_diferente(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, "!=", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) != (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) != this.toAscii(der));
        }        
        String descri="Critico diferente, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError("!=",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
    
    private Object ev_r_mayor(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, ">", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) > (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) > this.toAscii(der));
        }
        
        String descri="Critico mayorque, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError(">",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
        
    private Object ev_r_menor(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, "<", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) < (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) < this.toAscii(der));
        }        
        String descri="Critico menorque, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError("<",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
              
    private Object ev_r_mayorigual(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, ">=", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) >= (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) >= this.toAscii(der));
        }
        
        String descri="Critico mayorIgual, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError(">=",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
    
    private Object ev_r_menorigual(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, "<=", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) <= (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) <= this.toAscii(der));
        }
        
        String descri="Critico menorIgual, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError("<=",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }        
    
    private Object ev_a_suma(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return (Integer)izq + (Integer)der;
                case 2:
                    return Double.valueOf(izq.toString()) + (Double)der;
                case 3:
                    return izq.toString() + der.toString();
                case 4:
                    return (Integer)izq + this.toAscii(der);  
                case 5:
                    return (Integer)izq + this.toLogico(der); 
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);                    
                    this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return Double.valueOf(izq.toString())+ Double.valueOf(der.toString());     
                case 2: 
                    return (Double)izq + (Double)der;
                case 3:
                    return izq.toString() + der.toString();
                case 4:
                    Object double_char = this.toAscii(der);
                    return (Double)izq + Double.valueOf(double_char.toString()); 
                case 5:
                    Object double_bool = this.toLogico(der);
                    return (Double)izq + Double.valueOf(double_bool.toString());
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);
                    this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if((tipo_iz==3) || (tipo_de==3))
        {
            return izq.toString()+der.toString();
        }
//        if((tipo_iz==3 && tipo_de!=5) || (tipo_iz!=5 && tipo_de==3))
//        {
//            return izq.toString()+der.toString();
//        }
        if(tipo_iz==4)
        {
            switch (tipo_de)
            {
                case 1: 
                    return toAscii(izq) + (Integer)der;
                case 2:
                    Object izq_char = toAscii(izq);
                    return Double.valueOf(izq_char.toString()) + (Double)der;
                case 3:
                    return izq.toString() + der.toString();
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);
                    this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return this.toLogico(izq) + (Integer)der;     
                case 2: 
                    Object r5=this.toLogico(izq);
                    return Double.valueOf(r5.toString()) + (Double)der;                    
                case 5: 
                    return (Boolean)izq || (Boolean)der;
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);
                    this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);
        this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
        
    private Object ev_a_resta(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return (Integer)izq - (Integer)der;
                case 2:
                    return Double.valueOf(izq.toString()) - (Double)der;
                case 4:
                    return (Integer)izq - this.toAscii(der);  
                case 5:
                    return (Integer)izq - this.toLogico(der); 
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);                    
                    this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return Double.valueOf(izq.toString())-Double.valueOf(der.toString());     
                case 2: 
                    return (Double)izq - (Double)der;
                case 4:
                    Object double_char = this.toAscii(der);
                    return (Double)izq - Double.valueOf(double_char.toString()); 
                case 5:
                    Object double_bool = this.toLogico(der);
                    return (Double)izq - Double.valueOf(double_bool.toString());
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);
                    this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if(tipo_iz==4)
        {
            switch (tipo_de)
            {
                case 1: 
                    return toAscii(izq) - (Integer)der;
                case 2:
                    Object izq_char = toAscii(izq);
                    return Double.valueOf(izq_char.toString()) - (Double)der;
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);
                    this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return this.toLogico(izq) - (Integer)der;     
                case 2: 
                    Object r5=this.toLogico(izq);
                    return Double.valueOf(r5.toString()) - (Double)der;
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);
                    this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);
        this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
    
    private Object ev_a_multiplicar(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return (Integer)izq * (Integer)der;
                case 2:
                    return Double.valueOf(izq.toString()) * (Double)der;
                case 4:
                    return (Integer)izq * this.toAscii(der);  
                case 5:
                    return (Integer)izq * this.toLogico(der); 
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);                    
                    this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return Double.valueOf(izq.toString()) * Double.valueOf(der.toString());     
                case 2: 
                    return (Double)izq * (Double)der;
                case 4:
                    Object double_char = this.toAscii(der);
                    return (Double)izq * Double.valueOf(double_char.toString()); 
                case 5:
                    Object double_bool = this.toLogico(der);
                    return (Double)izq * Double.valueOf(double_bool.toString());
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);
                    this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if(tipo_iz==4)
        {
            switch (tipo_de)
            {
                case 1: 
                    return toAscii(izq) * (Integer)der;
                case 2:
                    Object izq_char = toAscii(izq);
                    return Double.valueOf(izq_char.toString()) * (Double)der;
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);
                    this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return this.toLogico(izq) * (Integer)der;     
                case 2: 
                    Object r5=this.toLogico(izq);
                    return Double.valueOf(r5.toString()) * (Double)der;                    
                case 5: 
                    return (Boolean)izq && (Boolean)der;
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);
                    this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);
        this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
    
    private Object ev_a_dividir(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));        
        
        int valor = this.getInt2(der);
        if(valor == 0)
        {
            String descri="division entre 0";                    
            this.guardarError("/",nodoactual.linea,nodoactual.columna,"Ejecucion",descri); 
            return 1;
        }
        
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return (Double.valueOf(izq.toString()) / (Double.valueOf(der.toString())));
                case 2:
                    return (Double.valueOf(izq.toString()) / (Double)der);
                case 4:
                    Object r14 = this.toAscii(der);
                    return (Double.valueOf(izq.toString()) / (Double.valueOf(r14.toString())));
                case 5:
                    Object r15 = this.toLogico(der);
                    return (Double.valueOf(izq.toString()) / (Double.valueOf(r15.toString())));
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);                    
                    this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return (Double.valueOf(izq.toString()) / (Double.valueOf(der.toString())));
                case 2: 
                    return (Double)izq / (Double)der;
                case 4:
                    Object double_char = this.toAscii(der);
                    return (Double)izq / Double.valueOf(double_char.toString()); 
                case 5:
                    Object double_bool = this.toLogico(der);
                    return (Double)izq / Double.valueOf(double_bool.toString());
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);
                    this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if(tipo_iz==4)
        {
            Object r4= this.toAscii(izq);
            switch (tipo_de)
            {
                case 1: 
                    return (Double.valueOf(r4.toString()) / (Double.valueOf(der.toString())));
                case 2:                    
                    return (Double.valueOf(r4.toString()) / (Double)der);
                case 5:
                    Object r45 = this.toLogico(der);
                    return (Double.valueOf(r4.toString()) /  Double.valueOf(r45.toString()));
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);
                    this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {        
            Object r5 = this.toLogico(izq);
            switch (tipo_de) 
            {
                case 1:
                    return Double.valueOf(r5.toString()) / Double.valueOf(der.toString());
                case 2: 
                    return Double.valueOf(r5.toString()) / (Double)der;
                case 4: 
                    Object r4 = this.toAscii(der);
                    return Double.valueOf(r5.toString()) / Double.valueOf(r4.toString());
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);
                    this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);
        this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
    
    private Object ev_a_potenciar(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return Math.pow(Double.valueOf(izq.toString()), (Double.valueOf(der.toString())));
                case 2:
                    return Math.pow(Double.valueOf(izq.toString()) , (Double)der);
                case 4:
                    Object double_char = this.toAscii(der);
                    return Math.pow((Double)izq, Double.valueOf(double_char.toString()));
                case 5:
                    Object double_bool = this.toLogico(der);
                    return Math.pow((Double)izq,Double.valueOf(double_bool.toString()));
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);                    
                    this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1:                     
                    return Math.pow(Double.valueOf(izq.toString()), (Double.valueOf(der.toString())));
                case 2: 
                    return Math.pow((Double)izq , (Double)der);
                case 4:
                    Object double_char = this.toAscii(der);
                    return Math.pow((Double)izq, Double.valueOf(double_char.toString()));
                case 5:
                    Object double_bool = this.toLogico(der);
                    return Math.pow((Double)izq,Double.valueOf(double_bool.toString()));
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);
                    this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if(tipo_iz==4)
        {
            Object r4= this.toAscii(izq);
            switch (tipo_de)
            {
                case 1: 
                    return Math.pow(Double.valueOf(r4.toString()) , (Double.valueOf(der.toString())));
                case 2:                    
                    return (Double.valueOf(r4.toString()) / (Double)der);
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);
                    this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {        
            Object r5 = this.toLogico(izq);
            switch (tipo_de) 
            {
                case 1:
                    return Math.pow(Double.valueOf(r5.toString()) , Double.valueOf(der.toString()));
                case 2: 
                    return Math.pow(Double.valueOf(r5.toString()) ,(Double)der);
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);
                    this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);
        this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
    
    private Object ev_a_aumento(Nodo nodoactual)
    {
        Nodo hijo0 = nodoactual.hijos.get(0);        
        //si paso es otra onda pero no modifica variables
        Object uni = this.evaluarExpresion(nodoactual.hijos.get(0));
        int tipo_uni = this.tipoObjeto(uni);
        
        if(tipo_uni==1)
        {
            return (Integer)uni +1;
        }
        else if(tipo_uni==2)
        {
            if(uni.toString().contains("."))
            {
                String sdouble = uni.toString();
                String[] adouble = sdouble.split("\\.");
                int posiciones = adouble[1].length();
                Double precision = Math.pow(10, posiciones);
                Double decimales = 1 / precision;
                return (Double)uni+decimales;
            }
            else
            {
                return (Double)uni+0.1;
            }
        }
        else if(tipo_uni==4)
        {
            return toAscii(uni) + 1;
        }
        else
        {
            String err = "++, Combinacion invalida "+uni.toString();
            this.guardarError("++",nodoactual.linea,nodoactual.columna,"semantico",err); 
            return 1;   
        }
    }
    
    private Object ev_a_decremento(Nodo nodoactual)
    {
        Nodo hijo0 = nodoactual.hijos.get(0);
        //si paso es otra onda pero no modifica variables
        Object uni = this.evaluarExpresion(nodoactual.hijos.get(0));
        int tipo_uni = this.tipoObjeto(uni);
        
        if(tipo_uni==1)
        {
            return (Integer)uni-1;
        }
        else if(tipo_uni==2)
        {
            if(uni.toString().contains("."))
            {
                String sdouble = uni.toString();
                String[] adouble = sdouble.split("\\.");
                int posiciones = adouble[1].length();
                Double precision = Math.pow(10, posiciones);
                Double decimales = 1 / precision;
                return (Double)uni-decimales;
            }
            else
            {
                return (Double)uni-0.1;
            }
        }
        else if(tipo_uni==4)
        {
            return toAscii(uni) - 1;
        }
        else
        {
            String err = "--, Combinacion invalida "+uni.toString();
            this.guardarError("--",nodoactual.linea,nodoactual.columna,"semantico",err); 
            return 1;   
        }
    }
    
    public Object ev_valor(Nodo nodoactual)
    {
        Nodo tipo = nodoactual.hijos.get(0);        
        Nodo valor =nodoactual.hijos.get(1);
        
        System.out.println(tipo.name);
        System.out.println("ev_valor, ants del switch: "+valor.name);        
        switch (tipo.name.toLowerCase()) 
        {
            case "int":
                String valor_int = valor.name.replace(" ", "");
                if(valor_int.contains("-")==true)
                {
                    String ttt= valor_int.replace("-", "");
                    Object r1= Integer.valueOf(ttt)*-1;
                    System.out.println("****** salio del int negativo");
                    return r1;
                }
                else
                {
                    Object r1= Integer.valueOf(valor_int);
                    System.out.println("****** salio del int");
                    return r1;
                }
            case "double":
                String valor_double = valor.name.replace(" ", "");
                if(valor_double .contains("-")==true)
                {
                    String ttt= valor_double .replace("-", "");
                    Object r1= Double.valueOf(ttt)*-1;
                    System.out.println("****** salio del double negativo");
                    return r1;
                }
                else
                {
                    Object r1= Double.valueOf(valor_double );
                    System.out.println("****** salio del double");
                    return r1;
                }
            case "string":
                return valor.name;
            case "char":                
                char cad[]=valor.name.toCharArray();
                System.out.println("****** salio del char");
                return cad[0];            
            case "bool":
                Object result5 = Boolean.valueOf(valor.name);
                System.out.println("****** salio del bool");
                return result5;
                //break;
            default:
                System.out.println("****** default ev_valor");
                return 1;
        }
    }
    
    //<editor-fold desc="Otros">
    
    public boolean correcto(int tipo_variable, Object ob)
    {
        int tipo_objeto=this.tipoObjeto(ob);
        
        switch (tipo_variable) 
        {
            case 1:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int default
                    case 4: return true;    //char pero en modo ascii
                    case 5: return true;    //booleano -> logico
                    default:    break;
                }break;
            case 2:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int
                    case 2: return true;    //double default
                    default:    break;
                }break;
            case 3:
                if(tipo_objeto==3){return true;}    //string default
                break;
            case 4:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int, de ascii a char
                    case 4: return true;    //char default
                    default:    break;
                }break;
            case 5:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int
                    case 5: return true;    //bool default
                    default:    break;
                }break;
            default:    break;
        }
        return false;
    } 
    
    public int tipoObjeto(Object ob)
    {
        if(ob instanceof Integer){return 1;}
        else if(ob instanceof Double){return 2;}
        else if(ob instanceof String){return 3;}
        else if(ob instanceof Character){return 4;}
        else if(ob instanceof Boolean){return 5;}
        return 0;
    }    
        
    public String stipoObjeto(Object ob)
    {
        if(ob instanceof Integer){return "int";}
        else if(ob instanceof Double){return "double";}
        else if(ob instanceof String){return "string";}
        else if(ob instanceof Character){return "char";}
        else if(ob instanceof Boolean){return "bool";}
        return "";
    }
    
    private char toCaracter(int c)
    {
        return (char)c;
    }

    private int toAscii(char c)
    {
        int ascii = (int)c;
        return ascii; 
    }
    
    public int getInt(Object ob)
    {
       switch (tipoObjeto(ob))
       {
           case 1:
               return (int)ob;
           case 4:
               return toAscii((char)ob);
           case 5:
               if ((boolean)ob)
               {
                   return 1;
               }
               else
               {
                   return 0;
               }                    
           default:
               System.out.println("Error get Int");
               return 1;
       }
    }
    
    public int getInt2(Object ob)
    {
       switch (tipoObjeto(ob))
       {
           case 1:
               return (int)ob;
           case 2:;
               Object result = Double.valueOf(ob.toString());
               int sali = ((Number)result).intValue();
               return sali;
           case 4:
               return toAscii((char)ob);
           case 5:
               if ((boolean)ob)
               {
                   return 1;
               }
               else
               {
                   return 0;
               }         
           default:
               System.out.println("Error get Int2");
               return 1;
       }
    }
    
    public double getDouble(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 1:
                return Double.valueOf(ob.toString());
            case 2:
                return (Double)ob;
            default:
                System.out.println("Error get Double");
                return 1.0;
        }
    }
    
    public char getChar(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 1:
                return toCaracter((int)ob);
            case 4:
                return (char)ob;
            case 5:
                if ((boolean)ob)
                {
                    return toCaracter(1);
                }
                else
                {
                    return toCaracter(0);
                }
            default:
                System.out.println("Error get Char");
                return 'c';
        }
    }
    
    public Boolean getBool(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 5:
                return (boolean)ob;
            case 1:
                if ((int)ob == 1){return true;}
                else if ((int)ob == 0){return false;}
                String descri = "1, 0 pueden ser logico y bool";
                this.guardarError(ob.toString(), 0, 0, "Ejecucion", descri);
                return false;
            default: return false;
        }
    }
    
    public Boolean getBool(Nodo nodoactual,Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 5:
                return Boolean.valueOf(ob.toString());
            case 1:
                if ((int)ob == 1){return true;}
                else if ((int)ob == 0){return false;}
                String descri1 = "1, 0 pueden ser logico y bool";
                this.guardarError(ob.toString(), 0, 0, "Ejecucion", descri1);
            default: 
                String descri2=this.stipoObjeto(ob)+" no es bool";
                this.guardarError(ob.toString(), nodoactual.linea, nodoactual.columna, "Ejecucion", descri2);
                return false;
        }
    } 
    
    private int sumasAscii(Object o)
    {
        String cadena1=o.toString();
        //retorna la suma de todos los caracteres en ascii del char o string
        cadena1 = cadena1.replace("'", "");
        char[] chars1 = cadena1.toCharArray();
        int retorno=0;
        for (int i = 0; i < chars1.length; i++)
        {
            retorno+= toAscii(chars1[i]);
        }        
        return retorno;
    }
    
    private int toLogico(Object ob)
    {
        if(this.tipoObjeto(ob)==5)
        {
            if((Boolean)ob== true)
            {
                return 1;
            }
            else if((Boolean)ob==false)
            {
                return 0;
            }
        }        
        return 0;
    }  
    
    private int toAscii(String s)
    {
        char c = s.charAt(0); // c='a'
        int ascii = (int)c;
        return ascii; 
    }
         
    private int toAscii(Object ob)
    {
        char c=(Character)ob;
        int ascii = (int)c;
        return ascii; 
    }
    
    private boolean numerosRelacionales(Object a, String operador, Object b)
    {
        int ti = this.tipoObjeto(a);    
        int td = this.tipoObjeto(b);
        
        switch (ti) 
        {
            case 1:
                if(td==1)
                {
                    switch (operador) 
                    {
                        case "==":
                            return (Objects.equals(Integer.valueOf(a.toString()), Integer.valueOf(b.toString())));
                        case "!=":
                            return !(Objects.equals(Integer.valueOf(a.toString()), Integer.valueOf(b.toString())));
                        case ">":
                            return Integer.valueOf(a.toString()) > Integer.valueOf(b.toString());
                        case "<":
                            return Integer.valueOf(a.toString()) < Integer.valueOf(b.toString());
                        case ">=":
                            return Integer.valueOf(a.toString()) >= Integer.valueOf(b.toString());
                        case "<=":
                            return Integer.valueOf(a.toString()) <= Integer.valueOf(b.toString());
                        default: break;
                    }
                }
                else
                {
                    switch (operador) {
                        case "==":
                            return (Objects.equals(Double.valueOf(a.toString()), Double.valueOf(b.toString())));
                        case "!=":
                            return !(Objects.equals(Double.valueOf(a.toString()), Double.valueOf(b.toString())));
                        case ">":
                            return Double.valueOf(a.toString()) > Double.valueOf(b.toString());
                        case "<":
                            return Double.valueOf(a.toString()) < Double.valueOf(b.toString());
                        case ">=":
                            return Double.valueOf(a.toString()) >= Double.valueOf(b.toString());
                        case "<=":
                            return Double.valueOf(a.toString()) <= Double.valueOf(b.toString());
                        default: break;
                    }
                }
            
            case 2:
                switch (operador) {
                        case "==":
                            return (Objects.equals(Double.valueOf(a.toString()), Double.valueOf(b.toString())));
                        case "!=":
                            return !(Objects.equals(Double.valueOf(a.toString()), Double.valueOf(b.toString())));
                        case ">":
                            return Double.valueOf(a.toString()) > Double.valueOf(b.toString());
                        case "<":
                            return Double.valueOf(a.toString()) < Double.valueOf(b.toString());
                        case ">=":
                            return Double.valueOf(a.toString()) >= Double.valueOf(b.toString());
                        case "<=":
                            return Double.valueOf(a.toString()) <= Double.valueOf(b.toString());
                        default: break;
                    }
                
            default:
                System.out.println("DEFAULT NUMEROS IGUALES");                
        }
        return false;
    }
    
}
