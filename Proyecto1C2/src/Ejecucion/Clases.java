/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class Clases {
    
    //<editor-fold desc="Atributos de la clase">
    
    public String name;
    public Nodo cuerpo;
    public boolean esHeredada;
    public String heredade;
    public static HashMap<String,Variable>lista_variables;
    public static LinkedList<Funcion> listaFM;
    
    
    //</editor-fold>
    
    
      // <editor-fold desc="Contructores"> 
    
    public Clases(){}
    
    public Clases(String pname){
        this.name=pname.toLowerCase();
        this.lista_variables=new HashMap<String,Variable>();
        this.listaFM = new LinkedList<Funcion>();
    }
    
    
    public Clases(String pname, Nodo pcuerpo){
        this.name=pname.toLowerCase();
        this.cuerpo=pcuerpo;
        
    }    
    
    public void convertirHeredada(String ch){        
        this.esHeredada = true;        
        this.lista_variables=new HashMap<String,Variable>();
        this.listaFM = new LinkedList<Funcion>();
        this.heredade = ch;
    }
    
    public void convertirNohereda(){        
        this.esHeredada = false;        
        this.lista_variables=new HashMap<String,Variable>();
        this.listaFM = new LinkedList<Funcion>();
    }
    
    
    // </editor-fold>
    
    //<editor-fold desc="Manejo de Listas">
    
    private void guardarError(String t, int l, int c, String ti, String d){
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        Compilador.listaErrores.add(new Errores(t,l,c,ti,d));
    }        
    
    //</editor-fold>
    
}
