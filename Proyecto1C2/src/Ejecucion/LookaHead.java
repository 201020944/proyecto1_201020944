/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.HashMap;

/**
 *
 * @author LuisAroldo
 */
public class LookaHead {
    public String sizquierda;                       //no terminal asociado
    public HashMap<String, String> listaFollow;     //siguientes al no terminal asociado
    
    public LookaHead(){
        if(this.listaFollow==null){
            this.listaFollow=new HashMap<String, String>();
        }        
    }
    
    public LookaHead(String pizq){
        this.sizquierda=pizq;
        if(listaFollow==null){
            this.listaFollow=new HashMap<String, String>();
        }        
    }
    
    public void guardarFollow(String psimbolo){
        if(this.listaFollow.containsKey(psimbolo)==false){
            this.listaFollow.put(psimbolo, psimbolo);
        }
    }
    
    public String getSlookahead(){
        if(this.listaFollow==null){
            return "";
        }
        
        String reto="";
        for(String tempo:this.listaFollow.values()){
            reto+=" "+tempo;
        }        
        return reto;
    }
    
    public String getSsiguientes(){
        if(this.listaFollow==null){
            return "";
        }
        
        String reto="";
        for(String tempo:this.listaFollow.values()){
            reto+=" "+tempo;
        }        
        return reto;
    }
    
}
