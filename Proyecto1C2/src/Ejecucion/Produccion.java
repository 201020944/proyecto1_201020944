/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.ArrayList;

/**
 *
 * @author LuisAroldo
 */
public class Produccion {
    public NoTerminal izquierda;
    public String sizquierda;
    public ArrayList<Regla> listaReglas;
    public Boolean esInicial;
    
    public Produccion(){}
    
    public Produccion(NoTerminal pnoterminal){
        this.sizquierda=pnoterminal.name.toLowerCase();
        this.izquierda=pnoterminal;
        this.listaReglas=new ArrayList<Regla>();
        this.esInicial=false;
    }
    
     public String getSproduccion(){
        String reto=this.sizquierda+" ->\t";
        Integer contador = 0;
        
        for(Regla tempo:this.listaReglas){
            if(contador==0){
                reto+=tempo.getSregla()+"\n";
                contador=1;
            }else{
                reto+= "   |\t"+tempo.getSregla()+"\n";
            }
        }                
        return reto;
    }
}
