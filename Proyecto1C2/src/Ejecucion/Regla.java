/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author LuisAroldo
 */
public class Regla {
    public Integer id;
    public String sizquierda;    
    public Integer kernel, nReduccion;
    public Boolean heredada, evaluada, extendida, esReduccion, similar;
    public ArrayList<Simbolo> simbolos;
    public NoTerminal ntasociado;
    
    public Regla(){}
    
    public Regla(String psenca){
        this.sizquierda=psenca;
        this.simbolos=new ArrayList<Simbolo>();       
    }
    
    public void iniciarTodo(String pse){
        this.id=-1;
        this.sizquierda=pse;
        this.kernel=0;
        this.heredada=false;
        this.evaluada=false;
        this.extendida =false;
        this.esReduccion=false;
        this.similar=false;
        this.nReduccion=-1;        
        this.simbolos=new ArrayList<Simbolo>();
    }
    
    public void iniciarTodo(){
        this.id=-1;
        this.sizquierda="";
        this.kernel=0;
        this.heredada=false;
        this.evaluada=false;
        this.extendida =false;
        this.esReduccion=false;
        this.similar=false;
        this.nReduccion=-1;        
        this.simbolos=new ArrayList<Simbolo>();
    }
    
    public String getSregla(){
        String reto = "";        
        for(Simbolo tempo:this.simbolos){
            reto+=tempo.name+" ";
        }        
        return reto;
    }
    
    public String getSregla_primeros(HashMap<String, Primeros> listaPrimeros){        
        String reto = "";
        
        if(listaPrimeros!=null){
            for(Simbolo tempo:this.simbolos){
                reto+=tempo.name+" ";
            }
            Primeros primeros = listaPrimeros.get(this.sizquierda);
            if(primeros!=null){
                reto+="\t\t\tPrimeros: "+primeros.getSprimeros();
            }
        }                
        return reto;
    }
    
    public String getSregla_Primeros_Siguientes(HashMap<String, Primeros> listaPrimeros, HashMap<String, LookaHead> listaSiguientes){        
        String reto = "";
        
        if(listaPrimeros!=null){
            for(Simbolo tempo:this.simbolos){
                reto+=tempo.name+" ";
            }
            Primeros primeros = listaPrimeros.get(this.sizquierda);
            if(primeros!=null){
                reto+="\t\tPrimeros: "+primeros.getSprimeros();
            }
            LookaHead siguientes=listaSiguientes.get(this.sizquierda);
            if(siguientes!=null){
                reto+="\t\tSiguientes: "+siguientes.getSsiguientes();
            }
        }                
        return reto;
    }
    
    public String getSregla_Reduccion(){
        if(this.esReduccion==true){return "\t\tReduccion: "+"R"+this.id;}
        return "";
    }
    
    public Simbolo getSimbolo(Integer posicion){        
        return this.simbolos.get(posicion); 
    }
    
    public void convertirHeredada(int pkernel){
        this.id=-1;
        this.kernel=pkernel;
        this.heredada=true;
        this.evaluada=false;
        this.extendida=false;
        this.esReduccion=false;
        this.similar=false;
        this.nReduccion=-1;
    }
}
