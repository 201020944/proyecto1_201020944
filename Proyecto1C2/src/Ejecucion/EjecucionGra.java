/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import LSR.NodoS;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Stack;
import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class EjecucionGra {
    //<editor-fold desc="Atributos de la clase">
    
    public HashMap<String,Clases> lista_Clases;
    private HashMap<String,Variable> listaLocal;
    private Stack<HashMap<String,Variable>> lista_lista;
    public javax.swing.JEditorPane consola;
    
    NodoS superpadre;
    private int break_salir;
    private LinkedList<Integer> listaSalir;
    
    private TablaSimbolos ts;
    private int osx_pasada, osx_temporal, osx_etiqueta;
    private int osx_auxtempo, osx_auxtipo;
    String osx_auxlugar;
    StringBuilder salidaNormal;
    StringBuilder salida3D;
    
    StringBuilder salidaNormalDouble;
    
    String claseactual;
    //StringBuilder salida3DDouble;
    //</editor-fold>

    
    //<editor-fold desc="Constructores">
    
    
    public EjecucionGra(HashMap<String,Variable> lv, HashMap<String,Clases> cla)
    {
        this.listaLocal=lv;
        this.lista_Clases=cla;
        this.lista_lista=new Stack<HashMap<String,Variable>>();
        this.break_salir= 0;
        this.listaSalir= new LinkedList<Integer>();
        this.listaSalir.add(break_salir);     
        this.lista_lista.push(lv);
        
        this.osx_pasada=1;
        this.osx_temporal=0;
        this.osx_etiqueta=0;
        this.osx_auxtempo=0;
        this.osx_auxtipo=0;
        this.osx_auxlugar="stack";
        this.salidaNormal = new StringBuilder();
        this.salida3D = new StringBuilder();
        
        this.salidaNormalDouble = new StringBuilder();
        //this.salida3DDouble = new StringBuilder();
        
        this.claseactual = "";
        this.ts=new TablaSimbolos();
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Manejo de Listas">
    
    private void guardarError(String t, int l, int c, String ti, String d)
    {
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        Compilador.listaErrores.add(new Errores(t,l,c,ti,d));
    }            
    
    private void ambito_Mas()
    {
        HashMap<String,Variable> lista=new HashMap<String,Variable>();
        this.lista_lista.push(lista);
        this.listaLocal=lista;        
    }
    
    private void ambito_Menos()
    {
        if(this.lista_lista.size()>0)
        {
            this.lista_lista.pop();
            if(this.lista_lista.size()>0)
            {
                this.listaLocal=this.lista_lista.peek();
            }
        }
    }
    
    private void ambito_Mas(HashMap<String,Variable> plista)
    {
        this.lista_lista.push(plista);
        this.listaLocal=plista;
    }
    
    private void guardarVariable(Variable vnueva, Nodo nodoactual){
        if(this.listaLocal.containsKey(vnueva.name.toLowerCase())){
            String descri="variable ya existe";
            this.guardarError(vnueva.name, nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
        }else{
            this.listaLocal.put(vnueva.name.toLowerCase(), vnueva);
            Compilador.acciones += "ACCIONES: se guardo la variable: " + vnueva.name + " de tipo: " + vnueva.stipo + "\n";
        }
    }
    
    private Variable getVariable(String pnombre){
        //return this.listaLocal.get(pnombre.toLowerCase());
        Variable reto=null;
        for(HashMap<String,Variable> item: this.lista_lista){
            if(item != null){
                reto = item.get(pnombre.toLowerCase());
            }            
            if(reto!=null){
                return reto;
            }
        }
        return null;
    }
    
    
    public void break_aumento()
    {
        this.break_salir++;
        this.listaSalir.addFirst(break_salir);
    }
    
    public void break_encontrado()
    {
        this.listaSalir.removeFirst();
    }
    
    public void break_decremento()
    {
        if(this.break_salir==this.listaSalir.getFirst())
        {
            this.break_salir--;
            this.listaSalir.removeFirst();
        }
        else
        {
            this.break_salir--;
        }                
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Ejecucion"> 
    
    public void Ejecutar_1_iniciar(NodoS nodo)
    {
        //nodo es el nodo aumentado
        //Se tienen que ejecutar desde el nodo inicial
        //nodo inicial es el hijo de nodo aumentado
        
        this.Ejecutar_2_derivacion(nodo.hijos.get(0), nodo);
        
//        if(opcion==1 || opcion==3)
//        {
//            this.Ejecutar_2_derivacion(nodo.hijos.get(0), nodo);
//        }
//        else
//        {
//            //Para el recorrido de tabla de Simbolos
//            this.Ejecutar_2_derivacion(nodo.hijos.get(0), nodo);
//            
//            //Para el recorrido de traduccion de los INIT
//            this.Ejecutar_2_derivacion(nodo.hijos.get(0), nodo);
//            
//            //Para el recorrido de traduccion a Codigo 3D
//            this.Ejecutar_2_derivacion(nodo.hijos.get(0), nodo);
//            
//            //Imprimir el valor Contenido de codigo 3 Direcciones
//            this.print(this.salida3D.toString());
//            
//            //Graficador g = new Graficador();
//           // g.crearC3D(this.salidaNormal, this.salidaNormalDouble);
//        }
        Compilador.acciones += this.salidaNormal.toString();
      //  this.consola.setText(this.salidaNormal.toString());
    }
    
    private void Ejecutar_2_derivacion(NodoS nodo, NodoS padre)
    {
        //Ejecuto la primera accion del nodo
        if(nodo.accionPRIME!=null)
        {
            this.superpadre=padre;
            this.ambito_Mas();
            this.Ejecutar_3_acciones(nodo.accionPRIME);
            this.ambito_Menos();
        }  
        
        //Recorrido: izquierda, derecha, raiz        
        //Recorro lo hijos de izquierda a derecha
        for(NodoS tempo:nodo.hijos)
        {
            this.superpadre=padre;
            this.Ejecutar_2_derivacion(tempo, nodo);
        }
                      
        if(nodo.accion!=null)
        {
            this.superpadre=padre;
            this.ambito_Mas();
            this.Ejecutar_3_acciones(nodo.accion);
            this.ambito_Menos();
        }
    }
    
    private void Ejecutar_3_acciones(Nodo nodoactual)
    {
        //String name = nodoactual.name.toLowerCase();
        String name = nodoactual.name;
        //System.out.println("Ejecutar_3 "+name);
        switch (name) 
        {
            case "sentencias_a":
                for(Nodo hijo:nodoactual.hijos)
                {
                    this.Ejecutar_3_acciones(hijo);
                }
                break;
                
            case "asig_hijo":  
                //System.out.println("asig_hijo 3");
                int index_h = Integer.valueOf(nodoactual.hijos.get(0).name);
                Object valor_h = this.evaluarExpresion(nodoactual.hijos.get(1));
                this.superpadre.setHijoValor(index_h, valor_h);
                break;
                
            case "asig_hijo_a":
                //System.out.println("asig_hijo_a 3");
                int index_h_a = Integer.valueOf(nodoactual.hijos.get(0).name);
                String name_h_a = nodoactual.hijos.get(1).name;
                Object valor_h_a = this.evaluarExpresion(nodoactual.hijos.get(2));
                this.superpadre.setHijoAtributo(index_h_a, name_h_a, valor_h_a);
                break;
                
            case "asig_padre":
                //System.out.println("asig_padre 3");
                Object valor_p = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.superpadre.setPadreValor(valor_p);
                break;
                
            case "asig_padre_a":
                //System.out.println("asig_padre_a 3");
                Object valor_p_a = this.evaluarExpresion(nodoactual.hijos.get(1));
                String name_p_a = nodoactual.hijos.get(0).name;
                this.superpadre.setPadreAtributo(name_p_a, valor_p_a);
                break;
                
            case "dec_variables":
                this.capturarDeclaracion(nodoactual);
                break;
                
            case "dec_vec_arr":
                this.capturarDeclaracion_Vector(nodoactual);
                break;                            
                
            case "decla_asignacion":
                this.capturarDeclaAsigna(nodoactual);
                break;
                
            case "asignacion":
                this.capturarAsignacion(nodoactual);
                break;
                
            case "asignacion_vector":
                this.capturarAsignacion_Vector(nodoactual);
                break;
                
            case "print":
                Object valor_print = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.print(valor_print);
                break;                
                     
            case "llamada_1":                
                //Buscar el metodo
                Funcion fu1 = this.get_mf_vacio(this.claseactual, nodoactual.hijos.get(0).name);
                if(fu1==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                this.Ejecutar_5_mf(fu1);
                break;
                
            case "llamada_2":
                Funcion fu2 = this.get_mf_multis(this.claseactual,nodoactual);
                if(fu2==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                this.Ejecutar_5_mf(fu2);
                break;
                
            case "++":
                this.evaluarExpresion(nodoactual);
                break;
                
            case "--":
                this.evaluarExpresion(nodoactual);
                break;
                
            case "if1":
                this.capturar_if1(nodoactual);
                break;
                
            case "if2":
                this.capturar_if2(nodoactual);
                break;
                
            case "if3":
                this.capturar_if3(nodoactual);
                break;
                
            case "if4":
                this.capturar_if4(nodoactual);
                break;
                
            case "switch":
                this.capturar_switch(nodoactual);
                break;
                 
            case "while":
                this.capturar_while(nodoactual);
                break;
                
            case "do_while":
                this.capturar_dowhile(nodoactual);
                break;
                
            case "repeat":
                this.capturar_repeat(nodoactual);
                break;
                
            case "for":
                this.capturar_for(nodoactual);
                break;
                
            case "loop":
                this.capturar_loop(nodoactual);
                break;
                
            default:
                System.out.println(nodoactual.name);
            System.out.println("! ============================================================ Default, Ejecutar_3");
        }        
    }
          
    private void capturar_if1(Nodo nodoactual) {
        Nodo nodo_if = nodoactual.hijos.get(0);
        Object ovalor = this.evaluarExpresion(nodo_if.hijos.get(0));
        //System.out.println("Paso por IF 1");
        if(this.correcto(5, ovalor)==true)
        {
            if(this.getBool(ovalor)==true)
            {
                this.ambito_Mas();
                this.Ejecutar_4_accionesControl(nodo_if.hijos.get(1));
                this.ambito_Menos();
            }
        }
        else
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo_if.linea, nodo_if.columna, "Ejecucion", descri);
        }        
    }
    
    private void capturar_if2(Nodo nodo)
    {
        
        //if, else
        Nodo nodo_if = nodo.hijos.get(0);
        Nodo nodo_else = nodo.hijos.get(1);
        Object ovalor = this.evaluarExpresion(nodo_if.hijos.get(0));
        //System.out.println("Paso por IF 2");
        if(this.correcto(5, ovalor)==false)
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo_if.linea, nodo_if.columna, "Ejecucion", descri);
            return;
        }
        
        if(this.getBool(ovalor)==true)
        {
            this.ambito_Mas();
            this.Ejecutar_4_accionesControl(nodo_if.hijos.get(1));
            this.ambito_Menos();
        }
        else
        {
            this.ambito_Mas();
            this.Ejecutar_4_accionesControl(nodo_else.hijos.get(0));
            this.ambito_Menos();
        }        
    }
    
    private void capturar_if3(Nodo nodo)
    {
        //if, elsees
        Nodo nodo_if = nodo.hijos.get(0);
        Nodo nodo_elses = nodo.hijos.get(1);
        Object ovalor = this.evaluarExpresion(nodo_if.hijos.get(0));
        //System.out.println("Paso por IF 3");        
        if(this.correcto(5, ovalor)==false)
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo_if.linea, nodo_if.columna, "Ejecucion", descri);
            return;
        }
        if(this.getBool(ovalor)==true)
        {
            this.ambito_Mas();
            this.Ejecutar_4_accionesControl(nodo_if.hijos.get(1));
            this.ambito_Menos();
            return;
        }
        //si paso esta le toca a los elses
        for(Nodo else_if:nodo_elses.hijos)
        {
            Object ovalor2 = this.evaluarExpresion(else_if.hijos.get(0));
            if(this.correcto(5, ovalor2))
            {
                if(this.getBool(ovalor2)==true)
                {
                    this.ambito_Mas();
                    this.Ejecutar_4_accionesControl(else_if.hijos.get(1));
                    this.ambito_Menos();
                    return;
                }
            }
            else
            {
                String descri="Condicion tiene que ser del tipo bool";
                this.guardarError(ovalor2.toString(), else_if.linea, else_if.columna, "Ejecucion", descri);
            }
        }
        
    }
    
    private void capturar_if4(Nodo nodo)
    {
        //if, elsees, else
        Nodo nodo_if = nodo.hijos.get(0);
        Nodo nodo_elses = nodo.hijos.get(1);
        Nodo nodo_else = nodo.hijos.get(2);
        Object ovalor = this.evaluarExpresion(nodo_if.hijos.get(0));
        //System.out.println("Paso por IF 4");
        if(this.correcto(5, ovalor)==false)
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo_if.linea, nodo_if.columna, "Ejecucion", descri);
            return;
        }
        if(this.getBool(ovalor)==true)
        {
            this.ambito_Mas();
            this.Ejecutar_4_accionesControl(nodo_if.hijos.get(1));
            this.ambito_Menos();
            return;
        }
        //si paso le toca a los elses
        for(Nodo else_if:nodo_elses.hijos)
        {
            Object ovalor2 = this.evaluarExpresion(else_if.hijos.get(0));
            if(this.correcto(5, ovalor2))
            {
                if(this.getBool(ovalor2)==true)
                {
                    this.ambito_Mas();
                    this.Ejecutar_4_accionesControl(else_if.hijos.get(1));
                    this.ambito_Menos();
                    return;
                }
            }
            else
            {
                String descri="Condicion tiene que ser del tipo bool";
                this.guardarError(ovalor2.toString(), else_if.linea, else_if.columna, "Ejecucion", descri);
            }
        }
        //si paso le toca al else
        this.ambito_Mas();
        this.Ejecutar_4_accionesControl(nodo_else.hijos.get(0));
        this.ambito_Menos();
    } 
    
    private void capturar_switch(Nodo nodo)
    {
        Object original = this.evaluarExpresion(nodo.hijos.get(0));
        if(original==null){return;}
        
        Nodo nodo_cases = nodo.hijos.get(1);
        Object valuando;
        
        this.ambito_Mas();
	this.break_aumento();
	int mibreak = this.break_salir;
        for(Nodo nodo_caso : nodo_cases.hijos)
        {
            if(nodo_caso.name.equalsIgnoreCase("caso"))
            {
                valuando = this.evaluarExpresion(nodo_caso.hijos.get(0));
                if(original.equals(valuando)==true)
                {
                    this.Ejecutar_4_accionesControl(nodo_caso.hijos.get(1));
                    if(mibreak>this.listaSalir.getFirst())
                    {
                        break;
                    }
                }
            }
            else if(nodo_caso.name.equalsIgnoreCase("default"))
            {
                //System.out.println("Pasa por default");
                this.Ejecutar_4_accionesControl(nodo_caso.hijos.get(0));
                if(mibreak>this.listaSalir.getFirst())
                {
                    break;
                }
            }
        }
        this.ambito_Menos();
        this.break_decremento();
    }
    
    private void capturar_while(Nodo nodo)
    {
        //WHILE::= fin rwhile:posi apar EXP:condi cpar rdo SCONTROL:sentencias fin rendwhile
        Object ovalor = this.evaluarExpresion(nodo.hijos.get(0));
        if(this.correcto(5, ovalor)==true)
        {
            this.ambito_Mas();
            this.break_aumento();
            int mibreak = this.break_salir;
            while(this.getBool(ovalor)==true)
            {
                this.Ejecutar_4_accionesControl(nodo.hijos.get(1));
                if(mibreak>this.listaSalir.getFirst())
		{
                    break;
		}
                ovalor = this.evaluarExpresion(nodo.hijos.get(0));                
                if(this.correcto(5, ovalor)==false)
                {
                    String descri="Condicion tiene que ser del tipo bool";
                    this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
                    break;
                }
            }
            this.ambito_Menos();
            this.break_decremento();
        }
        else
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
        }
    }
     
    private void capturar_dowhile(Nodo nodo)
    {
        //DO_WHILE::= fin rdo:posi SCONTROL:sentencias fin rend rwhile apar EXP:condi cpar                        
        Object ovalor = false;
        this.ambito_Mas();
        this.break_aumento();
        int mibreak = this.break_salir;
        do 
        {
            this.Ejecutar_4_accionesControl(nodo.hijos.get(0));            
            if(mibreak>this.listaSalir.getFirst())
            {
                break;
            }
            ovalor = this.evaluarExpresion(nodo.hijos.get(1));
            if(this.correcto(5, ovalor)==false)
            {
                String descri="Condicion tiene que ser del tipo bool";
                this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
                break;
            }
        } while (this.getBool(ovalor)==true);
        this.ambito_Menos();
        this.break_decremento();
    }
    
    private void capturar_repeat(Nodo nodo)
    {
        //REPEAT::= fin rrepeat:posi SCONTROL:sentencias fin renduntil apar EXP:condi cpar
        Object ovalor = true;
        this.ambito_Mas();
        this.break_aumento();
	int mibreak = this.break_salir;
        do 
        {
            this.Ejecutar_4_accionesControl(nodo.hijos.get(0));
            if(mibreak>this.listaSalir.getFirst())
            {
                break;
            }
            ovalor = this.evaluarExpresion(nodo.hijos.get(1));
            if(this.correcto(5, ovalor)==false)
            {
                String descri="Condicion tiene que ser del tipo bool";
                this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
                break;
            }
        } while (this.getBool(ovalor)==false);
        this.ambito_Menos();
        this.break_decremento();
    }
        
    private void capturar_for(Nodo nodo)
    {
        //FOR::= fin rfor:posi ASIGNACION_2:asigna rto EXP:condi rdo SCONTROL:sentencias fin rendfor
        //variable asignacion
            //debe estar ya declarada
            //tiene que ser entero        
        Nodo nodo_asigna = nodo.hijos.get(0);
        Nodo nodo_limite = nodo.hijos.get(1);
        Nodo nodo_acciones = nodo.hijos.get(2);
        //Buscando Variable
        Variable variable = this.getVariable(nodo_asigna.hijos.get(0).name);
        if(variable==null)
        {
            String descri = "Variable sin declarar";
            this.guardarError(nodo_asigna.hijos.get(0).name, nodo.linea, nodo.columna, "Ejecucion", descri);
            return;
        }
        if(variable.tipo!=1)
        {
            String descri = "Variable de control tiene que ser de tipo int";
            this.guardarError(variable.name, nodo.linea, nodo.columna, "Ejecucion", descri);
            return;
        }
        //Asignando Valor a Variable
        Object ovalor = this.evaluarExpresion(nodo_asigna.hijos.get(1));
        if(this.correcto(1, ovalor)==false)
        {
            String descri = "Variable " +variable.name+" de control tiene que tener valor int";
            this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
            return;
        }
        variable.valor=this.getInt(ovalor);
        //Obteniendo valor de limite :D
        Object olimite = this.evaluarExpresion(nodo_limite);
        if(this.correcto(1, olimite)==false)
        {
            String descri = "Limte for tiene que ser int";
            this.guardarError(olimite.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
            return;
        }
        
        //paso entonces si se ejecuta el for :D        
        int int_limite = this.getInt(olimite);
        this.ambito_Mas();
        this.break_aumento();
	int mibreak = this.break_salir;
        while((Integer)variable.valor <= int_limite)
        {
            this.Ejecutar_4_accionesControl(nodo_acciones);            
            if(mibreak>this.listaSalir.getFirst())
            {
                break;
            }
            variable.valor= (Integer)variable.valor+1;
        } 
        this.ambito_Menos();
	this.break_decremento();
    }
    
    private void capturar_loop(Nodo nodo)
    {
        this.ambito_Mas();
        this.break_aumento();
	int mibreak = this.break_salir;
        while(true)
        {
            this.Ejecutar_4_accionesControl(nodo.hijos.get(0));
            if(mibreak>this.listaSalir.getFirst())
            {
                break;
            }
        }
        this.ambito_Menos();
	this.break_decremento();
    }
    
    private void Ejecutar_4_accionesControl(Nodo nodoactual)
    {
        //String name = nodoactual.name.toLowerCase();
        String name = nodoactual.name;
        //System.out.println("Ejecutar_4 "+name);
        switch (name) 
        {
            case "sentencias_ac":
                for(Nodo hijo:nodoactual.hijos)
                {
                    if("break".equalsIgnoreCase(hijo.name)==true)
                    {
                        this.break_encontrado();
                        break;
                    }
                    this.Ejecutar_4_accionesControl(hijo);
                }
                break;
                
            case "asig_hijo":  
                //System.out.println("asig_hijo 4");
                int index_h = Integer.valueOf(nodoactual.hijos.get(0).name);
                Object valor_h = this.evaluarExpresion(nodoactual.hijos.get(1));
                this.superpadre.setHijoValor(index_h, valor_h);
                break;
                
            case "asig_hijo_a":
                //System.out.println("asig_hijo_a 4");
                int index_h_a = Integer.valueOf(nodoactual.hijos.get(0).name);
                String name_h_a = nodoactual.hijos.get(1).name;
                Object valor_h_a = this.evaluarExpresion(nodoactual.hijos.get(2));
                this.superpadre.setHijoAtributo(index_h_a, name_h_a, valor_h_a);
                break;
                
            case "asig_padre":
               // System.out.println("asig_padre 4");
                Object valor_p = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.superpadre.setPadreValor(valor_p);
                break;
                
            case "asig_padre_a":
                //System.out.println("asig_padre_a 4");
                Object valor_p_a = this.evaluarExpresion(nodoactual.hijos.get(1));
                String name_p_a = nodoactual.hijos.get(0).name;
                this.superpadre.setPadreAtributo(name_p_a, valor_p_a);
                break;
                
            case "declaracion":
                this.capturarDeclaracion(nodoactual);
                break;
                
            case "declaracion_vector":
                this.capturarDeclaracion_Vector(nodoactual);
                break;                            
                
            case "decla_asignacion":
                this.capturarDeclaAsigna(nodoactual);
                break;
                
            case "asignacion":
                this.capturarAsignacion(nodoactual);
                break;
                
            case "asignacion_vector":
                this.capturarAsignacion_Vector(nodoactual);
                break;
                
            case "print":
                Object valor_print = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.print(valor_print);
                break;
                
            case "llamada_1":                
                //Buscar el metodo
                Funcion fu1 = this.get_mf_vacio(this.claseactual,nodoactual.hijos.get(0).name);
                if(fu1==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                this.Ejecutar_5_mf(fu1);
                break;
                
            case "llamada_2":
                Funcion fu2 = this.get_mf_multis(this.claseactual,nodoactual);
                if(fu2==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                this.Ejecutar_5_mf(fu2);
                break;            
                 
            case "++":
                this.evaluarExpresion(nodoactual);
                break;
                
            case "--":
                this.evaluarExpresion(nodoactual);
                break;
                
            case "if1":
                this.capturar_if1(nodoactual);
                break;
                
            case "if2":
                this.capturar_if2(nodoactual);
                break;
                
            case "if3":
                this.capturar_if3(nodoactual);
                break;
                
            case "if4":
                this.capturar_if4(nodoactual);
                break;
                
            case "switch":
                this.capturar_switch(nodoactual);
                break;
                 
            case "while":
                this.capturar_while(nodoactual);
                break;
                
            case "do_while":
                this.capturar_dowhile(nodoactual);
                break;
                
            case "repeat":
                this.capturar_repeat(nodoactual);
                break;
                
            case "for":
                this.capturar_for(nodoactual);
                break;
                
            case "loop":
                this.capturar_loop(nodoactual);
                break;
                
            default:
                System.out.println(nodoactual.name);
            System.out.println("! ============================================================ Default, Ejecutar_4");
        }
    }    
    
    
    private void print(Object ovalor)
    {
        if(ovalor!=null)
        {
            //String anterior = this.consola.getText();
            //this.consola.setText(anterior+ovalor.toString()+"\n");
            //this.consola.setText(this.salidaNormal.toString()+"\n");
            this.salidaNormal.append(ovalor.toString());
            this.salidaNormal.append("\n");
            
            
            String temporal = ovalor.toString().replace("stack[t", "stack[(int)t");
            temporal = temporal.replace("int t", "double t");
            temporal = temporal.replace("int p", "double p");
            temporal = temporal.replace("int h", "double h");
            temporal = temporal.replace("heap[t", "heap[(int)t");
            temporal = temporal.replace("printf(\"%c\",t", "printf(\"%c\",(int)t");
            temporal = temporal.replace("printf(\"%d\",t", "printf(\"%d\",(int)t");
            this.salidaNormalDouble.append(temporal);
            this.salidaNormalDouble.append("\n");
        }
    }
    
    
    
    public Funcion get_mf_vacio(String ncla,String name)
    {
        for(Funcion fu:this.lista_Clases.get(ncla).listaFM)
        {
            if(fu.name.equalsIgnoreCase(name)==true)
            {
                //validar que los parametros sean 0
                if(fu.lista_parametros==null)
                {                    
                    Funcion copia = new Funcion();
                    this.duplicarFuncion(fu, copia);
                    copia.lista_variables=new HashMap<String,Variable>();
                    return copia;
                }
                if(fu.lista_parametros.isEmpty())
                {
                    Funcion copia = new Funcion();
                    this.duplicarFuncion(fu, copia);
                    copia.lista_variables=new HashMap<String,Variable>();
                    return copia;
                }
            }
        }
        return null;
    }
    
    public Funcion get_mf_multis( String name,Nodo nodoactual)
    {        
        Nodo nodo_parametros = nodoactual.hijos.get(1);
        String name_buscado = nodoactual.hijos.get(0).name;
        
        ArrayList<Object> lista_parametrizados = new ArrayList<>();        
        for(Nodo nodo_parametro:nodo_parametros.hijos)
        {
            lista_parametrizados.add(this.evaluarExpresion(nodo_parametro));
        }
        
        //buscar la funcion
        for(Funcion fu:this.lista_Clases.get(name).listaFM)
        {
            if(fu.name.equalsIgnoreCase(name_buscado)==true)
            {
                if(fu.verificarParametrizados(lista_parametrizados)==true)
                {
                    //Actualizar los parametros de la funcion
                    //fu.actualizarParametros(lista_parametrizados);
                    Funcion copia = new Funcion();
                    this.duplicarFuncion(fu, copia);
                    copia.actualizarParametros(lista_parametrizados);
                    return copia;
                }
            }
        }
        //si llego hasta aqui, no encontro la funcion
        return null;
    }
    
    private void duplicarFuncion(Funcion o, Funcion d)
    {
        d.cuerpo=o.cuerpo;
        d.esMetodo=o.esMetodo;
        //d.lista_parametros=o.lista_parametros;
        d.name=o.name;
        d.tipo=o.tipo;
        d.lista_parametros = new ArrayList<Variable>();
        for (Variable ori : o.lista_parametros) 
        {
            Variable nueva = new Variable();
            nueva.esVector=ori.esVector;
            nueva.name=ori.name;
            nueva.stipo=ori.stipo;
            nueva.tamanio=ori.tamanio;
            nueva.tipo=ori.tipo;
            nueva.valor=null;
            nueva.vector=ori.vector;
            d.lista_parametros.add(nueva);
        }        
    }
    
    private void Ejecutar_5_mf(Funcion fu)
    {
        fu.retorno=null;
        this.ambito_Mas(fu.lista_variables);
        this.Ejecutar_6_accionesmf(fu.cuerpo, fu);
        this.ambito_Menos();        
    }        
    
    private void Ejecutar_6_accionesmf(Nodo nodoactual, Funcion funcion)
    {
        String name = nodoactual.name;
        //System.out.println("Ejecutar_6 "+name);
        switch (name) 
        {
            case "sentencias_mf":
                for(Nodo nodohijo:nodoactual.hijos)
                {
                    if(funcion.retorno!=null){break;}
                    this.Ejecutar_6_accionesmf(nodohijo, funcion);
                    if(funcion.retorno!=null){break;}
                }
                break;
                
            case "declaracion":
                this.capturarDeclaracion(nodoactual);
                break;
                
            case "declaracion_vector":
                this.capturarDeclaracion_Vector(nodoactual);
                break;
                
            case "decla_asignacion":
                this.capturarDeclaAsigna(nodoactual);
                break;
                
            case "asignacion":
                this.capturarAsignacion(nodoactual);
                break;
                
            case "asignacion_vector":
                this.capturarAsignacion_Vector(nodoactual);
                break;
                
            case "asignacion_pasada":
                Object valor_pasada = this.evaluarExpresion(nodoactual.hijos.get(0));
                if(this.correcto(1, valor_pasada))
                {
                    this.osx_pasada=this.getInt2(valor_pasada);
                }
                else
                {
                    this.osx_pasada++;
                    String descri= "OSX_Pasada es del tipo int";
                    this.guardarError("OSX_Pasada", nodoactual.linea, nodoactual.columna, "Semantico",descri);
                }
                break;
                
            case "asignacion_temporal":
                Object valor_temporal = this.evaluarExpresion(nodoactual.hijos.get(0));
                if(this.correcto(1, valor_temporal))
                {
                    this.osx_pasada=this.getInt2(valor_temporal);
                }
                else
                {
                    this.osx_pasada++;
                    String descri= "OSX_Temporal es del tipo int";
                    this.guardarError("OSX_Temporal", nodoactual.linea, nodoactual.columna, "Semantico",descri);
                }
                break;
                
            case "asignacion_etiqueta":
                Object valor_etiqueta = this.evaluarExpresion(nodoactual.hijos.get(0));
                if(this.correcto(1, valor_etiqueta))
                {
                    this.osx_pasada=this.getInt2(valor_etiqueta);
                }
                else
                {
                    this.osx_pasada++;
                    String descri= "OSX_Etiqueta es del tipo int";
                    this.guardarError("OSX_Etiqueta", nodoactual.linea, nodoactual.columna, "Semantico",descri);
                }
                break;
                
            case "print":
                Object valor_print = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.print(valor_print);
                break;
                
            case "osx_print3d":
                Object valor_print3d = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.OSX_print3D(valor_print3d.toString());
                break;
            
            case "osx_saveerror":
                String oerror1 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String oerror2 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                this.guardarError(oerror1, this.superpadre.linea, this.superpadre.columna, "Semantico", oerror2);
                break;
                
            case "return":
                if(funcion.esMetodo==false)
                {
                    Object valor_reto = this.evaluarExpresion(nodoactual.hijos.get(0));
                    if(this.correcto(funcion.tipo, valor_reto)==true)
                    {
                        if(funcion.tipo==1){funcion.retorno=this.getInt(valor_reto);}
                        else if(funcion.tipo==2){funcion.retorno=this.getDouble(valor_reto);}
                        else if(funcion.tipo==3){funcion.retorno=valor_reto.toString();}
                        else if(funcion.tipo==4){funcion.retorno=this.getChar(valor_reto);}
                        else if(funcion.tipo==5){funcion.retorno =this.getBool(valor_reto);}
                    }
                    else
                    {
                        String descri="return de tipo incorrecto";
                        this.guardarError("return", nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
                        funcion.retorno=0;
                    }
                }
                else
                {
                    String descri=funcion.name+" no es del tipo funcion";
                    this.guardarError("return", nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
                    funcion.retorno=0;
                }                                
                break;
                
            case "llamada_1":
                //Buscar el metodo
                Funcion fu1 = this.get_mf_vacio(this.claseactual,nodoactual.hijos.get(0).name);
                if(fu1==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                this.Ejecutar_5_mf(fu1);
                break;
                
            case "llamada_2":
                Funcion fu2 = this.get_mf_multis(this.claseactual,nodoactual);
                if(fu2==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                this.Ejecutar_5_mf(fu2);
                break;
                
            case "osx_startclase":
                Object sclase1 = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.ts.startClase(sclase1.toString(), nodoactual);
                break;
                
            case "osx_saveclase":
                this.ts.saveClase();
                break;
            
            case "osx_startmain":
                String sclase2 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                this.ts.startMain(sclase2, nodoactual);
                break;
                
            case "osx_savemain":
                this.ts.saveMain();
                break;
                
            case "osx_saveatributo":
                String sclas3 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String satri3 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String stipo3 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String svisi3 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                this.ts.saveAtributo(sclas3, satri3, stipo3, svisi3, nodoactual);
                break;
                
            case "osx_startconstructor":
                String sclase4 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String scon4 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                this.ts.startConstructor(sclase4, scon4);
                break;
                
            case "osx_saveconstructor":
                this.ts.saveConstructor();
                break;                            
            
            case "osx_startfuncion":
                String sclase5 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String sfuncio5 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String stipo5 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String svisi5 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                this.ts.startFuncion(sclase5, sfuncio5, stipo5, svisi5, nodoactual);
                break;
                
            case "osx_savefuncion":
                this.ts.saveFuncion();
                break;
                
            case "osx_saveparametro":
                String sclase6 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String sfuncio6 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String sparam6 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String stipo6 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                this.ts.saveParametro(sclase6, sfuncio6, sparam6, stipo6, nodoactual);
                break;
                
            case "osx_setcadenaparametros":
                String cadenap7 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                this.ts.setCadenaParametros(cadenap7);
                break;

            case "osx_savevariable":
                String sclase8 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String sfuncio8 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String svar8 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String stipo8 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                this.ts.saveVariable(sclase8, sfuncio8, svar8, stipo8, nodoactual);
                break;               
            
            case "osx_printtabla":
                this.ts.soutTablaCSV();
                break;
                
            case "osx_imprimir":
                Object otam = this.evaluarExpresion(nodoactual.hijos.get(0));
                Object otemp = this.evaluarExpresion(nodoactual.hijos.get(1));
                Object otipo = this.evaluarExpresion(nodoactual.hijos.get(2));
                int itam = this.getInt2(otam);
                int itemp = this.getInt2(otemp);
                int itipo = this.getInt2(otipo);
                this.OSX_imprimir(itam, itemp, itipo);
                break;
                
            case "osx_illamadas":
                String tclase = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String tefuncion = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String tcadena =this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                
                Object otamanios = this.evaluarExpresion(nodoactual.hijos.get(3));
                int ttama = this.getInt2(otamanios);
                
                Object otparametros =this.evaluarExpresion(nodoactual.hijos.get(4));
                int tparametros = this.getInt2(otparametros);
                
                String tregistro = this.evaluarExpresion(nodoactual.hijos.get(5)).toString();
                
                if(tregistro.contains("[:THIS:]")==true)
                {
                    if(tparametros==0)
                    {
                        this.OSX_llamada_i_TS(tclase, tefuncion, ttama, tregistro);
                    }
                    else
                    {
                        this.OSX_llamada_i_VSMULTI(tclase, tefuncion, tcadena, ttama, tparametros, tregistro);
                    }
                }
                else
                {
                    this.OSX_llamada_i(tclase, tefuncion, tcadena, ttama, tparametros, tregistro);
                }                                
                break;
                
                
            case "++":
                this.evaluarExpresion(nodoactual);
                break;
                
            case "--":
                this.evaluarExpresion(nodoactual);
                break;
                
            case "if1":
                this.capturar_if1(nodoactual, funcion);
                break;
                
            case "if2":
                this.capturar_if2(nodoactual, funcion);
                break;
                
            case "if3":
                this.capturar_if3(nodoactual, funcion);
                break;
                
            case "if4":
                this.capturar_if4(nodoactual, funcion);
                break;
                
            case "switch":
                this.capturar_switch(nodoactual, funcion);
                break;
                 
            case "while":
                this.capturar_while(nodoactual, funcion);
                break;
                
            case "do_while":
                this.capturar_dowhile(nodoactual, funcion);
                break;
                
            case "repeat":
                this.capturar_repeat(nodoactual, funcion);
                break;
                
            case "for":
                this.capturar_for(nodoactual, funcion);
                break;
                
            case "loop":
                this.capturar_loop(nodoactual, funcion);
                break;
                
            default:
                System.out.println("! ============================================================ Default, Ejecutar 6");
        }
    }
    
    private void Ejecutar_7_accionesControl(Nodo nodoactual, Funcion funcion)
    {
        String name = nodoactual.name;
        //System.out.println("Ejecutar_7 "+name);
        switch (name) 
        {
            case "sentencias_c":
                for(Nodo nodohijo:nodoactual.hijos)
                {
                    if(funcion.retorno!=null){break;}
                    if("break".equalsIgnoreCase(nodohijo.name)==true)
                    {
                        this.break_encontrado();
                        break;
                    }                    
                    this.Ejecutar_7_accionesControl(nodohijo, funcion);                    
                    if(funcion.retorno!=null){break;}
                }
                break;
                
            case "declaracion":
                this.capturarDeclaracion(nodoactual);
                break;
                
            case "declaracion_vector":
                this.capturarDeclaracion_Vector(nodoactual);
                break;
                
            case "decla_asignacion":
                this.capturarDeclaAsigna(nodoactual);
                break;
                
            case "asignacion":
                this.capturarAsignacion(nodoactual);
                break;
                
            case "asignacion_vector":
                this.capturarAsignacion_Vector(nodoactual);
                break;
                
            case "asignacion_pasada":
                Object valor_pasada = this.evaluarExpresion(nodoactual.hijos.get(0));
                if(this.correcto(1, valor_pasada))
                {
                    this.osx_pasada=this.getInt2(valor_pasada);
                }
                else
                {
                    this.osx_pasada++;
                    String descri= "OSX_Pasada es del tipo int";
                    this.guardarError("OSX_Pasada", nodoactual.linea, nodoactual.columna, "Semantico",descri);
                }
                break;
                
            case "asignacion_temporal":
                Object valor_temporal = this.evaluarExpresion(nodoactual.hijos.get(0));
                if(this.correcto(1, valor_temporal))
                {
                    this.osx_pasada=this.getInt2(valor_temporal);
                }
                else
                {
                    this.osx_pasada++;
                    String descri= "OSX_Temporal es del tipo int";
                    this.guardarError("OSX_Temporal", nodoactual.linea, nodoactual.columna, "Semantico",descri);
                }
                break;
                
            case "asignacion_etiqueta":
                Object valor_etiqueta = this.evaluarExpresion(nodoactual.hijos.get(0));
                if(this.correcto(1, valor_etiqueta))
                {
                    this.osx_pasada=this.getInt2(valor_etiqueta);
                }
                else
                {
                    this.osx_pasada++;
                    String descri= "OSX_Etiqueta es del tipo int";
                    this.guardarError("OSX_Etiqueta", nodoactual.linea, nodoactual.columna, "Semantico",descri);
                }
                break;
                
            case "print":
                Object valor_print = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.print(valor_print);
                break;
            
            case "osx_print3d":
                Object valor_print3d = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.OSX_print3D(valor_print3d.toString());
                break;
                
            case "osx_saveerror":
                String oerror1 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String oerror2 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                this.guardarError(oerror1, this.superpadre.linea, this.superpadre.columna, "Semantico", oerror2);
                break;
                
            case "return":
                if(funcion.esMetodo==false)
                {
                    Object valor_reto = this.evaluarExpresion(nodoactual.hijos.get(0));
                    if(this.correcto(funcion.tipo, valor_reto)==true)
                    {
                        if(funcion.tipo==1){funcion.retorno=this.getInt(valor_reto);}
                        else if(funcion.tipo==2){funcion.retorno=this.getDouble(valor_reto);}
                        else if(funcion.tipo==3){funcion.retorno=valor_reto.toString();}
                        else if(funcion.tipo==4){funcion.retorno=this.getChar(valor_reto);}
                        else if(funcion.tipo==5){funcion.retorno =this.getBool(valor_reto);}
                    }
                    else
                    {
                        String descri="return de tipo incorrecto";
                        this.guardarError("return", nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
                        funcion.retorno=0;
                    }
                }
                else
                {
                    String descri=funcion.name+" no es del tipo funcion";
                    this.guardarError("return", nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
                    funcion.retorno=0;
                }                                
                break;
                
            case "llamada_1":
                //Buscar el metodo
                Funcion fu1 = this.get_mf_vacio(this.claseactual,nodoactual.hijos.get(0).name);
                if(fu1==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                this.Ejecutar_5_mf(fu1);
                break;
                
            case "llamada_2":
                Funcion fu2 = this.get_mf_multis(this.claseactual,nodoactual);
                if(fu2==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                this.Ejecutar_5_mf(fu2);
                break;
            
            case "osx_startclase":
                Object sclase1 = this.evaluarExpresion(nodoactual.hijos.get(0));
                this.ts.startClase(sclase1.toString(), nodoactual);
                break;
                
            case "osx_saveclase":
                this.ts.saveClase();
                break;
            
            case "osx_startmain":
                String sclase2 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                this.ts.startMain(sclase2, nodoactual);
                break;
                
            case "osx_savemain":
                this.ts.saveMain();
                break;
                
            case "osx_saveatributo":
                String sclas3 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String satri3 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String stipo3 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String svisi3 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                this.ts.saveAtributo(sclas3, satri3, stipo3, svisi3, nodoactual);
                break;
                
            case "osx_startconstructor":
                String sclase4 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String scon4 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                this.ts.startConstructor(sclase4, scon4);
                break;
                
            case "osx_saveconstructor":
                this.ts.saveConstructor();
                break;                            
            
            case "osx_startfuncion":
                String sclase5 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String sfuncio5 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String stipo5 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String svisi5 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                this.ts.startFuncion(sclase5, sfuncio5, stipo5, svisi5, nodoactual);
                break;
                
            case "osx_savefuncion":
                this.ts.saveFuncion();
                break;
                
            case "osx_saveparametro":
                String sclase6 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String sfuncio6 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String sparam6 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String stipo6 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                this.ts.saveParametro(sclase6, sfuncio6, sparam6, stipo6, nodoactual);
                break;
                
            case "osx_setcadenaparametros":
                String cadenap7 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                this.ts.setCadenaParametros(cadenap7);
                break;

            case "osx_savevariable":
                String sclase8 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String sfuncio8 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String svar8 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String stipo8 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                this.ts.saveVariable(sclase8, sfuncio8, svar8, stipo8, nodoactual);
                break;
                
            case "osx_printtabla":
                this.ts.soutTablaCSV();
                break;
                
            case "osx_imprimir":
                Object otam = this.evaluarExpresion(nodoactual.hijos.get(0));
                Object otemp = this.evaluarExpresion(nodoactual.hijos.get(1));
                Object otipo = this.evaluarExpresion(nodoactual.hijos.get(2));
                int itam = this.getInt2(otam);
                int itemp = this.getInt2(otemp);
                int itipo = this.getInt2(otipo);
                this.OSX_imprimir(itam, itemp, itipo);
                break;
                
            case "osx_illamadas":
                String tclase = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String tefuncion = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String tcadena =this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                
                Object otamanios = this.evaluarExpresion(nodoactual.hijos.get(3));
                int ttama = this.getInt2(otamanios);
                
                Object otparametros =this.evaluarExpresion(nodoactual.hijos.get(4));
                int tparametros = this.getInt2(otparametros);
                
                String tregistro = this.evaluarExpresion(nodoactual.hijos.get(5)).toString();
                
                if(tregistro.contains("[:THIS:]")==true)
                {
                    if(tparametros==0)
                    {
                        this.OSX_llamada_i_TS(tclase, tefuncion, ttama, tregistro);
                    }
                    else
                    {
                        this.OSX_llamada_i_VSMULTI(tclase, tefuncion, tcadena, ttama, tparametros, tregistro);
                    }
                }
                else
                {
                    this.OSX_llamada_i(tclase, tefuncion, tcadena, ttama, tparametros, tregistro);
                }                                
                break;
                
                
            case "++":
                this.evaluarExpresion(nodoactual);
                break;
                
            case "--":
                this.evaluarExpresion(nodoactual);
                break;
                
            case "if1":
                this.capturar_if1(nodoactual, funcion);
                break;
                
            case "if2":
                this.capturar_if2(nodoactual, funcion);
                break;
                
            case "if3":
                this.capturar_if3(nodoactual, funcion);
                break;
                
            case "if4":
                this.capturar_if4(nodoactual, funcion);
                break;
                
            case "switch":
                this.capturar_switch(nodoactual, funcion);
                break;
                 
            case "while":
                this.capturar_while(nodoactual, funcion);
                break;
                
            case "do_while":
                this.capturar_dowhile(nodoactual, funcion);
                break;
                
            case "repeat":
                this.capturar_repeat(nodoactual, funcion);
                break;
                
            case "for":
                this.capturar_for(nodoactual, funcion);
                break;
                
            case "loop":
                this.capturar_loop(nodoactual, funcion);
                break;
                
            default:
                System.out.println("! ============================================================ Default, Ejecutar 7");
        }
    }
                
    private void capturarDeclaracion(Nodo nodoactual){
        
        String stipo = nodoactual.hijos.get(0).name;
        Nodo listaids = nodoactual.hijos.get(1);        
        
        for(Nodo temp: listaids.hijos){
            Variable nueva_variable =new Variable(temp.name, stipo);
            this.guardarVariable(nueva_variable, nodoactual);            
        }        
    }
    
    private void capturarDeclaracion_Vector(Nodo nodoactual){
        Object tamanio = this.evaluarExpresion(nodoactual.hijos.get(2).hijos.get(0));
        String stipo = nodoactual.hijos.get(0).name;
        
        if(this.correcto(1, tamanio)==true){
            Variable nueva_variable = new Variable();
            nueva_variable.convertirVector(nodoactual.hijos.get(1).name, stipo, tamanio);
            this.guardarVariable(nueva_variable, nodoactual);
            Compilador.acciones += "Se Guardo el vector: " + nueva_variable.name + " con tamanio: " + nueva_variable.tamanio + "\n";
        }else{
            String descri="Tamanio del vector tiene que ser int";
            Nodo nodohijo = nodoactual.hijos.get(0);
            this.guardarError(nodohijo.name, nodohijo.linea, nodohijo.columna, "Ejecucion", descri);                   
        }
    }
    
    private void capturarAsignacion(Nodo nodoactual){
        String namevar = nodoactual.hijos.get(0).name.toLowerCase();
        Variable variable = this.getVariable(namevar);
        if(variable==null){
            String descri = "No existe dicha variable";
            this.guardarError(namevar, nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
            return;
        }        
        Object ovalor=this.evaluarExpresion(nodoactual.hijos.get(1));
        if(this.correcto(variable.tipo, ovalor)==false){
            String descri = "Tipos diferentes: "+variable.stipo+" vs "+this.stipoObjeto(ovalor);
            this.guardarError(namevar, nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
            return;
        }        
        if(variable.tipo==1){
            variable.valor=this.getInt(ovalor);
            Compilador.acciones += "Se asigno a la variable: " + variable.name + " el valor: " + variable.valor + " de tipo int\n";
        }else if(variable.tipo==2){
            variable.valor=this.getDouble(ovalor);
            Compilador.acciones += "Se asigno a la variable: " + variable.name + " el valor: " + variable.valor + " de tipo duoble\n";
        }else if(variable.tipo==3){
            variable.valor=ovalor.toString();
            Compilador.acciones += "Se asigno a la variable: " + variable.name + " el valor: " + variable.valor + " de tipo string\n";
        }else if(variable.tipo==4){
            variable.valor=this.getChar(ovalor);
            Compilador.acciones += "Se asigno a la variable: " + variable.name + " el valor: " + variable.valor + " de tipo char\n";
        }else if(variable.tipo==5){
            variable.valor =this.getBool(ovalor);
            Compilador.acciones += "Se asigno a la variable: " + variable.name + " el valor: " + variable.valor + " de tipo bool\n";
        }
    }    
    
    private void capturarAsignacion_Vector(Nodo nodoactual){
        String namevar = nodoactual.hijos.get(0).name.toLowerCase();
        Variable variable = this.getVariable(namevar);
        if(variable==null)
        {
            String descri = "No existe variable";
            this.guardarError(namevar, nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
            return;
        }        
        if(variable.esVector==false)
        {
            String descri = "Variable no es vector";
            this.guardarError(namevar, nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
            return;
        }
        
        Object index = this.evaluarExpresion(nodoactual.hijos.get(1).hijos.get(0));
        int iindex=0;        
        if(this.correcto(1, index)==true)
        {
            iindex = this.getInt(index);
        }
        else
        {
            String descri = "Index vector no es int";
            this.guardarError(index.toString(), nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
            return;
        }                        
        //validar tamanio
        if(iindex>=variable.tamanio)
        {
            String descri = "Index fuera de los limites";
            this.guardarError(index.toString(), nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
            return;
        }
        
        Object ovalor = this.evaluarExpresion(nodoactual.hijos.get(2));
        
        if(this.correcto(variable.tipo, ovalor))
        {
            if(variable.tipo==1){variable.setIndex(iindex, this.getInt(ovalor));}
            else if(variable.tipo==2){variable.setIndex(iindex, this.getDouble(ovalor));}
            else if(variable.tipo==3){variable.setIndex(iindex, ovalor.toString());}
            else if(variable.tipo==4){variable.setIndex(iindex,this.getChar(ovalor));}
            else if(variable.tipo==5){variable.setIndex(iindex, this.getBool(ovalor));}
        }
        else
        {
            String descri = "Tipos diferentes: "+variable.stipo+" vs "+this.stipoObjeto(ovalor);
            this.guardarError(namevar, nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
        }
    }
    
    private void capturarDeclaAsigna(Nodo nodoactual)
    {
        Nodo listaids = nodoactual.hijos.get(0);
        String stipo = nodoactual.hijos.get(1).name;        
        
        for(Nodo temp: listaids.hijos)
        {
            Variable nueva_variable =new Variable(temp.name, stipo);
            this.guardarVariable(nueva_variable, nodoactual);
            
            Object ovalor=this.evaluarExpresion(nodoactual.hijos.get(2));
            if(this.correcto(nueva_variable.tipo, ovalor)==false)
            {
                String descri = "Tipos diferentes: "+nueva_variable.stipo+" vs "+this.stipoObjeto(ovalor);
                this.guardarError(temp.name, nodoactual.linea, nodoactual.columna, "Ejecucion", descri);
                return;
            }            
            if(nueva_variable.tipo==1){nueva_variable.valor=this.getInt(ovalor);}
            else if(nueva_variable.tipo==2){nueva_variable.valor=this.getDouble(ovalor);}
            else if(nueva_variable.tipo==3){nueva_variable.valor=ovalor.toString();}
            else if(nueva_variable.tipo==4){nueva_variable.valor=this.getChar(ovalor);}
            else if(nueva_variable.tipo==5){nueva_variable.valor =this.getBool(ovalor);}   
        }                    
    }    
    
    private void capturar_if1(Nodo nodoactual, Funcion funcion)
    {
        Nodo nodo_if = nodoactual.hijos.get(0);
        Object ovalor = this.evaluarExpresion(nodo_if.hijos.get(0));
        //System.out.println("Paso por IF 1");
        if(this.correcto(5, ovalor)==true)
        {
            if(this.getBool(ovalor)==true)
            {
                this.ambito_Mas();
                this.Ejecutar_7_accionesControl(nodo_if.hijos.get(1), funcion);
                this.ambito_Menos();
            }
        }
        else
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo_if.linea, nodo_if.columna, "Ejecucion", descri);
        }        
    }
    
    private void capturar_if2(Nodo nodo, Funcion funcion)
    {
        
        //if, else
        Nodo nodo_if = nodo.hijos.get(0);
        Nodo nodo_else = nodo.hijos.get(1);
        Object ovalor = this.evaluarExpresion(nodo_if.hijos.get(0));
        //System.out.println("Paso por IF 2");
        if(this.correcto(5, ovalor)==false)
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo_if.linea, nodo_if.columna, "Ejecucion", descri);
            return;
        }
        
        if(this.getBool(ovalor)==true)
        {
            this.ambito_Mas();
            this.Ejecutar_7_accionesControl(nodo_if.hijos.get(1), funcion);
            this.ambito_Menos();
        }
        else
        {
            this.ambito_Mas();
            this.Ejecutar_7_accionesControl(nodo_else.hijos.get(0), funcion);
            this.ambito_Menos();
        }        
    }
    
    private void capturar_if3(Nodo nodo, Funcion funcion)
    {
        //if, elsees
        Nodo nodo_if = nodo.hijos.get(0);
        Nodo nodo_elses = nodo.hijos.get(1);
        Object ovalor = this.evaluarExpresion(nodo_if.hijos.get(0));
        //System.out.println("Paso por IF 3");        
        if(this.correcto(5, ovalor)==false)
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo_if.linea, nodo_if.columna, "Ejecucion", descri);
            return;
        }
        if(this.getBool(ovalor)==true)
        {
            this.ambito_Mas();
            this.Ejecutar_7_accionesControl(nodo_if.hijos.get(1), funcion);
            this.ambito_Menos();
            return;
        }
        //si paso esta le toca a los elses
        for(Nodo else_if:nodo_elses.hijos)
        {
            Object ovalor2 = this.evaluarExpresion(else_if.hijos.get(0));
            if(this.correcto(5, ovalor2))
            {
                if(this.getBool(ovalor2)==true)
                {
                    this.ambito_Mas();
                    this.Ejecutar_7_accionesControl(else_if.hijos.get(1), funcion);
                    this.ambito_Menos();
                    return;
                }
            }
            else
            {
                String descri="Condicion tiene que ser del tipo bool";
                this.guardarError(ovalor2.toString(), else_if.linea, else_if.columna, "Ejecucion", descri);
            }
        }
        
    }
    
    private void capturar_if4(Nodo nodo, Funcion funcion)
    {
        //if, elsees, else
        Nodo nodo_if = nodo.hijos.get(0);
        Nodo nodo_elses = nodo.hijos.get(1);
        Nodo nodo_else = nodo.hijos.get(2);
        Object ovalor = this.evaluarExpresion(nodo_if.hijos.get(0));
        //System.out.println("Paso por IF 4");
        if(this.correcto(5, ovalor)==false)
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo_if.linea, nodo_if.columna, "Ejecucion", descri);
            return;
        }
        if(this.getBool(ovalor)==true)
        {
            this.ambito_Mas();
            this.Ejecutar_7_accionesControl(nodo_if.hijos.get(1), funcion);
            this.ambito_Menos();
            return;
        }
        //si paso le toca a los elses
        for(Nodo else_if:nodo_elses.hijos)
        {
            Object ovalor2 = this.evaluarExpresion(else_if.hijos.get(0));
            if(this.correcto(5, ovalor2))
            {
                if(this.getBool(ovalor2)==true)
                {
                    this.ambito_Mas();
                    this.Ejecutar_7_accionesControl(else_if.hijos.get(1), funcion);
                    this.ambito_Menos();
                    return;
                }
            }
            else
            {
                String descri="Condicion tiene que ser del tipo bool";
                this.guardarError(ovalor2.toString(), else_if.linea, else_if.columna, "Ejecucion", descri);
            }
        }
        //si paso le toca al else
        this.ambito_Mas();
        this.Ejecutar_7_accionesControl(nodo_else.hijos.get(0), funcion);
        this.ambito_Menos();
    }    
    
    private void capturar_switch(Nodo nodo, Funcion fucion)
    {
        Object original = this.evaluarExpresion(nodo.hijos.get(0));
        if(original==null){return;}
        
        Nodo nodo_cases = nodo.hijos.get(1);
        Object valuando;
        
        this.ambito_Mas();
	this.break_aumento();
	int mibreak = this.break_salir;
        for(Nodo nodo_caso : nodo_cases.hijos)
        {
            if(nodo_caso.name.equalsIgnoreCase("caso"))
            {
                valuando = this.evaluarExpresion(nodo_caso.hijos.get(0));
                if(original.equals(valuando)==true)
                {
                    this.Ejecutar_7_accionesControl(nodo_caso.hijos.get(1), fucion);
                    if(mibreak>this.listaSalir.getFirst())
                    {
                        break;
                    }
                }
            }
            else if(nodo_caso.name.equalsIgnoreCase("default"))
            {
                //System.out.println("Pasa por default");
                this.Ejecutar_7_accionesControl(nodo_caso.hijos.get(0), fucion);
                if(mibreak>this.listaSalir.getFirst())
                {
                    break;
                }
            }
        }
        this.ambito_Menos();
        this.break_decremento();
    }
    
    private void capturar_while(Nodo nodo, Funcion funcion)
    {
        //WHILE::= fin rwhile:posi apar EXP:condi cpar rdo SCONTROL:sentencias fin rendwhile
        Object ovalor = this.evaluarExpresion(nodo.hijos.get(0));
        if(this.correcto(5, ovalor)==true)
        {
            this.ambito_Mas();
            this.break_aumento();
            int mibreak = this.break_salir;
            while(this.getBool(ovalor)==true)
            {
                this.Ejecutar_7_accionesControl(nodo.hijos.get(1), funcion);
                if(mibreak>this.listaSalir.getFirst())
		{
                    break;
		}
                ovalor = this.evaluarExpresion(nodo.hijos.get(0));                
                if(this.correcto(5, ovalor)==false)
                {
                    String descri="Condicion tiene que ser del tipo bool";
                    this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
                    break;
                }
            }
            this.ambito_Menos();
            this.break_decremento();
        }
        else
        {
            String descri="Condicion tiene que ser del tipo bool";
            this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
        }
    }
     
    private void capturar_dowhile(Nodo nodo, Funcion funcion)
    {
        //DO_WHILE::= fin rdo:posi SCONTROL:sentencias fin rend rwhile apar EXP:condi cpar                        
        Object ovalor = false;
        this.ambito_Mas();
        this.break_aumento();
        int mibreak = this.break_salir;
        do 
        {
            this.Ejecutar_7_accionesControl(nodo.hijos.get(0), funcion);            
            if(mibreak>this.listaSalir.getFirst())
            {
                break;
            }
            ovalor = this.evaluarExpresion(nodo.hijos.get(1));
            if(this.correcto(5, ovalor)==false)
            {
                String descri="Condicion tiene que ser del tipo bool";
                this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
                break;
            }
        } while (this.getBool(ovalor)==true);
        this.ambito_Menos();
        this.break_decremento();
    }
    
    private void capturar_repeat(Nodo nodo, Funcion funcion)
    {
        //REPEAT::= fin rrepeat:posi SCONTROL:sentencias fin renduntil apar EXP:condi cpar
        Object ovalor = true;
        this.ambito_Mas();
        this.break_aumento();
	int mibreak = this.break_salir;
        do 
        {
            this.Ejecutar_7_accionesControl(nodo.hijos.get(0), funcion);
            if(mibreak>this.listaSalir.getFirst())
            {
                break;
            }
            ovalor = this.evaluarExpresion(nodo.hijos.get(1));
            if(this.correcto(5, ovalor)==false)
            {
                String descri="Condicion tiene que ser del tipo bool";
                this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
                break;
            }
        } while (this.getBool(ovalor)==false);
        this.ambito_Menos();
        this.break_decremento();
    }
        
    private void capturar_for(Nodo nodo, Funcion funcion)
    {
        //FOR::= fin rfor:posi ASIGNACION_2:asigna rto EXP:condi rdo SCONTROL:sentencias fin rendfor
        //variable asignacion
            //debe estar ya declarada
            //tiene que ser entero        
        Nodo nodo_asigna = nodo.hijos.get(0);
        Nodo nodo_limite = nodo.hijos.get(1);
        Nodo nodo_acciones = nodo.hijos.get(2);
        //Buscando Variable
        Variable variable = this.getVariable(nodo_asigna.hijos.get(0).name);
        if(variable==null)
        {
            String descri = "Variable sin declarar";
            this.guardarError(nodo_asigna.hijos.get(0).name, nodo.linea, nodo.columna, "Ejecucion", descri);
            return;
        }
        if(variable.tipo!=1)
        {
            String descri = "Variable de control tiene que ser de tipo int";
            this.guardarError(variable.name, nodo.linea, nodo.columna, "Ejecucion", descri);
            return;
        }
        //Asignando Valor a Variable
        Object ovalor = this.evaluarExpresion(nodo_asigna.hijos.get(1));
        if(this.correcto(1, ovalor)==false)
        {
            String descri = "Variable " +variable.name+" de control tiene que tener valor int";
            this.guardarError(ovalor.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
            return;
        }
        variable.valor=this.getInt(ovalor);
        //Obteniendo valor de limite :D
        Object olimite = this.evaluarExpresion(nodo_limite);
        if(this.correcto(1, olimite)==false)
        {
            String descri = "Limte for tiene que ser int";
            this.guardarError(olimite.toString(), nodo.linea, nodo.columna, "Ejecucion", descri);
            return;
        }
        
        //paso entonces si se ejecuta el for :D        
        int int_limite = this.getInt(olimite);
        this.ambito_Mas();
        this.break_aumento();
	int mibreak = this.break_salir;
        while((Integer)variable.valor <= int_limite)
        {
            this.Ejecutar_7_accionesControl(nodo_acciones, funcion);            
            if(mibreak>this.listaSalir.getFirst())
            {
                break;
            }
            variable.valor= (Integer)variable.valor+1;
        } 
        this.ambito_Menos();
	this.break_decremento();
    }
    
    private void capturar_loop(Nodo nodo, Funcion funcion)
    {
        this.ambito_Mas();
        this.break_aumento();
	int mibreak = this.break_salir;
        while(true)
        {
            this.Ejecutar_7_accionesControl(nodo.hijos.get(0), funcion);
            if(mibreak>this.listaSalir.getFirst())
            {
                break;
            }
        }
        this.ambito_Menos();
	this.break_decremento();
    }
    
    
    //</editor-fold> 
    
    
    //<editor-fold desc="Evaluar Expresion"> 
    
    public Object evaluarExpresion(Nodo nodoactual)
    {
        String name = nodoactual.name;
        
        switch (name) 
        {
            case "??":  
                return this.ev_l_xor(nodoactual);
            case "||":  
                return this.ev_l_or(nodoactual);            
            case "&&":  
                return this.ev_l_and(nodoactual);
            case "!!":  
                return this.ev_l_not(nodoactual);
            case "==": 
                return this.ev_r_igual2(nodoactual);
            case "!=":  
                return this.ev_r_diferente(nodoactual);
            case ">":   
                return this.ev_r_mayor(nodoactual);
            case "<":   
                return this.ev_r_menor(nodoactual);
            case ">=":  
                return this.ev_r_mayorigual(nodoactual);
            case "<=":  
                return this.ev_r_menorigual(nodoactual);
            case "+":   
                return this.ev_a_suma(nodoactual);
            case "-":   
                return this.ev_a_resta(nodoactual);
            case "*":   
                return this.ev_a_multiplicar(nodoactual);
            case "/":   
                return this.ev_a_dividir(nodoactual);
            case "^":   
                return this.ev_a_potenciar(nodoactual);
            case "++":  
                return this.ev_a_aumento(nodoactual);
            case "--":  
                return this.ev_a_decremento(nodoactual);
            case "llamada_1":
                //Buscar el metodo
                Funcion fu1 = this.get_mf_vacio(this.claseactual,nodoactual.hijos.get(0).name);
                if(fu1==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                if(fu1.esMetodo==false)
                {
                    this.Ejecutar_5_mf(fu1);
                    if(fu1.retorno!=null)
                    {
                        return fu1.retorno;
                    }
                    return 1;
                }
                else
                {
                    String descri="No es funcion para tener retorno";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    return 1;
                }
            
            case "llamada_2":
                Funcion fu2 = this.get_mf_multis(this.claseactual,nodoactual);                
                if(fu2==null)
                {
                    String descri="No existe metodo, funcion";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    break;
                }
                if(fu2.esMetodo==false)
                {
                    this.Ejecutar_5_mf(fu2);
                    if(fu2.retorno!=null)
                    {
                        return fu2.retorno;
                    }
                    return 1;
                }
                else
                {
                    String descri="No es funcion para tener retorno";
                    Nodo nodoh = nodoactual.hijos.get(0);
                    this.guardarError(nodoh.name, nodoh.linea, nodoh.columna, "Ejecucion", descri);
                    return 1;
                }
            
            case "valor":
                try
                {
                    Object retorno_valor =  this.ev_valor(nodoactual);   
                    return retorno_valor;
                }
                catch(Exception ex)
                {
                    return 1;
                }
            
            case "get_id":
                return this.ev_id(nodoactual);
                
            case "get_vector":
                return this.ev_vector(nodoactual);
                    
            case "get_padre":       
                //valor de padre
                return this.superpadre.getValPadre();
                
            case "get_padre_a":     
                //valor de padre.atributo
                String atributo_padre = nodoactual.hijos.get(0).name;
                return this.superpadre.getValPadreAtributos(atributo_padre);
                
            case "get_hijo":        
                //valor de hijo
                //System.out.println("get_hijo");
                int index_hijo =  Integer.parseInt(nodoactual.hijos.get(0).name);
                return this.superpadre.getValHijo(index_hijo);
                
            case "get_hijo_a":        
                //System.out.println("get_hijo_a");
                //valor de hijo.atributo
                int index_hijo_a = Integer.parseInt(nodoactual.hijos.get(0).name);
                String atributo_hijo_a = nodoactual.hijos.get(1).name;
                return this.superpadre.getValHijoAtributos(index_hijo_a, atributo_hijo_a);
                
            case "osx_pasada":
                return this.osx_pasada;
                
            case "osx_temporal":
                return this.osx_temporal;                
                
            case "osx_etiqueta":
                return this.osx_etiqueta;
                
            case "osx_auxtipo":
                return this.osx_auxtipo;
                
            case "osx_auxlugar":
                return this.osx_auxlugar;
                
            case "osx_stringto3d":
                String stringto3d = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                return this.OSX_stringTO3D(stringto3d);
                
            case "osx_aritmetico":
                Object ot1 = this.evaluarExpresion(nodoactual.hijos.get(0));
                Object otipo1 = this.evaluarExpresion(nodoactual.hijos.get(1));
                Object ot2 = this.evaluarExpresion(nodoactual.hijos.get(2));
                Object otipo2 = this.evaluarExpresion(nodoactual.hijos.get(3));
                
                int t1 = this.getInt2(ot1); int tipo1 = this.getInt2(otipo1);
                int t2 = this.getInt2(ot2);
                int tipo2 = this.getInt2(otipo2);
                
                return this.OSX_Aritmetico1(t1, tipo1, t2, tipo2);
                
            case "osx_getregistro":
                String sclase = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String sfuncion = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String scadena_p2 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String sregistro = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                    
                if(sregistro.contains("[:THIS:]")==true)
                {
                    return this.OSX_getRegistroTHIS(sclase, sregistro);
                }
                else
                {
                    return this.OSX_getRegistro(sclase, sfuncion, scadena_p2, sregistro);
                }
            
            case "osx_getposregistro":
                String sclase2 = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String sfuncion2 = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String scadena_p22 = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                String sregistro2 = this.evaluarExpresion(nodoactual.hijos.get(3)).toString();
                if(sregistro2.contains("[:THIS:]")==true)
                {
                    return this.OSX_getPosicionRegistroTHIS(sclase2, sregistro2);
                }
                else
                {
                    
                    return this.OSX_getPosicionRegistro(sclase2, sfuncion2, scadena_p22, sregistro2);
                }
            
            case "osx_gettamanio":
                String lc = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String lf = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                String lcad = this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                return this.ts.getTamanioFuncion(lc, lf, lcad, superpadre);
                
            case "osx_dllamadas":
                String iclase = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                String ifuncion = this.evaluarExpresion(nodoactual.hijos.get(1)).toString();
                Object oitam = this.evaluarExpresion(nodoactual.hijos.get(3));
                int itamanio = this.getInt2(oitam);
                String cadenaactual =this.evaluarExpresion(nodoactual.hijos.get(2)).toString();
                Object opara =this.evaluarExpresion(nodoactual.hijos.get(4));
                int ipara = this.getInt2(opara);
                String iregistro = this.evaluarExpresion(nodoactual.hijos.get(5)).toString();
                
                if(iregistro.contains("[:THIS:]")==true)
                {
                    if(ipara==0)
                    {
                        return this.OSX_llamada_d_TS(iclase, ifuncion, itamanio, iregistro);
                    }
                    else
                    {
                        //THIS CON PARAMETROS
                        return this.OSX_llamada_d_TSMULTI(iclase, ifuncion, itamanio, ipara, iregistro);
                    }
                }
                else
                {
                    return this.OSX_llamada_d(iclase, ifuncion, itamanio, cadenaactual, ipara, iregistro);
                }                                
                //break;
            case "osx_getcadenapc":
                String nombrecon = this.evaluarExpresion(nodoactual.hijos.get(0)).toString();
                Object onp =  this.evaluarExpresion(nodoactual.hijos.get(1));
                int np = this.getInt2(onp);
                return ts.getCadenaPC(nombrecon, np, superpadre);
                
            default:
                System.out.println("! ============================================================ Default, evaluarExpresion: "+name);
                break;
        }
        return 1;
    }
        
    private Object ev_l_xor(Nodo nodo)
    {
        Object izq = this.evaluarExpresion(nodo.hijos.get(0));
        Object der = this.evaluarExpresion(nodo.hijos.get(1));
        
        if(this.correcto(5, izq) && this.correcto(5, der))
        {
            Boolean a = this.getBool(izq);
            Boolean b = this.getBool(der);
            return (a==true && b ==false) || (b==true && a ==false);
        }        
        String descri="Critico xor, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no son operandos logicos";
        this.guardarError("??", nodo.linea, nodo.columna, "Semantico", descri);
        return false;
    }
    
    private Object ev_l_or(Nodo nodo)
    {
        Object izq = this.evaluarExpresion(nodo.hijos.get(0));
        Object der = this.evaluarExpresion(nodo.hijos.get(1));                
        if(this.correcto(5, izq) && this.correcto(5, der))
        {
            Boolean a = this.getBool(izq);
            Boolean b = this.getBool(der);
            return (a || b);
        }
        String descri="Critico or, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no son operandos logicos";
        this.guardarError("||", nodo.linea, nodo.columna, "Semantico", descri);
        return false;
    }
    
    private Object ev_l_and(Nodo nodo)
    {
        Object izq = this.evaluarExpresion(nodo.hijos.get(0));
        Object der = this.evaluarExpresion(nodo.hijos.get(1));
        if(this.correcto(5, izq) && this.correcto(5, der))
        {
            Boolean a = getBool(nodo, izq);
            Boolean b = getBool(nodo, der);
            return (a && b);
        }                               
        String descri="Critico and, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no son operandos logicos";
        this.guardarError("&&",nodo.linea,nodo.columna,"Semantico",descri);
        return false;
    }
    
    private Object ev_l_not(Nodo nodoactual)
    { 
        Object uni = this.evaluarExpresion(nodoactual.hijos.get(0));
        if(this.correcto(5, uni))
        {
            Boolean a = this.getBool(nodoactual, uni);
            return !a;
        }        
        String descri="Critico not, "+this.stipoObjeto(uni)+"no es operando logico";
        this.guardarError("!!",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;
    }
    
    private Object ev_r_igual2(Nodo nodoactual)
    {
        //System.out.println("*** ev_igual2");
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
                
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, "==", der);
        }
        if(ti==3 && td==3)
        {
            return (izq.toString().equals(der.toString()));
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) == (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) == this.toAscii(der));
        }   
        else if(ti==5 && td==5)
        {
            return (Objects.equals(Boolean.valueOf(izq.toString()), Boolean.valueOf(der.toString())));
        }
        String descri="Critico igual2, "+this.stipoObjeto(izq)+" vs "+this.stipoObjeto(der)+" no operandos relacionales";        
        this.guardarError("==",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
    
    private Object ev_r_diferente(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, "!=", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) != (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) != this.toAscii(der));
        }     
        else if(ti==5 && td==5)
        {
            return (!Objects.equals(Boolean.valueOf(izq.toString()), Boolean.valueOf(der.toString())));
        }
        String descri="Critico diferente, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError("!=",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
    
    private Object ev_r_mayor(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, ">", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) > (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) > this.toAscii(der));
        }
        
        String descri="Critico mayorque, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError(">",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
        
    private Object ev_r_menor(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, "<", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) < (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) < this.toAscii(der));
        }        
        String descri="Critico menorque, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError("<",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
              
    private Object ev_r_mayorigual(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, ">=", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) >= (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) >= this.toAscii(der));
        }
        
        String descri="Critico mayorIgual, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError(">=",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }
    
    private Object ev_r_menorigual(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int ti = this.tipoObjeto(izq);  int td = this.tipoObjeto(der);
        
        if((ti==1 || ti==2) && (td==1 || td==2))
        {
            return this.numerosRelacionales(izq, "<=", der);
        }
        else if((ti==3 || td==4) && (ti==3 && td==4))
        {
            return (this.sumasAscii(izq) <= (this.sumasAscii(der)));
        }
        else if((ti==1 && td==4)||(ti==5 && td==4))
        {
            return (this.toAscii(izq) <= this.toAscii(der));
        }
        
        String descri="Critico menorIgual, "+this.stipoObjeto(izq)+", "+this.stipoObjeto(der)+" no operandos relacionales";
        this.guardarError("<=",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return false;        
    }        
    
    private Object ev_a_suma(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return (Integer)izq + (Integer)der;
                case 2:
                    return Double.valueOf(izq.toString()) + (Double)der;
                case 3:
                    return izq.toString() + der.toString();
                case 4:
                    return (Integer)izq + this.toAscii(der);  
                case 5:
                    return (Integer)izq + this.toLogico(der); 
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);                    
                    this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return Double.valueOf(izq.toString())+ Double.valueOf(der.toString());     
                case 2: 
                    return (Double)izq + (Double)der;
                case 3:
                    return izq.toString() + der.toString();
                case 4:
                    Object double_char = this.toAscii(der);
                    return (Double)izq + Double.valueOf(double_char.toString()); 
                case 5:
                    Object double_bool = this.toLogico(der);
                    return (Double)izq + Double.valueOf(double_bool.toString());
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);
                    this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if((tipo_iz==3) || (tipo_de==3))
        {
            return izq.toString()+der.toString();
        }
//        if((tipo_iz==3 && tipo_de!=5) || (tipo_iz!=5 && tipo_de==3))
//        {
//            return izq.toString()+der.toString();
//        }
        if(tipo_iz==4)
        {
            switch (tipo_de)
            {
                case 1: 
                    return toAscii(izq) + (Integer)der;
                case 2:
                    Object izq_char = toAscii(izq);
                    return Double.valueOf(izq_char.toString()) + (Double)der;
                case 3:
                    return izq.toString() + der.toString();
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);
                    this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return this.toLogico(izq) + (Integer)der;     
                case 2: 
                    Object r5=this.toLogico(izq);
                    return Double.valueOf(r5.toString()) + (Double)der;                    
                case 5: 
                    return (Boolean)izq || (Boolean)der;
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);
                    this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" + "+stipoObjeto(der);
        this.guardarError("+",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
        
    private Object ev_a_resta(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return (Integer)izq - (Integer)der;
                case 2:
                    return Double.valueOf(izq.toString()) - (Double)der;
                case 4:
                    return (Integer)izq - this.toAscii(der);  
                case 5:
                    return (Integer)izq - this.toLogico(der); 
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);                    
                    this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return Double.valueOf(izq.toString())-Double.valueOf(der.toString());     
                case 2: 
                    return (Double)izq - (Double)der;
                case 4:
                    Object double_char = this.toAscii(der);
                    return (Double)izq - Double.valueOf(double_char.toString()); 
                case 5:
                    Object double_bool = this.toLogico(der);
                    return (Double)izq - Double.valueOf(double_bool.toString());
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);
                    this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if(tipo_iz==4)
        {
            switch (tipo_de)
            {
                case 1: 
                    return toAscii(izq) - (Integer)der;
                case 2:
                    Object izq_char = toAscii(izq);
                    return Double.valueOf(izq_char.toString()) - (Double)der;
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);
                    this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return this.toLogico(izq) - (Integer)der;     
                case 2: 
                    Object r5=this.toLogico(izq);
                    return Double.valueOf(r5.toString()) - (Double)der;
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);
                    this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" - "+stipoObjeto(der);
        this.guardarError("-",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
    
    private Object ev_a_multiplicar(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return (Integer)izq * (Integer)der;
                case 2:
                    return Double.valueOf(izq.toString()) * (Double)der;
                case 4:
                    return (Integer)izq * this.toAscii(der);  
                case 5:
                    return (Integer)izq * this.toLogico(der); 
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);                    
                    this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return Double.valueOf(izq.toString()) * Double.valueOf(der.toString());     
                case 2: 
                    return (Double)izq * (Double)der;
                case 4:
                    Object double_char = this.toAscii(der);
                    return (Double)izq * Double.valueOf(double_char.toString()); 
                case 5:
                    Object double_bool = this.toLogico(der);
                    return (Double)izq * Double.valueOf(double_bool.toString());
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);
                    this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if(tipo_iz==4)
        {
            switch (tipo_de)
            {
                case 1: 
                    return toAscii(izq) * (Integer)der;
                case 2:
                    Object izq_char = toAscii(izq);
                    return Double.valueOf(izq_char.toString()) * (Double)der;
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);
                    this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return this.toLogico(izq) * (Integer)der;     
                case 2: 
                    Object r5=this.toLogico(izq);
                    return Double.valueOf(r5.toString()) * (Double)der;                    
                case 5: 
                    return (Boolean)izq && (Boolean)der;
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);
                    this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" * "+stipoObjeto(der);
        this.guardarError("*",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
    
    private Object ev_a_dividir(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));        
        
        int valor = this.getInt2(der);
        if(valor == 0)
        {
            String descri="division entre 0";                    
            this.guardarError("/",nodoactual.linea,nodoactual.columna,"Ejecucion",descri); 
            return 1;
        }
        
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return (Double.valueOf(izq.toString()) / (Double.valueOf(der.toString())));
                case 2:
                    return (Double.valueOf(izq.toString()) / (Double)der);
                case 4:
                    Object r14 = this.toAscii(der);
                    return (Double.valueOf(izq.toString()) / (Double.valueOf(r14.toString())));
                case 5:
                    Object r15 = this.toLogico(der);
                    return (Double.valueOf(izq.toString()) / (Double.valueOf(r15.toString())));
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);                    
                    this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1: 
                    return (Double.valueOf(izq.toString()) / (Double.valueOf(der.toString())));
                case 2: 
                    return (Double)izq / (Double)der;
                case 4:
                    Object double_char = this.toAscii(der);
                    return (Double)izq / Double.valueOf(double_char.toString()); 
                case 5:
                    Object double_bool = this.toLogico(der);
                    return (Double)izq / Double.valueOf(double_bool.toString());
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);
                    this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if(tipo_iz==4)
        {
            Object r4= this.toAscii(izq);
            switch (tipo_de)
            {
                case 1: 
                    return (Double.valueOf(r4.toString()) / (Double.valueOf(der.toString())));
                case 2:                    
                    return (Double.valueOf(r4.toString()) / (Double)der);
                case 5:
                    Object r45 = this.toLogico(der);
                    return (Double.valueOf(r4.toString()) /  Double.valueOf(r45.toString()));
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);
                    this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {        
            Object r5 = this.toLogico(izq);
            switch (tipo_de) 
            {
                case 1:
                    return Double.valueOf(r5.toString()) / Double.valueOf(der.toString());
                case 2: 
                    return Double.valueOf(r5.toString()) / (Double)der;
                case 4: 
                    Object r4 = this.toAscii(der);
                    return Double.valueOf(r5.toString()) / Double.valueOf(r4.toString());
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);
                    this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" / "+stipoObjeto(der);
        this.guardarError("/",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
    
    private Object ev_a_potenciar(Nodo nodoactual)
    {
        Object izq = this.evaluarExpresion(nodoactual.hijos.get(0));
        Object der = this.evaluarExpresion(nodoactual.hijos.get(1));
        int tipo_iz = this.tipoObjeto(izq);
        int tipo_de = this.tipoObjeto(der);
        
        if(tipo_iz==1)
        {                           
            switch (tipo_de) 
            {
                case 1: 
                    return Math.pow(Double.valueOf(izq.toString()), (Double.valueOf(der.toString())));
                case 2:
                    return Math.pow(Double.valueOf(izq.toString()) , (Double)der);
                case 4:
                    Object double_char = this.toAscii(der);
                    return Math.pow((Double)izq, Double.valueOf(double_char.toString()));
                case 5:
                    Object double_bool = this.toLogico(der);
                    return Math.pow((Double)izq,Double.valueOf(double_bool.toString()));
                default:                  
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);                    
                    this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==2)
        {               
            switch (tipo_de) 
            {
                case 1:                     
                    return Math.pow(Double.valueOf(izq.toString()), (Double.valueOf(der.toString())));
                case 2: 
                    return Math.pow((Double)izq , (Double)der);
                case 4:
                    Object double_char = this.toAscii(der);
                    return Math.pow((Double)izq, Double.valueOf(double_char.toString()));
                case 5:
                    Object double_bool = this.toLogico(der);
                    return Math.pow((Double)izq,Double.valueOf(double_bool.toString()));
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);
                    this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri);
                    return 1;
            }
        }
        if(tipo_iz==4)
        {
            Object r4= this.toAscii(izq);
            switch (tipo_de)
            {
                case 1: 
                    return Math.pow(Double.valueOf(r4.toString()) , (Double.valueOf(der.toString())));
                case 2:                    
                    return (Double.valueOf(r4.toString()) / (Double)der);
                default:
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);
                    this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        if(tipo_iz==5)
        {        
            Object r5 = this.toLogico(izq);
            switch (tipo_de) 
            {
                case 1:
                    return Math.pow(Double.valueOf(r5.toString()) , Double.valueOf(der.toString()));
                case 2: 
                    return Math.pow(Double.valueOf(r5.toString()) ,(Double)der);
                default:    
                    String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);
                    this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri); 
                    return 1;
            }
        }
        String descri="Combinacion invalida: "+stipoObjeto(izq)+" ^ "+stipoObjeto(der);
        this.guardarError("^",nodoactual.linea,nodoactual.columna,"Semantico",descri);
        return 1;
    }
    
    private Object ev_a_aumento(Nodo nodoactual)
    {
        Nodo hijo0 = nodoactual.hijos.get(0);
        
        if(hijo0.name=="osx_pasada")
        {
            this.osx_pasada++;
            return this.osx_pasada;
        }
        if(hijo0.name=="osx_temporal")
        {
            this.osx_temporal++;
            return this.osx_temporal;
        }
        if(hijo0.name=="osx_etiqueta")
        {
            this.osx_etiqueta++;
            return this.osx_etiqueta;
        }        
        if(hijo0.name.equalsIgnoreCase("get_id"))
        {
            Variable vvv = this.getVariable(hijo0.hijos.get(0).name);
            if(vvv!=null)
            {
                Object uni = vvv.valor;
                if(vvv.tipo==1)
                {
                    vvv.valor = (Integer)uni +1;
                    return vvv.valor;
                }
                if(vvv.tipo==2)
                {
                    if(uni.toString().contains("."))
                    {
                        String sdouble = uni.toString();
                        String[] adouble = sdouble.split("\\.");
                        int posiciones = adouble[1].length();
                        Double precision = Math.pow(10, posiciones);
                        Double decimales = 1 / precision;
                        vvv.valor=(Double)uni+decimales;
                        return vvv.valor;
                    }
                    else
                    {
                        vvv.valor= (Double)uni+0.1;
                        return vvv.valor;
                    }
                }
                if(vvv.tipo==4)
                {
                    vvv.valor= toAscii(uni) + 1;
                    return vvv.valor;
                }
                else
                {
                    String err = "++, Combinacion invalida"+uni.toString()+"'";
                    this.guardarError("++",nodoactual.linea,nodoactual.columna,"semantico",err); 
                    return 1;   
                }
            }
            else
            {
                String err = "Variable no existe";
                this.guardarError(hijo0.hijos.get(0).name,nodoactual.linea,nodoactual.columna,"semantico",err); 
                return 1;   
            }
        }
        //si paso es otra onda pero no modifica variables
        Object uni = this.evaluarExpresion(nodoactual.hijos.get(0));
        int tipo_uni = this.tipoObjeto(uni);
        
        if(tipo_uni==1)
        {
            return (Integer)uni +1;
        }
        else if(tipo_uni==2)
        {
            if(uni.toString().contains("."))
            {
                String sdouble = uni.toString();
                String[] adouble = sdouble.split("\\.");
                int posiciones = adouble[1].length();
                Double precision = Math.pow(10, posiciones);
                Double decimales = 1 / precision;
                return (Double)uni+decimales;
            }
            else
            {
                return (Double)uni+0.1;
            }
        }
        else if(tipo_uni==4)
        {
            return toAscii(uni) + 1;
        }
        else
        {
            String err = "++, Combinacion invalida "+uni.toString();
            this.guardarError("++",nodoactual.linea,nodoactual.columna,"semantico",err); 
            return 1;   
        }
    }
    
    private Object ev_a_decremento(Nodo nodoactual)
    {
        Nodo hijo0 = nodoactual.hijos.get(0);
        if(hijo0.name=="osx_pasada")
        {
            this.osx_pasada--;
            return this.osx_pasada;
        }
        if(hijo0.name=="osx_temporal")
        {
            this.osx_temporal--;
            return this.osx_temporal;
        }
        if(hijo0.name=="osx_etiqueta")
        {
            this.osx_etiqueta--;
            return this.osx_etiqueta;
        }
        if(hijo0.name.equalsIgnoreCase("get_id"))
        {
            Variable vvv = this.getVariable(hijo0.hijos.get(0).name);
            if(vvv!=null)
            {
                Object uni = vvv.valor;
                if(vvv.tipo==1)
                {
                    vvv.valor = (Integer)uni-1;
                    return vvv.valor;
                }
                if(vvv.tipo==2)
                {
                    if(uni.toString().contains("."))
                    {
                        String sdouble = uni.toString();
                        String[] adouble = sdouble.split("\\.");
                        int posiciones = adouble[1].length();
                        Double precision = Math.pow(10, posiciones);
                        Double decimales = 1 / precision;
                        vvv.valor=(Double)uni-decimales;
                        return vvv.valor;
                    }
                    else
                    {
                        vvv.valor= (Double)uni-0.1;
                        return vvv.valor;
                    }
                }
                if(vvv.tipo==4)
                {
                    vvv.valor= toAscii(uni) - 1;
                    return vvv.valor;
                }
                else
                {
                    String err = "--, Combinacion invalida "+uni.toString();
                    this.guardarError("--",nodoactual.linea,nodoactual.columna,"semantico",err); 
                    return 1;   
                }
            }
            else
            {
                String err = "Variable no existe";
                this.guardarError(hijo0.hijos.get(0).name,nodoactual.linea,nodoactual.columna,"semantico",err); 
                return 1;   
            }
        }
        //si paso es otra onda pero no modifica variables
        Object uni = this.evaluarExpresion(nodoactual.hijos.get(0));
        int tipo_uni = this.tipoObjeto(uni);
        
        if(tipo_uni==1)
        {
            return (Integer)uni-1;
        }
        else if(tipo_uni==2)
        {
            if(uni.toString().contains("."))
            {
                String sdouble = uni.toString();
                String[] adouble = sdouble.split("\\.");
                int posiciones = adouble[1].length();
                Double precision = Math.pow(10, posiciones);
                Double decimales = 1 / precision;
                return (Double)uni-decimales;
            }
            else
            {
                return (Double)uni-0.1;
            }
        }
        else if(tipo_uni==4)
        {
            return toAscii(uni) - 1;
        }
        else
        {
            String err = "--, Combinacion invalida "+uni.toString();
            this.guardarError("--",nodoactual.linea,nodoactual.columna,"semantico",err); 
            return 1;   
        }
    }
    
    public Object ev_valor(Nodo nodoactual)
    {
        Nodo tipo = nodoactual.hijos.get(0);        
        Nodo valor =nodoactual.hijos.get(1);
        
        //System.out.println(tipo.name);
        //System.out.println("ev_valor, ants del switch: "+valor.name);        
        switch (tipo.name.toLowerCase()) 
        {
            case "int":
                String valor_int = valor.name.replace(" ", "");
                if(valor_int.contains("-")==true)
                {
                    String ttt= valor_int.replace("-", "");
                    Object r1= Integer.valueOf(ttt)*-1;
                    //System.out.println("****** salio del int negativo");
                    return r1;
                }
                else
                {
                    Object r1= Integer.valueOf(valor_int);
                    //System.out.println("****** salio del int");
                    return r1;
                }
            case "double":
                String valor_double = valor.name.replace(" ", "");
                if(valor_double .contains("-")==true)
                {
                    String ttt= valor_double .replace("-", "");
                    Object r1= Double.valueOf(ttt)*-1;
                    //System.out.println("****** salio del double negativo");
                    return r1;
                }
                else
                {
                    Object r1= Double.valueOf(valor_double );
                    //System.out.println("****** salio del double");
                    return r1;
                }
            case "string":
                return valor.name.replace("\\t", "\t").replace("\\n", "\n").replace("''", "\"");
            case "char":                
                char cad[]=valor.name.toCharArray();
                //System.out.println("****** salio del char");
                return cad[0];            
            case "bool":
                Object result5 = Boolean.valueOf(valor.name);
                //System.out.println("****** salio del bool");
                return result5;
                //break;
            default:
                //System.out.println("****** default ev_valor");
                return 1;
        }
    }
    
    private Object ev_id(Nodo nodoactual)
    {
        String pnombre = nodoactual.hijos.get(0).name;
        Variable vv = this.getVariable(pnombre);
        if(vv==null)
        {
            String descri = "No existe dicha variable";
            this.guardarError(pnombre,nodoactual.linea,nodoactual.columna, "Ejecucion",descri);
            return 1;
        }
        if(vv.valor==null)
        {
            String descri = "No hay valor asignado";
            this.guardarError(pnombre,nodoactual.linea,nodoactual.columna, "Ejecucion",descri);
            return 1;
        }
        return vv.valor;
    }
        
    private Object ev_vector(Nodo nodoactual)
    {
        String pnombre = nodoactual.hijos.get(0).name;
        Variable vv = this.getVariable(pnombre);
        if(vv==null)
        {
            String descri = "No existe dicha variable";
            this.guardarError(pnombre,nodoactual.linea,nodoactual.columna, "Ejecucion",descri);
            return 1;
        }
        if(vv.esVector==false)
        {
            String descri = "No es vector";
            this.guardarError(pnombre,nodoactual.linea,nodoactual.columna, "Ejecucion",descri);
            return 1;
        }
        Object index = this.evaluarExpresion(nodoactual.hijos.get(1).hijos.get(0));
        if(this.correcto(1, index))
        {
            int int_index = this.getInt(index);
            //valira que el index sea menor que tamanio del vector
            if(int_index<vv.tamanio)
            {
                return vv.vector.get(int_index);
            }
        }
        String descri = "Fuera de los limites: tamanio: "+vv.tamanio+" vs index: "+index.toString();
        this.guardarError(pnombre,nodoactual.linea,nodoactual.columna, "Ejecucion",descri);
        return 1;
    }
    
    //</editor-fold> 
    
    
    //<editor-fold desc="Otros">
    
    private boolean correcto(int tipo_variable, Object ob)
    {
        int tipo_objeto=this.tipoObjeto(ob);
        
        switch (tipo_variable) 
        {
            case 1:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int default
                    case 4: return true;    //char pero en modo ascii
                    case 5: return true;    //booleano -> logico
                    default:    break;
                }break;
            case 2:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int
                    case 2: return true;    //double default
                    default:    break;
                }break;
            case 3:
                if(tipo_objeto==3){return true;}    //string default
                break;
            case 4:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int, de ascii a char
                    case 4: return true;    //char default
                    default:    break;
                }break;
            case 5:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int
                    case 5: return true;    //bool default
                    default:    break;
                }break;
            default:    break;
        }
        return false;
    } 
    
    private int tipoObjeto(Object ob)
    {
        if(ob instanceof Integer){return 1;}
        else if(ob instanceof Double){return 2;}
        else if(ob instanceof String){return 3;}
        else if(ob instanceof Character){return 4;}
        else if(ob instanceof Boolean){return 5;}
        return 0;
    }    
        
    private String stipoObjeto(Object ob)
    {
        if(ob instanceof Integer){return "int";}
        else if(ob instanceof Double){return "double";}
        else if(ob instanceof String){return "string";}
        else if(ob instanceof Character){return "char";}
        else if(ob instanceof Boolean){return "bool";}
        return "";
    }
    
    private char toCaracter(int c)
    {
        return (char)c;
    }

    private int toAscii(char c)
    {
        int ascii = (int)c;
        return ascii; 
    }
    
    private int getInt(Object ob)
    {
       switch (tipoObjeto(ob))
       {
           case 1:
               return (int)ob;
           case 4:
               return toAscii((char)ob);
           case 5:
               if ((boolean)ob)
               {
                   return 1;
               }
               else
               {
                   return 0;
               }                    
           default:
               System.out.println("Error get Int");
               return 1;
       }
    }
    
    private int getInt2(Object ob)
    {
       switch (tipoObjeto(ob))
       {
           case 1:
               return (int)ob;
           case 2:;
               Object result = Double.valueOf(ob.toString());
               int sali = ((Number)result).intValue();
               return sali;
           case 4:
               return toAscii((char)ob);
           case 5:
               if ((boolean)ob)
               {
                   return 1;
               }
               else
               {
                   return 0;
               }         
           default:
               System.out.println("Error get Int2");
               return 1;
       }
    }
    
    private double getDouble(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 1:
                return Double.valueOf(ob.toString());
            case 2:
                return (Double)ob;
            default:
                System.out.println("Error get Double");
                return 1.0;
        }
    }
    
    private char getChar(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 1:
                return toCaracter((int)ob);
            case 4:
                return (char)ob;
            case 5:
                if ((boolean)ob)
                {
                    return toCaracter(1);
                }
                else
                {
                    return toCaracter(0);
                }
            default:
                System.out.println("Error get Char");
                return 'c';
        }
    }
    
    private Boolean getBool(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 5:
                return (boolean)ob;
            case 1:
                if ((int)ob == 1){return true;}
                else if ((int)ob == 0){return false;}
                String descri = "1, 0 pueden ser logico y bool";
                this.guardarError(ob.toString(), 0, 0, "Ejecucion", descri);
                return false;
            default: return false;
        }
    }
    
    private Boolean getBool(Nodo nodoactual,Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 5:
                return Boolean.valueOf(ob.toString());
            case 1:
                if ((int)ob == 1){return true;}
                else if ((int)ob == 0){return false;}
                String descri1 = "1, 0 pueden ser logico y bool";
                this.guardarError(ob.toString(), 0, 0, "Ejecucion", descri1);
            default: 
                String descri2=this.stipoObjeto(ob)+" no es bool";
                this.guardarError(ob.toString(), nodoactual.linea, nodoactual.columna, "Ejecucion", descri2);
                return false;
        }
    } 
    
    private int sumasAscii(Object o)
    {
        String cadena1=o.toString();
        //retorna la suma de todos los caracteres en ascii del char o string
        cadena1 = cadena1.replace("'", "");
        char[] chars1 = cadena1.toCharArray();
        int retorno=0;
        for (int i = 0; i < chars1.length; i++)
        {
            retorno+= toAscii(chars1[i]);
        }        
        return retorno;
    }
    
    private int toLogico(Object ob)
    {
        if(this.tipoObjeto(ob)==5)
        {
            if((Boolean)ob== true)
            {
                return 1;
            }
            else if((Boolean)ob==false)
            {
                return 0;
            }
        }        
        return 0;
    }  
    
    private int toAscii(String s)
    {
        char c = s.charAt(0); // c='a'
        int ascii = (int)c;
        return ascii; 
    }
         
    private int toAscii(Object ob)
    {
        char c=(Character)ob;
        int ascii = (int)c;
        return ascii; 
    }
    
    private boolean numerosRelacionales(Object a, String operador, Object b)
    {
        int ti = this.tipoObjeto(a);    
        int td = this.tipoObjeto(b);
        
        switch (ti) 
        {
            case 1:
                if(td==1)
                {
                    switch (operador) 
                    {
                        case "==":
                            return (Objects.equals(Integer.valueOf(a.toString()), Integer.valueOf(b.toString())));
                        case "!=":
                            return !(Objects.equals(Integer.valueOf(a.toString()), Integer.valueOf(b.toString())));
                        case ">":
                            return Integer.valueOf(a.toString()) > Integer.valueOf(b.toString());
                        case "<":
                            return Integer.valueOf(a.toString()) < Integer.valueOf(b.toString());
                        case ">=":
                            return Integer.valueOf(a.toString()) >= Integer.valueOf(b.toString());
                        case "<=":
                            return Integer.valueOf(a.toString()) <= Integer.valueOf(b.toString());
                        default: break;
                    }
                }
                else
                {
                    switch (operador) {
                        case "==":
                            return (Objects.equals(Double.valueOf(a.toString()), Double.valueOf(b.toString())));
                        case "!=":
                            return !(Objects.equals(Double.valueOf(a.toString()), Double.valueOf(b.toString())));
                        case ">":
                            return Double.valueOf(a.toString()) > Double.valueOf(b.toString());
                        case "<":
                            return Double.valueOf(a.toString()) < Double.valueOf(b.toString());
                        case ">=":
                            return Double.valueOf(a.toString()) >= Double.valueOf(b.toString());
                        case "<=":
                            return Double.valueOf(a.toString()) <= Double.valueOf(b.toString());
                        default: break;
                    }
                }
            
            case 2:
                switch (operador) {
                        case "==":
                            return (Objects.equals(Double.valueOf(a.toString()), Double.valueOf(b.toString())));
                        case "!=":
                            return !(Objects.equals(Double.valueOf(a.toString()), Double.valueOf(b.toString())));
                        case ">":
                            return Double.valueOf(a.toString()) > Double.valueOf(b.toString());
                        case "<":
                            return Double.valueOf(a.toString()) < Double.valueOf(b.toString());
                        case ">=":
                            return Double.valueOf(a.toString()) >= Double.valueOf(b.toString());
                        case "<=":
                            return Double.valueOf(a.toString()) <= Double.valueOf(b.toString());
                        default: break;
                    }
                
            default:
                System.out.println("DEFAULT NUMEROS IGUALES");                
        }
        return false;
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="OSX"> 
    
    private int getTemporal()
    {
        this.osx_temporal++;
        return this.osx_temporal;
    }
    
    private int getEtiqueta()
    {
        this.osx_etiqueta++;
        return this.osx_etiqueta;
    }
    
    
    private void OSX_print3D(String ovalor)
    {
        if(ovalor!=null)
        {
            this.salida3D.append(ovalor);
            this.salida3D.append("\n");
            
            
//            //this.salida3DDouble.append((ovalor.toString()));
//            this.salida3DDouble.append(temporal);
//            this.salida3DDouble.append("\n");
        }
    }
    
    private void OSX_printIMPRESORA()
    {
        int t25 = this.getTemporal();//t25
        int t26 = this.getTemporal();//t26
        int t27 = this.getTemporal();//t27
        int L1 = this.getEtiqueta();//L1
        int L2 = this.getEtiqueta();//L2
        int L3 = this.getEtiqueta();//L3
        this.OSX_print3D("void impresora(){");
        
        this.OSX_print3D("t"+t25+" = p + 2;\t\t//Posicion parametro");
        this.OSX_print3D("t"+t26+"stack[t"+t25+"];\t\t//Direccion parametro");
        this.OSX_print3D("L"+L1+":\t\t\t//Inicio ciclo impresora");
        this.OSX_print3D("t"+t27+" = heap[t"+t26+"];\t\t//Caracter a ser impreso");
        this.OSX_print3D("if (t"+t27+" != 126) goto L"+L2+";");
        this.OSX_print3D("goto L"+L3+";");
        this.OSX_print3D("L"+L2+":\t\t\t//True ciclo impresora");        
        this.OSX_print3D("printf(\"%c\",t"+t27+");\t//printf(\"%c\",(int)t"+t27+");");
        this.OSX_print3D("t"+t26+" = t"+t26+" + 1;\t\t//Aumentar el Iterador");
        this.OSX_print3D("goto L"+L1+";");
        this.OSX_print3D("L"+L3+":\t\t\t//False ciclo impresora");
        this.OSX_print3D(";\n}\n");
        //this.OSX_printIMPRESORA
    }

    private void OSX_imprimir(int tam, int tempo, int tipo)
    {
        switch (tipo) 
        {
            case 1:
                this.OSX_print3D("\n\t\t\t//------- printf de int");
                this.OSX_print3D("printf(\"%d\",t"+tempo+");\t//printf(\"%d\",(int)t"+tempo+");");
                this.OSX_print3D("\n");
                break;
            case 2:
                this.OSX_print3D("\n\t\t\t//------- printf de double");
                this.OSX_print3D("printf(\"%f\",t"+tempo+");");
                this.OSX_print3D("\n");
                break;
            case 3:
                int t1=this.getTemporal();
                int t2=this.getTemporal();
                int t3=this.getTemporal();
                //this.OSX_print3D("")
                this.OSX_print3D("\n\t\t\t//------- printf de string");
                this.OSX_print3D("t"+t1+" = p + "+tam+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t2+" = t"+t1 +" + 2;\t\t//Pos parametro");
                this.OSX_print3D("t"+t3+" = t"+tempo+";\t\t//Valor a imprimir");
                this.OSX_print3D("stack[t"+t2+"] = t"+t3+";\t//Pasar valor");
                this.OSX_print3D("p = p + "+tam+";\t\t//Cambiar de ambito");
                this.OSX_print3D("\timpresora();\t\t//llamar funion q imprime");
                this.OSX_print3D("p = p - "+tam+";\t\t//Cambiar de ambito");
                this.OSX_print3D("\n");
                break;
            default:
                System.out.println("! ============================================================ Default, OSX_imprimir");
        }
    }    
    
    private int OSX_stringTO3D(String cadena)
    {
        int t0 = this.getTemporal();                
        
        this.OSX_print3D("t"+t0+" = h;\t\t\t//Posicion donde iniciara");        
        if(cadena.length()==0 || cadena=="")
        {
            int t1 = this.getTemporal();
            int t1_1 = this.getTemporal();
            
            this.OSX_print3D("t"+t1+" = h;\t\t//Posicion en donde se guardara");
            this.OSX_print3D("t"+t1_1+" = 32;\t\t//Ascii de Espacio");
            this.OSX_print3D("heap[t"+t1+" = t"+t1_1+";\t\t//Guardar en heap");
            this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
            this.crearFin();
        }
        else
        {
            //this.OSX_print3D("//Cadena: "+cadena);
            char[] caracteres = cadena.toCharArray();
            int tinicio;
            int tvalor;
            
            for(char carac:caracteres)
            {
                tinicio = this.getTemporal();
                tvalor = this.getTemporal();
                int el_ascii = this.toAscii(carac);
                this.OSX_print3D("t"+tinicio+" = h;\t\t//Posicion en donde guardar");
                this.OSX_print3D("t"+tvalor+" = "+el_ascii+";\t\t//Ascii de: "+carac);
                this.OSX_print3D("heap[t"+tinicio+"] = t"+tvalor+";\t\t//Guardar en heap");
                this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
                //this.OSX_print3D("");
            }
            this.crearFin();         
        }
        return t0;
    }
    
    private void crearFin()
    {
        int th = this.getTemporal();
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("heap[t"+th+"] = 126;\t\t//Guardar fin");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
    }    
    
    private int OSX_Aritmetico1(int t1, int izq, int t2, int der)
    {
        switch (izq) 
        {
            case 1:
                if(der==3){   return this.OSX_1mas3(t1, t2);  }
                break;
            case 2:
                if(der==3){   return this.OSX_1mas3(t1, t2);  }
                break;
            case 3:
                if(der==1){   return this.OSX_3mas1(t1, t2);  }
                if(der==2){   return this.OSX_3mas1(t1, t2);  }
                if(der==3){   return this.OSX_3mas3(t1, t2);  }
                if(der==4){   return this.OSX_3mas4(t1, t2);  }
                if(der==5){   return this.OSX_3mas1(t1, t2);  }
                break;
            case 4:
                if(der==3){   return this.OSX_4mas3(t1, t2);  }
                break;
            default:
                System.out.println("! ============================================================ Default, Aritmetico1: "+izq+" vs "+der);
        }
        return 1;
    }
    
    //int mas string        YA
    //string mas int        YA
    
    //double mas string     YA
    //string mas double     YA
    
    //string mas string     YA
    //char mas string       YA
    //string mas char       YA
    
    private int OSX_1mas3(int t1, int t2)
    {
        int treto = this.getTemporal();
        int th =this.getTemporal();
        int inta=this.getTemporal();
        this.OSX_print3D("t"+treto+" = h;\t\t//Pos. donde inicia");
        
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("t"+inta+" = t"+t1+" + 48;\t\t//48 para los int en ascii");
        this.OSX_print3D("heap[t"+th+"] = t"+inta+";\t\t//Guardar char");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia\n");
        
        int titera = this.getTemporal();
        int tval = this.getTemporal();
        th = this.getTemporal();
        int et1 = this.getEtiqueta();
        int et2 = this.getEtiqueta();
        int et3 = this.getEtiqueta();
        
        this.OSX_print3D("t"+titera+" = t"+t2+";\t\t//Iterador");
        this.OSX_print3D("\nL"+et1+":\t\t\t//Inicio for");
        this.OSX_print3D("t"+tval+" = heap[t"+titera+"];\t\t//Valor ascii guardado");
        this.OSX_print3D("if (t"+tval+" != 126) goto L"+et2+";");
        this.OSX_print3D("goto L"+et3+";");
        this.OSX_print3D("\nL"+et2+":\t\t\t//Verdadero del ciclo");
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("heap[t"+th+"] = t"+tval+";");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
        this.OSX_print3D("t"+titera+" = t"+titera+"+ 1;\t\t//Aumento el Iterador");
        this.OSX_print3D("goto L"+et1+";\t\t//Regresar a Evaluar");        
        this.OSX_print3D("\nL"+et3+":\t\t\t//Falso del ciclo\n");

        this.crearFin();
        
        return treto;
    }
    
    private int OSX_3mas1(int t1, int t2)
    {
        int treto = this.getTemporal();
        
        int titera = this.getTemporal();
        int tval = this.getTemporal();
        int th =this.getTemporal();        
        int et1 = this.getEtiqueta();
        int et2 = this.getEtiqueta();
        int et3 = this.getEtiqueta();
        
        this.OSX_print3D("t"+treto+" = h;\t\t//Pos. donde inicia");
        
        this.OSX_print3D("t"+titera+" = t"+t1+";\t\t//Iterador");
        this.OSX_print3D("\nL"+et1+":\t\t\t//Inicio for\n");
        this.OSX_print3D("t"+tval+" = heap[t"+titera+"];\t\t//Valor ascii guardado");
        this.OSX_print3D("if (t"+tval+" != 126) goto L"+et2+";");
        this.OSX_print3D("goto L"+et3+";");
        this.OSX_print3D("\nL"+et2+":\t\t\t//Verdadero del ciclo\n");
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("heap[t"+th+"] = t"+tval+";");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
        this.OSX_print3D("t"+titera+" = t"+titera+"+ 1;\t\t//Aumento el Iterador");
        this.OSX_print3D("goto L"+et1+";\t\t//Regresar a Evaluar");        
        this.OSX_print3D("\nL"+et3+":\t\t\t//Falso del ciclo\n");
        
        th = this.getTemporal();
        int inta = this.getTemporal();
        this.OSX_print3D("//-------\t\tCodigo para el Operando int");
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("t"+inta+" = t"+t2+" + 48;\t\t//48 para los int en ascii");
        this.OSX_print3D("heap[t"+th+"] = t"+inta+";\t\t//Guardar char");
        this.OSX_print3D("h = h + 1;\t\t\t//h apunta a posicion vacia\n");
        
        this.crearFin();
        
        return treto;
    }
    
    private int OSX_3mas3(int t1,int t2)
    {
        int treto = this.getTemporal();
        
        int titera = this.getTemporal();
        int tval = this.getTemporal();
        int th =this.getTemporal();        
        int et1 = this.getEtiqueta();
        int et2 = this.getEtiqueta();
        int et3 = this.getEtiqueta();
        
        this.OSX_print3D("t"+treto+" = h;\t\t//Pos. donde inicia");
        
        this.OSX_print3D("t"+titera+" = t"+t1+";\t\t//Iterador");
        this.OSX_print3D("\nL"+et1+":\t\t\t//Inicio for\n");
        this.OSX_print3D("t"+tval+" = heap[t"+titera+"];\t\t//Valor ascii guardado");
        this.OSX_print3D("if (t"+tval+" != 126) goto L"+et2+";");
        this.OSX_print3D("goto L"+et3+";");
        this.OSX_print3D("\nL"+et2+":\t\t\t//Verdadero del ciclo\n");
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("heap[t"+th+"] = t"+tval+";");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
        this.OSX_print3D("t"+titera+" = t"+titera+"+ 1;\t\t//Aumento el Iterador");
        this.OSX_print3D("goto L"+et1+";\t\t//Regresar a Evaluar");        
        this.OSX_print3D("\nL"+et3+":\t\t\t//Falso del ciclo\n");
        
        titera = this.getTemporal();
        tval = this.getTemporal();
        th =this.getTemporal();
        
        et1 = this.getEtiqueta();
        et2 = this.getEtiqueta();
        et3 = this.getEtiqueta();
        
        this.OSX_print3D("t"+titera+" = t"+t2+";\t\t//Iterador");
        this.OSX_print3D("\nL"+et1+":\t\t\t//Inicio for");
        this.OSX_print3D("t"+tval+" = heap[t"+titera+"];\t\t//Valor ascii guardado");
        this.OSX_print3D("if (t"+tval+" != 126) goto L"+et2+";");
        this.OSX_print3D("goto L"+et3+";");
        this.OSX_print3D("\nL"+et2+":\t\t\t//Verdadero del ciclo");
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("heap[t"+th+"] = t"+tval+";");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
        this.OSX_print3D("t"+titera+" = t"+titera+"+ 1;\t\t//Aumento el Iterador");
        this.OSX_print3D("goto L"+et1+";\t\t//Regresar a Evaluar");        
        this.OSX_print3D("\nL"+et3+":\t\t\t//Falso del ciclo");

        this.crearFin();
        
        return treto;
    }
    
    private int OSX_3mas4(int t1, int t2)
    {
        int treto = this.getTemporal();
        
        int titera = this.getTemporal();
        int tval = this.getTemporal();
        int th =this.getTemporal();        
        int et1 = this.getEtiqueta();
        int et2 = this.getEtiqueta();
        int et3 = this.getEtiqueta();
        
        this.OSX_print3D("t"+treto+" = h;\t\t//Pos. donde inicia");
        
        this.OSX_print3D("t"+titera+" = t"+t1+";\t\t//Iterador");
        this.OSX_print3D("\nL"+et1+":\t\t\t//Inicio for\n");
        this.OSX_print3D("t"+tval+" = heap[t"+titera+"];\t\t//Valor ascii guardado");
        this.OSX_print3D("if (t"+tval+" != 126) goto L"+et2+";");
        this.OSX_print3D("goto L"+et3+";");
        this.OSX_print3D("\nL"+et2+":\t\t\t//Verdadero del ciclo\n");
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("heap[t"+th+"] = t"+tval+";");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
        this.OSX_print3D("t"+titera+" = t"+titera+"+ 1;\t\t//Aumento el Iterador");
        this.OSX_print3D("goto L"+et1+";\t\t//Regresar a Evaluar");        
        this.OSX_print3D("\nL"+et3+":\t\t\t//Falso del ciclo\n");
        
        th = this.getTemporal();
        int tchar = this.getTemporal();
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("t"+tchar+" = heap["+t2+"];\t\t//Char Ascii en heap");
        this.OSX_print3D("heap[t"+th+"] = t"+tchar+";\t\t//Guardar char");
        this.OSX_print3D("h = h + 1;\t\t\t//h apunta a posicion vacia\n");
        
        this.crearFin();
        
        return treto;
    }
    
    private int OSX_4mas3(int t1, int t2)
    {
        int treto = this.getTemporal();        
        int th =this.getTemporal();                
        
        this.OSX_print3D("t"+treto+" = h;\t\t//Pos. donde inicia");
        
        int tchar = this.getTemporal();
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("t"+tchar+" = heap["+t1+"];\t\t//Char Ascii en heap");
        this.OSX_print3D("heap[t"+th+"] = t"+tchar+";\t\t//Guardar char");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia\n");
        
        int titera = this.getTemporal();
        int tval = this.getTemporal();
        th = this.getTemporal();
        int et1 = this.getEtiqueta();
        int et2 = this.getEtiqueta();
        int et3 = this.getEtiqueta();
        
        this.OSX_print3D("t"+titera+" = t"+t2+";\t\t//Iterador");
        this.OSX_print3D("\nL"+et1+":\t\t\t//Inicio for\n");
        this.OSX_print3D("t"+tval+" = heap[t"+titera+"];\t\t//Valor ascii guardado");
        this.OSX_print3D("if (t"+tval+" != 126) goto L"+et2+";");
        this.OSX_print3D("goto L"+et3+";");
        this.OSX_print3D("\nL"+et2+":\t\t\t//Verdadero del ciclo\n");
        this.OSX_print3D("t"+th+" = h;\t\t//posicion donde se guardara");
        this.OSX_print3D("heap[t"+th+"] = t"+tval+";");
        this.OSX_print3D("h = h + 1;\t\t//h apunta a posicion vacia");
        this.OSX_print3D("t"+titera+" = t"+titera+"+ 1;\t\t//Aumento el Iterador");
        this.OSX_print3D("goto L"+et1+";\t\t//Regresar a Evaluar");        
        this.OSX_print3D("\nL"+et3+":\t\t\t//Falso del ciclo\n");

        this.crearFin();
        
        return treto;
    }
           
    ////Busca en la tabla de simbolos y devulve un temporal con la direccion
    public int OSX_getRegistro(String sc, String sf, String scadena_p2, String sr)
    {
        ////Caso acceso de 1
        if(sr.contains(".")==false)
        {
            ////Primero parametros, variable local, variable global(Atributo)
            if(ts.esParam_Variable(sc, sf, scadena_p2, sr, superpadre)==true)
            {
                ////Es un parametro, variable
                return this.OSX_getRegistroPVAR(sc, sf, scadena_p2, sr);
            }
            else
            {
                ////Es un atributo
                return this.OSX_getRegistroTHIS(sc, sr);
            }
        }
        
        ////Caso acceso de n registros: hola.saludo, persona.nacimiento.fecha, etc.
        //String registros[] = sr.split(".");
        String rr[]=sr.split("\\.");
        String r1 = rr[0];
        //Ver si el primer resgistro es un atributo sino es parametro o variable
        ////Primer Registro: parametros, variable local, variable global(Atributo)
        if(ts.esParam_Variable(sc, sf, scadena_p2, r1, superpadre)==true)
        {
            ////Es un parametro, variable
            return this.OSX_getRegistroPVAR(sc, sf, scadena_p2, sr);
        }
        else
        {
            ////Es un atributo
            return this.OSX_getRegistroTHIS(sc, sr);
        }
    }
            
    public int OSX_getRegistroTHIS(String sc, String sr)
    {
        System.out.println("! ============================================================ get Registro THIS");
        System.out.println("Clase: "+sc);
        System.out.println("Registro: "+sr);
        
        sr = sr.replace("[:THIS:].", "");
        
        if(sr.contains(".")==false)
        {   
            int tthis = this.getTemporal();
            int tdir = this.getTemporal();
            int tpos = this.getTemporal();
            int tval = this.getTemporal();
            int tamaniopos = this.ts.getPosAtributo(sc, sr, superpadre);            
            this.OSX_print3D("t"+tthis+" = p + 0;\t\t//this objeto actual");
            this.OSX_print3D("t"+tdir+" = stack[t"+tthis+"];\t//dir objeto actual en heap");
            this.OSX_print3D("t"+tpos+" = t"+tdir+" + "+tamaniopos+";\t\t//pos relativa atributo");
            this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+sr);
            this.osx_auxtipo=ts.OSX_auxtipo;
            return tval;
        }
        
        //String[]rr = sr.split(".");        
        String rr[]=sr.split("\\.");
        int tthis = this.getTemporal();
        int tdir = this.getTemporal();
        int tpos = this.getTemporal();
        int tval = this.getTemporal();
        int tamaniopos = this.ts.getPosAtributo(sc, rr[0], superpadre);
        this.OSX_print3D("t"+tthis+" = p + 0;\t\t//this objeto actual");
        this.OSX_print3D("t"+tdir+" = stack[t"+tthis+"];\t//dir objeto actual en heap");
        this.OSX_print3D("t"+tpos+" = t"+tdir+" + "+tamaniopos+";\t\t//pos relativa atributo");
        this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[0]);
                
        for (int i = 1; i < rr.length; i++) 
        {                        
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            ///Modifica el ts.osx_variable 
            tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
            
            tpos = this.getTemporal();            
            this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
            tval = this.getTemporal();
            this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
        }      
        this.osx_auxtipo=ts.OSX_auxtipo;
        return tval;        
    }
    
    public int OSX_getRegistroPVAR(String sc, String sf, String scadena_p2, String sr)
    {        
        System.out.println("! ============================================================ get Registro PVAR");
        System.out.println("Clase: "+sc);
        System.out.println("Funcion: "+sf);
        System.out.println("Cadena: "+scadena_p2);
        System.out.println("Registro: "+sr);
        
        //Caso 1, solo de uno: numero, hola, etc.
        if(sr.contains(".")==false)
        {               
            int tpos = this.getTemporal();
            int tval = this.getTemporal();
            int tamaniopos = this.ts.getPosPVAR(sc, sf, scadena_p2, sr, this.superpadre);
            this.OSX_print3D("t"+tpos+" = p + "+tamaniopos+";\t\t//pos relativa variable");
            this.OSX_print3D("t"+tval+" = stack[t"+tpos+"];\t//variable "+sr);
            this.osx_auxtipo=ts.OSX_auxtipo;
            return tval;
        }
        
        //Caso 2, multiples registros: THIS.persona.edad, THIS.miobjeto.cumpleanios.dia      
        //String[]rr = sr.split(".");
        String rr[]=sr.split("\\.");
        int tamaniopos = this.ts.getPosPVAR(sc, sf, scadena_p2, rr[0], superpadre);
        int tpos = this.getTemporal();
        int tval = this.getTemporal();          
        this.OSX_print3D("t"+tpos+" = p + "+tamaniopos+";\t\t//pos relativa variable");
        this.OSX_print3D("t"+tval+" = stack[t"+tpos+"];\t//variable "+rr[0]);
                
        for (int i = 1; i < rr.length; i++) 
        {                        
            if(Compilador.listaErrores.size()>0) 
            {break;}
                        
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");} 
            
            ///Modifica el ts.osx_variable 
            tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
            
            //int tdir2 = this.getTemporal();
            tpos = this.getTemporal();           
            this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
            tval = this.getTemporal();
            this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
        }
        this.osx_auxtipo=ts.OSX_auxtipo;
        return tval;  
    }
        
    public int OSX_getPosicionRegistro(String sc, String sf, String scadena_p2, String sr)
    {
        ////Caso acceso de 1
        if(sr.contains(".")==false)
        {
            ////Primero parametros, variable local, variable global(Atributo)
            if(ts.esParam_Variable(sc, sf, scadena_p2, sr, superpadre)==true)
            {
                ////Es un parametro, variable
                return this.OSX_getPosicionRegistroPVAR(sc, sf, scadena_p2, sr);
            }
            else
            {
                ////Es un atributo
                return this.OSX_getPosicionRegistroTHIS(sc, sr);
            }
        }
        
        ////Caso acceso de n registros: hola.saludo, persona.nacimiento.fecha, etc.
        //String registros[] = sr.split(".");
        String registros[]=sr.split("\\.");
        String r1 = registros[0];
        //Ver si el primer resgistro es un atributo sino es parametro o variable
        ////Primer Registro: parametros, variable local, variable global(Atributo)
        if(ts.esParam_Variable(sc, sf, scadena_p2, r1, superpadre)==true)
        {
            ////Es un parametro, variable
            return this.OSX_getPosicionRegistroPVAR(sc, sf, scadena_p2, sr);
        }
        else
        {
            ////Es un atributo
            return this.OSX_getPosicionRegistroTHIS(sc, sr);
        }
    }
        
    public int OSX_getPosicionRegistroTHIS(String sc, String sr)
    {
        System.out.println("! ============================================================ get posicion THIS");
        System.out.println("Clase: "+sc);
        System.out.println("Registro: "+sr);
        
        sr = sr.replace("[:THIS:].", "");
        
        if(sr.contains(".")==false)
        {   
            int tthis = this.getTemporal();
            int tdir = this.getTemporal();
            int tpos = this.getTemporal();
            int tamaniopos = this.ts.getPosAtributo(sc, sr, superpadre);            
            this.OSX_print3D("t"+tthis+" = p + 0;\t\t//this objeto actual");
            this.OSX_print3D("t"+tdir+" = stack[t"+tthis+"];\t//dir objeto actual en heap");
            this.OSX_print3D("t"+tpos+" = t"+tdir+" + "+tamaniopos+";\t\t//pos relativa atributo");
            this.osx_auxtipo=ts.OSX_auxtipo;
            this.osx_auxlugar="heap";
            return tpos;
        }
        
        //String[]rr = sr.split(".");        
        String rr[]=sr.split("\\.");
        int tthis = this.getTemporal();
        int tdir = this.getTemporal();
        int tpos = this.getTemporal();
        int tval = this.getTemporal();
        int tamaniopos = this.ts.getPosAtributo(sc, rr[0], superpadre);
        this.OSX_print3D("t"+tthis+" = p + 0;\t\t//this objeto actual");
        this.OSX_print3D("t"+tdir+" = stack[t"+tthis+"];\t//dir objeto actual en heap");
        this.OSX_print3D("t"+tpos+" = t"+tdir+" + "+tamaniopos+";\t\t//pos relativa atributo");
        this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[0]);
                
        for (int i = 1; i < rr.length; i++) 
        {                        
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}            
            
            ///Modifica el ts.osx_variable 
            tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
            
            tpos = this.getTemporal();            
            this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");            
            
            if((i+1)<rr.length)
            {
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }      
        this.osx_auxtipo=ts.OSX_auxtipo;
        this.osx_auxlugar="heap";
        return tpos;
    }
    
    public int OSX_getPosicionRegistroPVAR(String sc, String sf, String scadena_p2, String sr)
    {        
        System.out.println("! ============================================================ get Posicion Normal");
        System.out.println("Clase: "+sc);
        System.out.println("Funcion: "+sf);
        System.out.println("Cadena: "+scadena_p2);
        System.out.println("Registro: "+sr);
        
        //Caso 1, solo de uno: numero, hola, etc.
        if(sr.contains(".")==false)
        {               
            int tpos = this.getTemporal();
            int tamaniopos = this.ts.getPosPVAR(sc, sf, scadena_p2, sr, this.superpadre);
            this.OSX_print3D("t"+tpos+" = p + "+tamaniopos+";\t\t//pos relativa variable");
            this.osx_auxtipo=ts.OSX_auxtipo;
            this.osx_auxlugar="stack";
            return tpos;
        }
        
        //Caso 2, multiples registros: THIS.persona.edad, THIS.miobjeto.cumpleanios.dia      
        //String[]rr = sr.split(".");
        String rr[]=sr.split("\\.");

        int tamaniopos = this.ts.getPosPVAR(sc, sf, scadena_p2, rr[0], superpadre);
        int tpos = this.getTemporal();
        int tval = this.getTemporal();          
        this.OSX_print3D("t"+tpos+" = p + "+tamaniopos+";\t\t//pos relativa variable");
        this.OSX_print3D("t"+tval+" = stack[t"+tpos+"];\t//variable "+rr[0]);
                
        for (int i = 1; i < rr.length; i++) 
        {                        
            if(Compilador.listaErrores.size()>0) 
            {break;}
                        
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");} 
            
            ///Modifica el ts.osx_variable 
            tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
            
            //int tdir2 = this.getTemporal();
            tpos = this.getTemporal();           
            this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");            
            if((i+1)<rr.length)
            {
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
        this.osx_auxtipo=ts.OSX_auxtipo;
        this.osx_auxlugar="heap";
        return tpos; 
    }
   
    //Clase donde llamos, Funcion donde llamo, la cadena q identifica los tipos de atributos pasados, el registro, a llamar
    public void OSX_llamada_i(String laclase, String lafuncion,String cactual, int eltamanio, int iparams, String sr)
    {        
        //Validar q sea de 1 id
        if(sr.contains(".")==false)
        {
            //THIS sin parametros
            if(iparams==0)
            {
                this.OSX_llamada_i_TS(laclase, lafuncion, eltamanio, sr);
                return;
            }            
            //THIS con parametros
            System.out.println("a");
            //XXXXXXX llamar funcion de this con parametros    
            this.OSX_llamada_d_TSMULTI(laclase, lafuncion, eltamanio, iparams, sr);
            return;
        }
        ////Caso acceso de n registros: hola.saludo, persona.nacimiento.fecha, etc.
        //StringBuilder tempo =new StringBuilder();
        //tempo.append(sr.replace(".", ":"));
        //String []rr = tempo.toString().split(":");
        String rr[]=sr.split("\\.");
        String r1 = rr[0];
        //Ver si el primer resgistro es un atributo sino es parametro o variable
        
        Boolean esParametro = ts.esParam_Variable(laclase, lafuncion, cactual, r1, superpadre);
        if(esParametro==true)
        {
            if(iparams==0)
            {
                this.OSX_llamada_i_VS(laclase, lafuncion, cactual, eltamanio, sr);
            }
            else
            {                        
                //es Variable con parametros
                this.OSX_llamada_i_VSMULTI(laclase, lafuncion, cactual, eltamanio, iparams, sr);
            }
        }
        else
        {
            if(iparams==0)
            {
                this.OSX_llamada_i_TS(laclase, lafuncion, eltamanio, sr);
            }
            else
            {
                //es THIS con parametros
                this.OSX_llamada_i_TSMULTI(laclase, lafuncion, eltamanio, iparams, sr);
            }
        }
        
    }
    
    //llamada que empieza con this, sin parametros
    public void OSX_llamada_i_TS(String laclase, String lafuncion, int eltamanio, String elregistro)
    {
        System.out.println("! ============================================================ Izquierda, llamada THIS Simple");
        System.out.println("Clase: "+laclase);
        System.out.println("Funcion actual: "+lafuncion);
        System.out.println("Tamanio actual: "+eltamanio);
        System.out.println("Registro: "+elregistro);
        String sr = elregistro.replace("[:THIS:].", "").trim();                
        if(sr.contains(".")==false)
        {
            //es llamada simple:
            int t1 =this.getTemporal();int t2 =this.getTemporal();
            int t3 =this.getTemporal();int t4 =this.getTemporal();
            this.OSX_print3D("t"+t1+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
            this.OSX_print3D("t"+t2+" = t"+t1+" + 0;\t\t//Pos this, funcion a llamar");
            this.OSX_print3D("t"+t3+" = p + 0;\t\t//Pos this, funcion actual");
            this.OSX_print3D("t"+t4+" = stack[t"+t3+"];\t//Dir this, funcion actual");
            this.OSX_print3D("stack[t"+t2+"] = t"+t4+";\t//Pasar this actual a this a llamar");
            this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
            this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"();//call");
            this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
            return;
        }
        //String rr[]=sr.replace(".", ":").split(":");
        String rr[]=sr.split("\\.");
        int tthis = this.getTemporal();
        int tdir = this.getTemporal();
        int tpos = this.getTemporal();
        int tval = this.getTemporal();
        int tamaniopos = this.ts.getPosAtributo(laclase, rr[0], superpadre);
        this.OSX_print3D("t"+tthis+" = p + 0;\t\t//this objeto actual");
        this.OSX_print3D("t"+tdir+" = stack[t"+tthis+"];\t//dir objeto actual en heap");
        this.OSX_print3D("t"+tpos+" = t"+tdir+" + "+tamaniopos+";\t\t//pos relativa atributo");
        this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[0]);
        
        for (int i = 1; i < rr.length; i++) 
        {
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            //Se llego a la funcion :D
            if((i+1)==rr.length)
            {
                int t7=this.getTemporal();int t8=this.getTemporal();
                int t9=this.getTemporal();
                this.OSX_print3D("t"+t7+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t8+" = t"+t7+"+ 0;\t\t//this funcion a llamar");
                this.OSX_print3D("t"+t9+" = t"+tval+";\t\t//valor this a pasar");
                this.OSX_print3D("stack[t"+t8+"] = t"+t9+";\t//pasar this a funcion");                
                this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
                if(ts.getVisibilidad(atributo.clase, rr[i], "", superpadre)!=1)
                {
                    String descri = "Funcion no visible (Private, Protected)";
                    this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                }
                this.OSX_print3D("\tCLASE_"+atributo.clase+"_FUNCION_"+rr[i]+"();//call");             
                this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
                return;
            }
            else
            {                
                //aun estoy en un atributo                
                tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
                tpos = this.getTemporal();            
                this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
    }
    
    public void OSX_llamada_i_VS(String laclase, String lafuncion, String cadenaf, int eltamanio, String elregistro)
    {
       System.out.println("! ============================================================ Izquierda, llamada Var Simple");
        System.out.println("Clase: "+laclase);
        System.out.println("Funcion actual: "+lafuncion);
        System.out.println("Tamanio actual: "+eltamanio);
        System.out.println("Cadena Parametros Actual: "+cadenaf);
        System.out.println("Registro: "+elregistro);
        String sr = elregistro;
        if(sr.contains(".")==false)
        {
            //es llamada simple:
            int t1 =this.getTemporal();int t2 =this.getTemporal();
            int t3 =this.getTemporal();int t4 =this.getTemporal();
            this.OSX_print3D("t"+t1+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
            this.OSX_print3D("t"+t2+" = t"+t1+" + 0;\t\t//Pos this, funcion a llamar");
            this.OSX_print3D("t"+t3+" = p + 0;\t\t//Pos this, funcion actual");
            this.OSX_print3D("t"+t4+" = stack[t"+t3+"];\t//Dir this, funcion actual");
            this.OSX_print3D("stack[t"+t2+"] = t"+t4+";\t//Pasar this actual a this a llamar");
            this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
            this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"();//call");
            this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
            return;
        }
        //String rr[]=sr.replace(".", ":").split(":");
        String rr[]=sr.split("\\.");
        int tamaniopos = this.ts.getPosPVAR(laclase, lafuncion,cadenaf, rr[0], superpadre);
        int tpos = this.getTemporal();
        int tval = this.getTemporal();        
        this.OSX_print3D("t"+tpos+" = p + "+tamaniopos+";\t\t//pos relativa variable");
        this.OSX_print3D("t"+tval+" = stack[t"+tpos+"];\t//variable "+rr[0]);
        
        for (int i = 1; i < rr.length; i++) 
        {
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            //Se llego a la funcion :D
            if((i+1)==rr.length)
            {
                int t7=this.getTemporal();int t8=this.getTemporal();
                int t9=this.getTemporal();
                this.OSX_print3D("t"+t7+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t8+" = t"+t7+" + 0;\t\t//this funcion a llamar");
                this.OSX_print3D("t"+t9+" = t"+tval+";\t\t//valor this a pasar");
                this.OSX_print3D("stack[t"+t8+"] = t"+t9+";\t//pasar this a funcion");                
                this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
                if(ts.getVisibilidad(atributo.clase, rr[i], "", superpadre)!=1)
                {
                    String descri = "Funcion no visible (Private, Protected)";
                    this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                }
                this.OSX_print3D("\tCLASE_"+atributo.clase+"_FUNCION_"+rr[i]+"();//call");
                this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
                return;
            }
            else
            {                
                //aun estoy en un atributo                
                tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
                tpos = this.getTemporal();
                this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
    }
    
    public int OSX_llamada_d(String laclase, String lafuncion,int eltamanio, String cactual, int iparams, String sr)
    {
        //Validar q sea de 1 id
        if(sr.contains(".")==false)
        {
            //THIS sin parametros
            if(iparams==0)
            {
                return this.OSX_llamada_d_TS(laclase, lafuncion, eltamanio, sr);
            }            
            //THIS con parametros
            //System.out.println("a");
            //XXXXXXX llamar funcion de this con parametros 
            return this.OSX_llamada_d_TSMULTI(laclase, lafuncion, eltamanio, iparams, sr);
            //return 1000;
        }
        ////Caso acceso de n registros: hola.saludo, persona.nacimiento.fecha, etc.
        //StringBuilder tempo =new StringBuilder();
        //tempo.append(sr.replace(".", ":"));
        //String []rr = tempo.toString().split(":");
        String rr[]=sr.split("\\.");
        String r1 = rr[0];
        //Ver si el primer resgistro es un atributo sino es parametro o variable
        
        Boolean esParametro = ts.esParam_Variable(laclase, lafuncion, cactual, r1, superpadre);
        if(esParametro==true)
        {
            if(iparams==0)
            {
                return this.OSX_llamada_d_VS(laclase, lafuncion, cactual, eltamanio, sr);
            }
            else
            {                        
                //es Variable con parametros
                return this.OSX_llamada_d_VSMULTI(laclase, lafuncion, cactual, eltamanio, iparams, sr);
            }
        }
        else
        {
            if(iparams==0)
            {
                return this.OSX_llamada_d_TS(laclase, lafuncion, eltamanio, sr);
            }
            else
            {
                //es THIS con parametros
                return this.OSX_llamada_d_TSMULTI(laclase, lafuncion, eltamanio, iparams, sr);
            }
        }
        //return 3500;
    }
    
    public int OSX_llamada_d_TS(String laclase, String lafuncion, int eltamanio, String elregistro)
    {
        System.out.println("! ============================================================ Derecha, llamada This Simple");
        System.out.println("Clase: "+laclase);
        System.out.println("Funcion actual: "+lafuncion);
        System.out.println("Tamanio actual: "+eltamanio);
        System.out.println("Registro: "+elregistro);
        String sr = elregistro.replace("[:THIS:].", "").trim();                
        if(sr.contains(".")==false)
        {
            //es llamada simple:
            int t1 =this.getTemporal();int t2 =this.getTemporal();
            int t3 =this.getTemporal();int t4 =this.getTemporal();
            int t5 =this.getTemporal();int t6 =this.getTemporal();
            this.OSX_print3D("t"+t1+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
            this.OSX_print3D("t"+t2+" = t"+t1+" + 0;\t\t//Pos this, funcion a llamar");
            this.OSX_print3D("t"+t3+" = p + 0;\t\t//Pos this, funcion actual");
            this.OSX_print3D("t"+t4+" = stack[t"+t3+"];\t//Dir this, funcion actual");
            this.OSX_print3D("stack[t"+t2+"] = t"+t4+";\t//Pasar this actual a this a llamar");
            this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
            this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"();//call");
            this.OSX_print3D("t"+t5+" = p + 1;\t\t//Pos retorno, funcion llamada");
            this.OSX_print3D("t"+t6+" = stack[t"+t5+"];\t//Obtener el retorno");
            this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
            this.osx_auxtipo = ts.OSX_auxtipo;
            return t6;
        }
        //String rr[]=sr.split(".");
        String rr[]=sr.split("\\.");
        int tthis = this.getTemporal();
        int tdir = this.getTemporal();
        int tpos = this.getTemporal();
        int tval = this.getTemporal();
        int tamaniopos = this.ts.getPosAtributo(laclase, rr[0], superpadre);
        this.OSX_print3D("t"+tthis+" = p + 0;\t\t//this objeto actual");
        this.OSX_print3D("t"+tdir+" = stack[t"+tthis+"];\t//dir objeto actual en heap");
        this.OSX_print3D("t"+tpos+" = t"+tdir+" + "+tamaniopos+";\t\t//pos relativa atributo");
        this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[0]);
        
        for (int i = 1; i < rr.length; i++) 
        {
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            //Se llego a la funcion :D
            if((i+1)==rr.length)
            {
                int t11=this.getTemporal();int t12=this.getTemporal();
                int t13=this.getTemporal();
                int t17=this.getTemporal();int t18=this.getTemporal();
                this.OSX_print3D("t"+t11+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t12+" = t"+t11+" + 0;\t\t//this funcion a llamar");
                this.OSX_print3D("t"+t13+" = t"+tval+";\t\t//valor this a pasar");
                this.OSX_print3D("stack[t"+t12+"] = t"+t13+";\t//pasar this a funcion");                
                this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
                if(ts.getVisibilidad(atributo.clase, rr[i], "", superpadre)!=1)
                {
                    String descri = "Funcion no visible (Private, Protected)";
                    this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                }
                this.OSX_print3D("\tCLASE_"+atributo.clase+"_FUNCION_"+rr[i]+"();//call");
                this.OSX_print3D("t"+t17+" = p + 1;\t\t//Pos retorno, funcion llamada");
                this.OSX_print3D("t"+t18+" = stack[t"+t17+"];\t//Obtener el retorno");
                this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
                this.osx_auxtipo = ts.OSX_auxtipo;
                return t18;                
            }
            else
            {                
                //aun estoy en un atributo                
                tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
                tpos = this.getTemporal();            
                this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
        return 1000;
    }
    
    public int OSX_llamada_d_VS(String laclase, String lafuncion, String cadenaf, int eltamanio, String elregistro)
    {
        System.out.println("! ============================================================ Derecha, llamada Var Simple");
        System.out.println("Clase: "+laclase);
        System.out.println("Funcion actual: "+lafuncion);
        System.out.println("Tamanio actual: "+eltamanio);
        System.out.println("Cadena Parametros Actual: "+cadenaf);
        System.out.println("Registro: "+elregistro);
        String sr = elregistro;             
        if(sr.contains(".")==false)
        {
            //es llamada simple:
            int t1 =this.getTemporal();int t2 =this.getTemporal();
            int t3 =this.getTemporal();int t4 =this.getTemporal();
            int t5 =this.getTemporal();int t6 =this.getTemporal();
            this.OSX_print3D("t"+t1+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
            this.OSX_print3D("t"+t2+" = t"+t1+" + 0;\t\t//Pos this, funcion a llamar");
            this.OSX_print3D("t"+t3+" = p + 0;\t\t//Pos this, funcion actual");
            this.OSX_print3D("t"+t4+" = stack[t"+t3+"];\t//Dir this, funcion actual");
            this.OSX_print3D("stack[t"+t2+"] = t"+t4+";\t//Pasar this actual a this a llamar");
            this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
            this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"();//call");
            this.OSX_print3D("t"+t5+" = p + 1;\t\t//Pos retorno, funcion llamada");
            this.OSX_print3D("t"+t6+" = stack[t"+t5+"];\t//Obtener el retorno");
            this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
            this.osx_auxtipo=ts.OSX_auxtipo;
            return t6;
        }
        String rr[]=sr.split("\\.");
        int tpos = this.getTemporal();
        int tval = this.getTemporal();
        int tamaniopos = this.ts.getPosPVAR(laclase, lafuncion, cadenaf, rr[0], superpadre);
        this.OSX_print3D("t"+tpos+" = p + "+tamaniopos+";\t\t//pos relativa variable");
        this.OSX_print3D("t"+tval+" = stack[t"+tpos+"];\t//variable "+rr[0]);
        
        for (int i = 1; i < rr.length; i++) 
        {
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            //Se llego a la funcion :D
            if((i+1)==rr.length)
            {
                int t11=this.getTemporal();int t12=this.getTemporal();
                int t13=this.getTemporal();
                int t17=this.getTemporal();int t18=this.getTemporal();
                this.OSX_print3D("t"+t11+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t12+" = t"+t11+" + 0;\t\t//this funcion a llamar");
                this.OSX_print3D("t"+t13+" = t"+tval+";\t\t//valor this a pasar");
                this.OSX_print3D("stack[t"+t12+"] = t"+t13+";\t//pasar this a funcion");                
                this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
                if(ts.getVisibilidad(atributo.clase, rr[i], "", superpadre)!=1)
                {
                    String descri = "Funcion no visible (Private, Protected)";
                    this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                }
                this.OSX_print3D("\tCLASE_"+atributo.clase+"_FUNCION_"+rr[i]+"();//call");
                this.OSX_print3D("t"+t17+" = p + 1;\t\t//Pos retorno, funcion llamada");
                this.OSX_print3D("t"+t18+" = stack[t"+t17+"];\t//Obtener el retorno");
                this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
                this.osx_auxtipo=ts.OSX_auxtipo;
                return t18;
            }
            else
            {                
                //aun estoy en un atributo                
                //tamaniopos = this.ts.getPosPVAR(laclase, lafuncion, "", rr[i], superpadre);
                tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
                tpos = this.getTemporal();            
                this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
        return 1000;
    }
    
    
    //</editor-fold>     
    
    public void OSX_llamada_i_TSMULTI(String laclase, String lafuncion, int eltamanio, int parametros, String elregistro)
    {
        System.out.println("! ============================================================ Izquierda, llamada THIS Multi");
        System.out.println("Clase: "+laclase);
        System.out.println("Funcion actual: "+lafuncion);
        System.out.println("Tamanio actual: "+eltamanio);
        System.out.println("Registro: "+elregistro);
        String sr = elregistro.replace("[:THIS:].", "").trim();                
        if(sr.contains(".")==false)
        {
            //es llamada simple:
            int t1 =this.getTemporal();int t2 =this.getTemporal();
            int t3 =this.getTemporal();int t4 =this.getTemporal();
            this.OSX_print3D("t"+t1+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
            this.OSX_print3D("t"+t2+" = t"+t1+" + 0;\t\t//Pos this, funcion a llamar");
            this.OSX_print3D("t"+t3+" = p + 0;\t\t//Pos this, funcion actual");
            this.OSX_print3D("t"+t4+" = stack[t"+t3+"];\t//Dir this, funcion actual");
            this.OSX_print3D("stack[t"+t2+"] = t"+t4+";\t//Pasar this actual a this a llamar");
            this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
            FuncionO fsol = ts.getFuncion_NPARAMETROS(laclase, sr, parametros);
            if(fsol==null)
            {
                String descri = "Funcion I THIS MULTI, no existe con: "+parametros+" parametros";
                this.guardarError(sr, superpadre.linea, superpadre.columna, "Semantico", descri);
                return;
            }
            this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"_PARAMS_"+fsol.cadenap1+"();//call");
            this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
            return;
        }
        //String rr[]=sr.replace(".", ":").split(":");
        String rr[]=sr.split("\\.");
        int tthis = this.getTemporal();
        int tdir = this.getTemporal();
        int tpos = this.getTemporal();
        int tval = this.getTemporal();
        int tamaniopos = this.ts.getPosAtributo(laclase, rr[0], superpadre);
        this.OSX_print3D("t"+tthis+" = p + 0;\t\t//this objeto actual");
        this.OSX_print3D("t"+tdir+" = stack[t"+tthis+"];\t//dir objeto actual en heap");
        this.OSX_print3D("t"+tpos+" = t"+tdir+" + "+tamaniopos+";\t\t//pos relativa atributo");
        this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[0]);
        
        for (int i = 1; i < rr.length; i++) 
        {
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            //Se llego a la funcion :D
            if((i+1)==rr.length)
            {
                int t7=this.getTemporal();int t8=this.getTemporal();
                int t9=this.getTemporal();
                this.OSX_print3D("t"+t7+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t8+" = t"+t7+" + 0;\t\t//this funcion a llamar");
                this.OSX_print3D("t"+t9+" = t"+tval+";\t\t//valor this a pasar");
                this.OSX_print3D("stack[t"+t8+"] = t"+t9+";\t//pasar this a funcion");                
                this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
                
                FuncionO fsol = ts.getFuncion_NPARAMETROS(atributo.clase, rr[i], parametros);
                if(fsol==null)
                {
                    String descri = "Funcion I THIS MULTI, no existe con: "+parametros+" parametros";
                    this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                    return;
                }
                if(fsol.visibilidad.equals("private")||fsol.visibilidad.equals("protected"))
                {
                    String descri = "Funcion no visible (Private, Protected)";
                    this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                }
                this.OSX_print3D("\tCLASE_"+fsol.clase+"_FUNCION_"+rr[i]+"_PARAMS_"+fsol.cadenap1+"();//call");
                this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
                return;
            }
            else
            {                
                //aun estoy en un atributo                
                tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
                tpos = this.getTemporal();            
                this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
    }
    
    public void OSX_llamada_i_VSMULTI(String laclase, String lafuncion, String cadenaf, int eltamanio, int parametros, String elregistro)
    {
       System.out.println("! ============================================================ Izquierda, llamada Var Multi");
        System.out.println("Clase: "+laclase);
        System.out.println("Funcion actual: "+lafuncion);
        System.out.println("Tamanio actual: "+eltamanio);
        System.out.println("Cadena Parametros Actual: "+cadenaf);
        System.out.println("Parametros pasados: "+parametros);
        System.out.println("Registro: "+elregistro);
        String sr = elregistro;
        if(sr.contains(".")==false)
        {
            //es llamada simple:
            int t1 =this.getTemporal();int t2 =this.getTemporal();
            int t3 =this.getTemporal();int t4 =this.getTemporal();
            this.OSX_print3D("t"+t1+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
            this.OSX_print3D("t"+t2+" = t"+t1+" + 0;\t\t//Pos this, funcion a llamar");
            this.OSX_print3D("t"+t3+" = p + 0;\t\t//Pos this, funcion actual");
            this.OSX_print3D("t"+t4+" = stack[t"+t3+"];\t//Dir this, funcion actual");
            this.OSX_print3D("stack[t"+t2+"] = t"+t4+";\t//Pasar this actual a this a llamar");
            this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
            FuncionO fsol = ts.getFuncion_NPARAMETROS(laclase, sr, parametros);
            if(fsol==null)
            {
                    String descri = "Funcion I VAR MULTI, no existe con: "+parametros+" parametros";
                    this.guardarError(sr, superpadre.linea, superpadre.columna, "Semantico", descri);
                    return;
            }
            //this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"();//call");
            this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"_PARAMS_"+fsol.cadenap1+"();//call");
            this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
            return;
        }
        //String rr[]=sr.replace(".", ":").split(":");
        String rr[]=sr.split("\\.");
        int tamaniopos = this.ts.getPosPVAR(laclase, lafuncion,cadenaf, rr[0], superpadre);
        int tpos = this.getTemporal();
        int tval = this.getTemporal();        
        this.OSX_print3D("t"+tpos+" = p + "+tamaniopos+";\t\t//pos relativa variable");
        this.OSX_print3D("t"+tval+" = stack[t"+tpos+"];\t//variable "+rr[0]);
        
        for (int i = 1; i < rr.length; i++) 
        {
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            //Se llego a la funcion :D
            if((i+1)==rr.length)
            {
                int t7=this.getTemporal();int t8=this.getTemporal();
                int t9=this.getTemporal();
                this.OSX_print3D("t"+t7+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t8+" = t"+t7+" + 0;\t\t//this funcion a llamar");
                this.OSX_print3D("t"+t9+" = t"+tval+";\t\t//valor this a pasar");
                this.OSX_print3D("stack[t"+t8+"] = t"+t9+";\t//pasar this a funcion");                
                this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
                
                FuncionO fsol = ts.getFuncion_NPARAMETROS(atributo.clase, rr[i], parametros);
                if(fsol==null)
                {
                    String descri = "Funcion I VAR MULTI, no existe con: "+parametros+" parametros";
                    this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                    return;
                }
                if(fsol.visibilidad.equals("private")||fsol.visibilidad.equals("protected"))
                {
                    String descri = "Funcion no visible (Private, Protected)";
                    this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                }
                this.OSX_print3D("\tCLASE_"+fsol.clase+"_FUNCION_"+rr[i]+"_PARAMS_"+fsol.cadenap1+"();//call");
                this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
                return;
            }
            else
            {                
                //aun estoy en un atributo                
                tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
                tpos = this.getTemporal();
                this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
    }
    
    public int OSX_llamada_d_TSMULTI(String laclase, String lafuncion, int eltamanio, int parametros, String elregistro)
    {
        System.out.println("! ============================================================ Derecha, llamada This Multi");
        System.out.println("Clase: "+laclase);
        System.out.println("Funcion actual: "+lafuncion);
        System.out.println("Tamanio actual: "+eltamanio);
        System.out.println("Registro: "+elregistro);
        String sr = elregistro.replace("[:THIS:].", "").trim();                
        if(sr.contains(".")==false)
        {
            //es llamada simple:
            int t1 =this.getTemporal();int t2 =this.getTemporal();
            int t3 =this.getTemporal();int t4 =this.getTemporal();
            int t5 =this.getTemporal();int t6 =this.getTemporal();
            this.OSX_print3D("t"+t1+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
            this.OSX_print3D("t"+t2+" = t"+t1+" + 0;\t\t//Pos this, funcion a llamar");
            this.OSX_print3D("t"+t3+" = p + 0;\t\t//Pos this, funcion actual");
            this.OSX_print3D("t"+t4+" = stack[t"+t3+"];\t//Dir this, funcion actual");
            this.OSX_print3D("stack[t"+t2+"] = t"+t4+";\t//Pasar this actual a this a llamar");
            this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
            FuncionO fsol = ts.getFuncion_NPARAMETROS(laclase, sr, parametros);
            if(fsol==null)
            {
                    String descri = "Funcion I THIS MULTI, no existe con: "+parametros+" parametros";
                    this.guardarError(sr, superpadre.linea, superpadre.columna, "Semantico", descri);
                    return 2500;
            }
            //this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"();//call");
            this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"_PARAMS_"+fsol.cadenap1+"();//call");
            this.OSX_print3D("t"+t5+" = p + 1;\t\t//Pos retorno, funcion llamada");
            this.OSX_print3D("t"+t6+" = stack[t"+t5+"];\t//Obtener el retorno");
            this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
            this.osx_auxtipo = ts.OSX_auxtipo;
            return t6;
        }
        //String rr[]=sr.split(".");
        String rr[]=sr.split("\\.");
        int tthis = this.getTemporal();
        int tdir = this.getTemporal();
        int tpos = this.getTemporal();
        int tval = this.getTemporal();
        int tamaniopos = this.ts.getPosAtributo(laclase, rr[0], superpadre);
        this.OSX_print3D("t"+tthis+" = p + 0;\t\t//this objeto actual");
        this.OSX_print3D("t"+tdir+" = stack[t"+tthis+"];\t//dir objeto actual en heap");
        this.OSX_print3D("t"+tpos+" = t"+tdir+" + "+tamaniopos+";\t\t//pos relativa atributo");
        this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[0]);
        
        for (int i = 1; i < rr.length; i++) 
        {
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            //Se llego a la funcion :D
            if((i+1)==rr.length)
            {
                int t11=this.getTemporal();int t12=this.getTemporal();
                int t13=this.getTemporal();
                int t17=this.getTemporal();int t18=this.getTemporal();
                this.OSX_print3D("t"+t11+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t12+" = t"+t11+" + 0;\t\t//this funcion a llamar");
                this.OSX_print3D("t"+t13+" = t"+tval+";\t\t//valor this a pasar");
                this.OSX_print3D("stack[t"+t12+"] = t"+t13+";\t//pasar this a funcion");                
                this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
                FuncionO fsol = ts.getFuncion_NPARAMETROS(atributo.clase, rr[i], parametros);
                if(fsol==null)
                {
                        String descri = "Funcion I THIS MULTI, no existe con: "+parametros+" parametros";
                        this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                        return 2500;
                }
                if(fsol.visibilidad.equals("private")||fsol.visibilidad.equals("protected"))
                {
                        String descri = "Funcion no visible (Private, Protected)";
                        this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                }
                //this.OSX_print3D("\tCLASE_"+atributo.clase+"_FUNCION_"+rr[i]+"();//call");
                this.OSX_print3D("\tCLASE_"+fsol.clase+"_FUNCION_"+rr[i]+"_PARAMS_"+fsol.cadenap1+"();//call");
                this.OSX_print3D("t"+t17+" = p + 1;\t\t//Pos retorno, funcion llamada");
                this.OSX_print3D("t"+t18+" = stack[t"+t17+"];\t//Obtener el retorno");
                this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
                this.osx_auxtipo = ts.OSX_auxtipo;
                return t18;                
            }
            else
            {                
                //aun estoy en un atributo                
                tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
                tpos = this.getTemporal();            
                this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
        return 2500;
    }
    
    public int OSX_llamada_d_VSMULTI(String laclase, String lafuncion, String cadenaf, int eltamanio, int parametros, String elregistro)
    {
        System.out.println("! ============================================================ Derecha, llamada Var Multi");
        System.out.println("Clase: "+laclase);
        System.out.println("Funcion actual: "+lafuncion);
        System.out.println("Tamanio actual: "+eltamanio);
        System.out.println("Cadena Parametros Actual: "+cadenaf);
        System.out.println("Registro: "+elregistro);
        String sr = elregistro;             
        if(sr.contains(".")==false)
        {
            //es llamada simple:
            int t1 =this.getTemporal();int t2 =this.getTemporal();
            int t3 =this.getTemporal();int t4 =this.getTemporal();
            int t5 =this.getTemporal();int t6 =this.getTemporal();
            this.OSX_print3D("t"+t1+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
            this.OSX_print3D("t"+t2+" = t"+t1+" + 0;\t\t//Pos this, funcion a llamar");
            this.OSX_print3D("t"+t3+" = p + 0;\t\t//Pos this, funcion actual");
            this.OSX_print3D("t"+t4+" = stack[t"+t3+"];\t//Dir this, funcion actual");
            this.OSX_print3D("stack[t"+t2+"] = t"+t4+";\t//Pasar this actual a this a llamar");
            this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");
            FuncionO fsol = ts.getFuncion_NPARAMETROS(laclase, sr, parametros);
            if(fsol==null)
            {
                String descri = "Funcion I VAR MULTI, no existe con: "+parametros+" parametros";
                this.guardarError(sr, superpadre.linea, superpadre.columna, "Semantico", descri);
                return 3500;
            }
            //this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"();//call");
            this.OSX_print3D("\tCLASE_"+laclase+"_FUNCION_"+sr+"_PARAMS_"+fsol.cadenap1+"();//call");
            this.OSX_print3D("t"+t5+" = p + 1;\t\t//Pos retorno, funcion llamada");
            this.OSX_print3D("t"+t6+" = stack[t"+t5+"];\t//Obtener el retorno");
            this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
            this.osx_auxtipo=ts.OSX_auxtipo;
            return t6;
        }
        String rr[]=sr.split("\\.");
        int tpos = this.getTemporal();
        int tval = this.getTemporal();
        int tamaniopos = this.ts.getPosPVAR(laclase, lafuncion, cadenaf, rr[0], superpadre);
        this.OSX_print3D("t"+tpos+" = p + "+tamaniopos+";\t\t//pos relativa variable");
        this.OSX_print3D("t"+tval+" = stack[t"+tpos+"];\t//variable "+rr[0]);
        
        for (int i = 1; i < rr.length; i++) 
        {
            if(Compilador.listaErrores.size()>0) 
            {break;}
            
            //Anterior atributo localizado (Atributo desde donde quiero acceder a rr[i] atributo
            VariableO atributo = ts.OSX_variable;
            if(atributo.visi=="private")
            {this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", "Atributo no visible (Private)");}
            
            //Se llego a la funcion :D
            if((i+1)==rr.length)
            {
                int t11=this.getTemporal();int t12=this.getTemporal();
                int t13=this.getTemporal();
                int t17=this.getTemporal();int t18=this.getTemporal();
                this.OSX_print3D("t"+t11+" = p + "+eltamanio+";\t\t//Simular cambio de ambito");
                this.OSX_print3D("t"+t12+" = t"+t11+" + 0;\t\t//this funcion a llamar");
                this.OSX_print3D("t"+t13+" = t"+tval+";\t\t//valor this a pasar");
                this.OSX_print3D("stack[t"+t12+"] = t"+t13+";\t//pasar this a funcion");                
                this.OSX_print3D("p = p + "+eltamanio+";\t\t//Cambiar de ambito");                
                FuncionO fsol = ts.getFuncion_NPARAMETROS(atributo.clase, rr[i], parametros);
                if(fsol==null)
                {
                        String descri = "Funcion I VAR MULTI, no existe con: "+parametros+" parametros";
                        this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                        return 3500;
                }
                if(fsol.visibilidad.equals("private")||fsol.visibilidad.equals("protected"))
                {
                        String descri = "Funcion no visible (Private, Protected)";
                        this.guardarError(rr[i], superpadre.linea, superpadre.columna, "Semantico", descri);
                }
                this.OSX_print3D("\tCLASE_"+fsol.clase+"_FUNCION_"+rr[i]+"_PARAMS_"+fsol.cadenap1+"();//call");
                //this.OSX_print3D("\tCLASE_"+atributo.clase+"_FUNCION_"+rr[i]+"();//call");
                this.OSX_print3D("t"+t17+" = p + 1;\t\t//Pos retorno, funcion llamada");
                this.OSX_print3D("t"+t18+" = stack[t"+t17+"];\t//Obtener el retorno");
                this.OSX_print3D("p = p - "+eltamanio+";\t\t//Cambiar de ambito");
                this.osx_auxtipo=ts.OSX_auxtipo;
                return t18;
            }
            else
            {                
                //aun estoy en un atributo                
                //tamaniopos = this.ts.getPosPVAR(laclase, lafuncion, "", rr[i], superpadre);
                tamaniopos = this.ts.getPosAtributo(atributo.clase, rr[i], superpadre);
                tpos = this.getTemporal();            
                this.OSX_print3D("t"+tpos+" = t"+tval+" + "+tamaniopos+";\t\t//pos relativa atributo");
                tval = this.getTemporal();
                this.OSX_print3D("t"+tval+" = heap[t"+tpos+"];\t//atributo "+rr[i]);
            }
        }
        return 1000;
    }
    
    
}
