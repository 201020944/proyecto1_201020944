/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

/**
 *
 * @author LuisAroldo
 */
public class Simbolo {
    public Boolean es_terminal;
    public Nodo accion;
    public String name;
    public Nodo accionprime;

    public Simbolo() {}
    
    public Simbolo(String pname){
        this.name=pname.toLowerCase();
        this.es_terminal=false;      
    }
}
