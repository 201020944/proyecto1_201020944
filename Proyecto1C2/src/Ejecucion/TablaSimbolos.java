/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejecucion;

import LSR.NodoS;
import LSR.TAS2;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class TablaSimbolos {
    
    public ArrayList<TSimbolo> tabla_simbolos;
    public ArrayList<ClaseO> lista_clases;
    public ClaseO clase;
    public FuncionO funcion;
    public FuncionO cons;
    public FuncionO elmain;
    public Boolean mainfull;
    
    public TablaSimbolos(){
        this.tabla_simbolos= new ArrayList<TSimbolo>();
        this.lista_clases=new ArrayList<ClaseO>();
        this.mainfull=false;
//        this.salida3D=psalida3D;
    }        
    
    private void guardarError(String t, int l, int c, String ti, String d){
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        Compilador.listaErrores.add(new Errores(t,l,c,ti,d));
    }
    
    public void startClase(String nombre_clase, Nodo nodo){        
        System.out.println("! ============================================================ Start Clase");
        System.out.println("Clase: "+nombre_clase);        
        this.clase = new ClaseO(nombre_clase);
        System.out.println("\n");
    }
    
    public void saveClase(){
        System.out.println("! ============================================================ Save Clase");
        System.out.println("Clase: "+this.clase.nombre);
        
        //XXXXXXX validar q no exista unaclase con el mismo nombre    
        
        this.clase.updateTamanio();
        this.lista_clases.add(clase);
        this.clase=null;
        System.out.println("\n");
    }
    
    public void startMain(String pclase, Nodo nodo){
        System.out.println("! ============================================================ Start Main");
        System.out.println("Clase: "+pclase);
        
        if(mainfull==false){
            System.out.println("Main: main");
            this.elmain=new FuncionO(pclase,"main","main","main","public");
            System.out.println("\n");
        }else{
            this.guardarError("main", nodo.linea, nodo.columna, "Semantico", "main ya existe");
        }        
    }
    
    public void saveMain(){
        System.out.println("! ============================================================ Save Main");
        System.out.println("Clase: "+this.clase.nombre);
        
        //XXXXXXX validar q ya se habia llamado un main
        
        this.elmain.updateTamanio();
        this.mainfull=true;
        this.clase.maines.add(elmain);        
        System.out.println("\n");
    }
        
    public void startConstructor(String sclase, String sconstructor){
        System.out.println("! ============================================================ Start Constructor");
        System.out.println("Constructor: "+sconstructor);
        System.out.println("Clase: "+sclase );
        //(String sclase, String sfuncion, String rol, String stipo, String svisi)
        this.cons = new FuncionO(sclase,sconstructor,"Constructor",sclase,"public");
        System.out.println("\n");
    }
        
    public void saveConstructor(){
        System.out.println("! ============================================================ Save Constructor");
        System.out.println("Constructor: "+this.cons.nombre);
        System.out.println("Clase: "+this.clase.nombre);
        
        //XXXXXX seria de validar si aqui hay una funcion con el mismo: nombre y cadena de parametros
        
        this.cons.updateTamanio();
        this.clase.constructores.add(cons);
        this.cons=null;
        System.out.println("\n");
    }
    
    public void startFuncion(String sclase, String sfuncio, String stipo, String svisi, Nodo nodo){
        System.out.println("! ============================================================ Start Funcion");
        System.out.println("Funcion: "+sfuncio);
        System.out.println("Clase: "+sclase);        
        System.out.println("Tipo: "+stipo);
        System.out.println("Visibilidad: "+svisi);
        this.funcion=new FuncionO(sclase, sfuncio, "Funcion", stipo, svisi);
        System.out.println("\n");
    }
    
    public void saveFuncion(){
        System.out.println("! ============================================================ Save Funcion");
        System.out.println("Funcion: "+this.funcion.nombre);
        System.out.println("Clase: "+this.clase.nombre);
        
        //XXXXXX seria de validar si aqui hay una funcion con el mismo: nombre y cadena de parametros
        
        this.funcion.updateTamanio();        
        this.clase.funciones.add(funcion);
        this.funcion=null;
        System.out.println("\n");
    }
    
    public void saveAtributo(String sclase, String satrib,String stipo,String svisi, Nodo nodo){
        //atrib puede traer una lista de ids separados por #        
        if(satrib.contains("#")==true){
            String[] lista_de_ids = satrib.split("#");
            for(String id_tempo:lista_de_ids){
                System.out.println("! ============================================================ Save Atributo");
                System.out.println("Atributo : "+id_tempo); System.out.println("Clase: "+sclase);
                System.out.println("Tipo: "+stipo);         System.out.println("Visibilidad: "+svisi);
                System.out.println("Tamanio: "+1);
                
                int posi_relativa_h = this.clase.atributos.size();
                System.out.println("Posicion: h + "+posi_relativa_h);
                VariableO var = new VariableO();
                var.toAtributo(sclase, id_tempo, stipo, svisi, posi_relativa_h);
                this.clase.atributos.add(var);
                System.out.println("\n");
            }                
        }else{
            System.out.println("! ============================================================ Save Atributo");
            System.out.println("Atributo : "+satrib);   System.out.println("Clase: "+sclase);
            System.out.println("Tipo: "+stipo);         System.out.println("Visibilidad: "+svisi);
            System.out.println("Tamanio: "+1);
            
            int posi_relativa_h = this.clase.atributos.size();
            System.out.println("Posicion: h + "+posi_relativa_h);
            VariableO var = new VariableO();
            var.toAtributo(sclase, satrib, stipo, svisi, posi_relativa_h);
            this.clase.atributos.add(var);
            System.out.println("\n");
        }                
    }
    
    public void saveParametro(String sclase, String sfuncio, String sparam, String stipo, Nodo nodo){       
        //(String pclase, String pnombre, String pambito1, String ptipo,int ppos)
        System.out.println("! ============================================================ Save Parametro");
        System.out.println("Parametro : "+sparam);
        System.out.println("Clase: "+sclase);
            
        if(this.funcion!=null)
        {        
            System.out.println("Funcion: "+sfuncio);
            System.out.println("Tipo: "+stipo);
            System.out.println("Tamanio: "+1);        
            int posi_relativa_p = this.funcion.parametros.size()+2;
            System.out.println("Posicion: p + "+posi_relativa_p);
            
            VariableO param = new VariableO();
            param.toParametro(sclase, sfuncio, sparam, stipo, posi_relativa_p);            
            this.funcion.parametros.add(param);
        }
        else if(this.cons!=null)
        {            
            System.out.println("Costructor: "+sfuncio);
            System.out.println("Tipo: "+stipo);
            System.out.println("Tamanio: "+1);
            int posi_relativa_p = this.cons.parametros.size()+2;
            System.out.println("Posicion: p + "+posi_relativa_p);
            
            VariableO param = new VariableO();
            param.toParametro(sclase, sfuncio, sparam, stipo, posi_relativa_p);
            this.cons.parametros.add(param);
        }
        else
        {
            System.out.println("XXXXXXX ERROR, EN OSX_saveParametro");
            return;
        }                
        System.out.println("\n");
    }
    
    public void setCadenaParametros(String pcadena)
    {        
        System.out.println("! ============================================================ Set Cadena Parametros");
        System.out.println("Cadena: "+pcadena);
        if(this.funcion!=null)
        {
            System.out.println("Funcion: "+this.funcion.nombre);
            this.funcion.setCadenasP(pcadena);
        }
        else if(this.cons!=null)
        {
            System.out.println("Constructor: "+this.cons.nombre);
            this.cons.setCadenasP(pcadena);
        }
        else if(this.elmain!=null)
        {
            this.elmain.setCadenasP(pcadena);
        }
        else
        {
            System.out.println("XXXXXXX ERROR, EN OSX_setCadenaParametros");
            return;
        }                
        System.out.println("\n");
        
    }
    
    public void saveVariable(String pclase, String pfuncion, String pvar, String ptipo, Nodo nodo)
    {
        System.out.println("! ============================================================ Save Variable");
        //atrib puede traer una lista de ids separados por #        
        if(pvar.contains("#")==true)
        {
            String[] lista_de_ids = pvar.split("#");
            for(String id_tempo:lista_de_ids)
            {
                System.out.println("Variable : "+id_tempo); 
                System.out.println("Clase: "+pclase);
                                                                
                if(this.funcion!=null)
                {
                    System.out.println("Funcion: "+this.funcion.nombre);
                    System.out.println("Tipo: "+ptipo);
                    System.out.println("Tamanio: "+1);                    
                    int posi_relativa_p = this.funcion.parametros.size()+this.funcion.variables.size()+2;
                    System.out.println("Posicion: p + "+posi_relativa_p);
                    VariableO var = new VariableO();
                    var.toVariable(pclase, pfuncion, id_tempo, ptipo, posi_relativa_p);
                    this.funcion.variables.add(var);
                }
                else if(this.cons!=null)
                {
                    System.out.println("Constructor: "+this.cons.nombre);
                    System.out.println("Tipo: "+ptipo);
                    System.out.println("Tamanio: "+1);                    
                    int posi_relativa_p = this.cons.parametros.size()+this.cons.variables.size()+2;
                    System.out.println("Posicion: p + "+posi_relativa_p);
                    VariableO var = new VariableO();
                    var.toVariable(pclase, pfuncion, id_tempo, ptipo, posi_relativa_p);
                    this.cons.variables.add(var);
                }
                else if(this.elmain!=null)
                {
                    System.out.println("Main: main");
                    System.out.println("Tipo: "+ptipo);
                    System.out.println("Tamanio: "+1);                    
                    int posi_relativa_p = this.elmain.variables.size()+2;
                    System.out.println("Posicion: p + "+posi_relativa_p);
                    VariableO var = new VariableO();
                    var.toVariable(pclase, pfuncion, id_tempo, ptipo, posi_relativa_p);
                    this.elmain.variables.add(var);
                }
                else
                {
                    System.out.println("XXXXXXX ERROR, EN OSX_saveVariable lista de ids");
                    return;
                } 
            }
        }
        else
        {
            System.out.println("Variable : "+pvar);
            System.out.println("Clase: "+pclase);
            if(this.funcion!=null)
            {
                System.out.println("Funcion: "+this.funcion.nombre);
                System.out.println("Tipo: "+ptipo);
                System.out.println("Tamanio: "+1);                    
                int posi_relativa_p = this.funcion.parametros.size()+this.funcion.variables.size()+2;
                System.out.println("Posicion: p + "+posi_relativa_p);
                VariableO var = new VariableO();
                var.toVariable(pclase, pfuncion,pvar, ptipo, posi_relativa_p);
                this.funcion.variables.add(var);
            }
            else if(this.cons!=null)
            {
                System.out.println("Constructor: "+this.cons.nombre);
                System.out.println("Tipo: "+ptipo);
                System.out.println("Tamanio: "+1);                    
                int posi_relativa_p = this.cons.parametros.size()+this.cons.variables.size()+2;
                System.out.println("Posicion: p + "+posi_relativa_p);
                VariableO var = new VariableO();
                var.toVariable(pclase, pfuncion, pvar, ptipo, posi_relativa_p);
                this.cons.variables.add(var);
            }
            else if(this.elmain!=null)
            {
                System.out.println("Main: main");
                System.out.println("Tipo: "+ptipo);
                System.out.println("Tamanio: "+1);                    
                int posi_relativa_p = this.elmain.variables.size()+2;
                System.out.println("Posicion: p + "+posi_relativa_p);
                VariableO var = new VariableO();
                var.toVariable(pclase, pfuncion, pvar, ptipo, posi_relativa_p);
                this.elmain.variables.add(var);
            }
            else
            {
                System.out.println("XXXXXXX ERROR, EN OSX_saveVariable normal");
                return;
            }
        }
        
        System.out.println("\n");
    }
    
    ////Crea un archivo .csv con la tabla de simbolos
    public void soutTablaCSV()
    {
        System.out.println("! ============================================================ Tabla de Simbolos");
        
        StringBuilder csvBuilder = new StringBuilder();
        //nombre, tipo, rol, ambito, tmanio, posicion
        for(ClaseO claset:this.lista_clases)
        {
            csvBuilder.append("Nombre, Tipo, Rol, Ambito, Tamanio, Posicion\n");
            csvBuilder.append(claset.getDatosCSV());
        }
        
        if(this.elmain!=null)
        {
            csvBuilder.append(this.elmain.getDatosCSV());
        }
        
        String ruta = "C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\Tabla\\Tabla de Simbolos.csv";        
        this.creararchivo(ruta, csvBuilder.toString());
        System.out.println("\n");
    }
    
    public synchronized void creararchivo(String pfichero,String pcontenido){   
        FileWriter archivo = null;
   
        try{archivo = new FileWriter(pfichero);} 
        catch (IOException ex) 
        {Logger.getLogger(TAS2.class.getName()).log(Level.SEVERE, null, ex);}

        File a = new File(pfichero);        
        if (!a.exists()){return;}   
        
        try(PrintWriter printwriter = new PrintWriter(archivo)){
            printwriter.print(pcontenido);
            printwriter.close();
        }
    }
    
    public ClaseO getClase(String sclase)
    {                
        for (ClaseO lista_clase : this.lista_clases) {
            if (lista_clase.nombre.equals(sclase)==true) {
                return lista_clase;
            }
        }
        
        String cb=""+sclase+"";
        for(ClaseO ccc:this.lista_clases)
        {            
            if(ccc.nombre.equals(cb))
            {
                return ccc;
            }
        }
        return null;
    }    
    
    public int OSX_auxtipo=0;
    public VariableO OSX_variable;
    public int getPosAtributo(String sclase, String satributo, NodoS nodo)
    {
        //Buscar la clase
        this.OSX_auxtipo=1;
        ClaseO claseb = this.getClase(sclase);
        if(claseb==null)
        {
            this.guardarError(sclase, nodo.linea, nodo.columna, "Semantico", "GTHIS Clase no existe");
            return 100;
        }
        //VariableO atri = claseb.getAtributo(satributo);
        VariableO atris=claseb.getAtributo_de_Clase(satributo);
        if(atris==null)
        {
            this.guardarError(satributo, nodo.linea, nodo.columna, "Semantico", "GTHIS Atributo no existe en clase: "+sclase);
            return 100;
        }
        this.OSX_auxtipo=atris.itipo;
        this.OSX_variable=atris;
        return atris.posicion;
    }
        
    public Boolean esParam_Variable(String sclase, String sfuncion, String scadena, String id, NodoS nodo)
    {
        //Buscar la clase
        ClaseO laclase = this.getClase(sclase);
        if(laclase==null)
        {
            return false;
        }
        FuncionO lafuncion = laclase.getFuncion(sfuncion, scadena);
        if(lafuncion==null)
        {
            return false;
        }
        VariableO lavar = lafuncion.getParametoVariable(id);
        if(lavar==null)
        {
            return false;
        }
        return true;
    }
    
    public int getPosPVAR(String sclase, String sfuncion, String cadena_p2, String sr, NodoS nodo)
    {
        //Buscar la clase
        this.OSX_auxtipo=1;
        ClaseO claseb = this.getClase(sclase);
        if(claseb==null)
        {
            this.guardarError(sclase, nodo.linea, nodo.columna, "Semantico", "Clase: "+sclase+" no existe para acceso: "+sr);
            return 100;
        }        
        //Buscar la funcion
        FuncionO funcionb = claseb.getFuncion(sfuncion, cadena_p2);
        if(funcionb==null)
        {
            this.guardarError(sfuncion, nodo.linea, nodo.columna, "Semantico", "Funcion: "+sfuncion+" no existe para acceso: "+sr);
            return 100;
        }        
        //buscar el atributo
        //VariableO pvar = funcionb.getPVAR(sr);
        VariableO pvar = funcionb.getParametoVariable(sr);
        if(pvar==null)
        {
            this.guardarError(sr, nodo.linea, nodo.columna, "Semantico", "Variable no existe para acceso: "+sr);
            return 100;
        }
        this.OSX_auxtipo=pvar.itipo;
        this.OSX_variable=pvar;
        return pvar.posicion;
    }
        
    public int getTamanioFuncion(String sclase, String namefuncion, String cadena_p2, NodoS nodo)
    {
        this.OSX_auxtipo=1;
        if(namefuncion.replace(" ", "").equals("main")==true)
        {
            if(this.elmain!=null)
            {
                return this.elmain.tamanio;
            }
        }
        
        
        //Buscar la clase        
        ClaseO claseb = this.getClase(sclase);
        if(claseb==null)
        {
            this.guardarError(sclase, nodo.linea, nodo.columna, "Semantico", "Clase:"+sclase+" no existe para acceso");
            return 100;
        }        
        //Buscar la funcion
        FuncionO funcionb = claseb.getFuncion(namefuncion, cadena_p2);
        if(funcionb==null)
        {
            this.guardarError(namefuncion, nodo.linea, nodo.columna, "Semantico", "Funcion: "+namefuncion+" no existe para acceso");
            return 100;
        }
        return funcionb.tamanio;
    }
    
    public int getVisibilidad(String laclase, String namefu, String scadena, NodoS nodo)
    {
  
        //String laclase = elatributo.clase;
        ClaseO clt = this.getClase(laclase);
        if(clt==null)
        {
            this.guardarError(laclase, nodo.linea, nodo.columna, "Semantico", "Clase:"+laclase+" no existe para acceso getvisi");
            return 1;
        }
        
        FuncionO funcionb = clt.getFuncion(namefu,scadena);
        if(funcionb==null)
        {
            this.guardarError(namefu, nodo.linea, nodo.columna, "Semantico", "Funcion: "+namefu+" no existe para acceso getvisi");
            return 1;
        }
        switch (funcionb.visibilidad) 
        {
            case "private":     return 3;
            case "protected":   return 2;
            default:            return 1;
        }
    }
    
    public FuncionO getFuncion_NPARAMETROS(String laclase, String lafuncion, int parametros)
    {
        ClaseO ccc = this.getClase(laclase);
        if(ccc==null)
        {
            return null;
        }
        FuncionO fu = ccc.getFuncion_NPARAMETROS(lafuncion, parametros);
        if(fu==null)
        {
            return null;
        }
        return fu;
    }
    
    public String getCadenaPC(String laclase, int nparametros, NodoS nodo)
    {
        for(ClaseO cla:this.lista_clases)
        {
            //estamos en la clase solicitadad
            if(cla.nombre.equals(laclase)==true)
            {
                //Recorrer los constructores
                for(FuncionO cns:cla.constructores)
                {
                    if(cns.parametros.size()==nparametros)
                    {
                        return "_PARAMS_"+cns.cadenap1;
                    }
                }
            }
        }            
        //Paso, no encontro la clase
        String descri ="getCadenaPC clase, constructor no existe: "+laclase;
        this.guardarError(laclase, nodo.linea, nodo.columna, "Semantico", descri);
        return "";
    }
    
    
}
