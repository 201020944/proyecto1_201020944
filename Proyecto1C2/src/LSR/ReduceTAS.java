/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

/**
 *
 * @author LuisAroldo
 */
public class ReduceTAS {
    
    public int numero_estado_fuente;
    public int numero_reduccion;
    public String simbolo_transicion;
    
    public ReduceTAS(int ef, int nr, String s){
        this.numero_estado_fuente=ef;
        this.numero_reduccion=nr;
        this.simbolo_transicion=s.toLowerCase();
    }
    
}
