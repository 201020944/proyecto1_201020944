package LSR;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Joaquin
 */
public class ReporteTAS extends javax.swing.JFrame 
{

    public ReporteTAS() {
        initComponents();
    }

    public void setTabla(DefaultTableModel dtm)
    {
        this.jTable1.setModel(dtm);        
        
        int ancho = 800/dtm.getColumnCount();
        int ancho2 = ancho*dtm.getColumnCount();
                        
        if(jTable1.getRowCount()<18)
        {
            int alto2 = 25*dtm.getRowCount();
            jTable1.setRowHeight(25);
            this.setSize(ancho2, alto2+66);
        }
        else
        {
            this.setSize(ancho2, 450);
        }                
    }
    
    public void setTabla2(DefaultTableModel dtm, int fila, int columna)
    {

            //this.tas.setModel(dtm);
            //fila 2, columna 1
            
            jTable1.setModel(dtm);
            FormatoTabla ft = new FormatoTabla(fila,columna);
            //jTable1.setDefaultRenderer (Class.forName("java.lang.String"), ft);
            jTable1.setDefaultRenderer (Object.class, ft);
//            jTable1.getColumnModel().getColumn(0).setMaxWidth(0);
//            jTable1.getColumnModel().getColumn(0).setMinWidth(0);
//            jTable1.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
//            jTable1.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
            
//        // tabla.setDefaultRenderer (Object.class, new MiRender());
//        //tas.setDefaultRenderer(Object.class, new FormatoTabla());
        
        int ancho = 800/dtm.getColumnCount();
        int ancho2 = ancho*dtm.getColumnCount();
                        
        if(jTable1.getRowCount()<18)
        {
            int alto2 = 25*dtm.getRowCount();
            jTable1.setRowHeight(25);
            this.setSize(ancho2, alto2+83);
        }
        else
        {
            this.setSize(ancho2, 450);
        }
//        this.jTable1.repaint();
//        this.repaint();        
        JOptionPane.showMessageDialog(null,"Accion","FiSintax",1);
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tabla de Analisis Sintactico");

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.setEnabled(false);
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
