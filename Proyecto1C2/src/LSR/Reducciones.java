/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

import java.util.HashMap;

/**
 *
 * @author LuisAroldo
 */
public class Reducciones {
    
    public HashMap<String, HashMap<Integer, Integer>> listaReducciones;
    
    
    public Reducciones(){
        this.listaReducciones=new HashMap<String, HashMap<Integer, Integer>>();
    }
        
    public void guardarReduce(String ssimbolo, Integer pid){                
        if(this.listaReducciones.containsKey(ssimbolo)==true){
            HashMap<Integer, Integer> nums_reducciones = this.listaReducciones.get(ssimbolo);
            if(nums_reducciones.containsKey(pid)==false){
                nums_reducciones.put(pid, pid);
            }
        }else{
            HashMap<Integer, Integer> nums_reducciones = new HashMap<Integer, Integer>();
            nums_reducciones.put(pid, pid);
            this.listaReducciones.put(ssimbolo, nums_reducciones);
        }              
    }
    
}
