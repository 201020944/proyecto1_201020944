/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

import Ejecucion.NoTerminal;
import Ejecucion.Simbolo;
import Ejecucion.Terminal;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LuisAroldo
 */
public class TAS2 {
    
    private HashMap<String, Terminal> listaTerminales;
    private HashMap<String, NoTerminal> listaNterminales; 
    private HashMap<Integer, Estado> listaEstados;
    
    //Para la tabla en JTable
    private CeldaTAS [][]tabla;
    private Object [][]data;
    private String[] datac;
    public DefaultTableModel dtm;
    
    //Para construir la Tabla de Analisis Sintactico
    private HashMap<String, Integer> simbolos;
    public HashMap<Integer, EstadoTAS> estadosTAS;
    
    public TAS2(HashMap<String, Terminal> lt, HashMap<String, NoTerminal> lnt, HashMap<Integer, Estado> le){
        this.listaTerminales=lt;
        this.listaNterminales=lnt;
        this.listaEstados=le;
    }
    
    
    public void crearTabla3() {
        //Instanciado para la JTable
        data = new Object[listaEstados.size()][this.listaTerminales.size()+this.listaNterminales.size()+1];
        datac = new String[this.listaTerminales.size()+this.listaNterminales.size()+1];
        tabla = new CeldaTAS[listaEstados.size()][this.listaTerminales.size()+this.listaNterminales.size()+1];
        datac[0]="E/S";
		
        //Simbolo y el numero que le toca
        simbolos = new HashMap<String, Integer> ();
        
        //Para TAS
        this.estadosTAS= new HashMap<Integer, EstadoTAS>();
        
        //Estas variables se usan para todos(Tabla, JTable)
        Boolean llenado = false;
        int fila_cantidad_estados=0;
        int columna_simbolos=0;
        
        for(Estado estado:this.listaEstados.values()){
            //Creo un estado nuevo
            EstadoTAS estado_nuevo = new EstadoTAS(estado.id);
            
            columna_simbolos=0;
            for (Map.Entry item2:this.listaTerminales.entrySet()){
                //Creando las Celda
                String name_terminal = (String)item2.getKey();
                name_terminal = name_terminal.toLowerCase();                
                CeldaTAS celda=new CeldaTAS(name_terminal);
                celda.columna=columna_simbolos+1;
                
                //Para TAS
                estado_nuevo.celdas.put(name_terminal, celda);                
                
                //Para JTable
                tabla[fila_cantidad_estados][columna_simbolos]=celda;                
                
                columna_simbolos++;
                if(llenado==false){
                    datac[columna_simbolos]=name_terminal;
                    this.simbolos.put(name_terminal, columna_simbolos-1);
                }
            }
            for(Map.Entry item2:this.listaNterminales.entrySet()){
                String name_noterminal = (String) item2.getKey();
                name_noterminal = name_noterminal.toLowerCase();                
                CeldaTAS celda=new CeldaTAS(name_noterminal);
                celda.columna=columna_simbolos+1;
                                
                estado_nuevo.celdas.put(name_noterminal, celda);                
                
                tabla[fila_cantidad_estados][columna_simbolos]=celda;   
                
                columna_simbolos++;
                if(llenado==false){
                    datac[columna_simbolos]="<"+name_noterminal+">";
                    this.simbolos.put(name_noterminal, columna_simbolos-1);
                }
            }
            this.estadosTAS.put(estado_nuevo.id, estado_nuevo);
            fila_cantidad_estados++;
            llenado=true;
        }
        System.out.println("\n\n! ============================	Cuerpo TAS2\n");
    }
    
    
    public void pasarDesplazamientos(){
        for(Estado estado: this.listaEstados.values()){
            int iproximo=estado.id;
            HashMap<Integer, Simbolo> listaDesplazamientos=estado.desplazamientos.listaDesplazamientos;
                        
            if(listaDesplazamientos==null){continue;}
            
            for (Map.Entry item2:listaDesplazamientos.entrySet()){
                int iestado_fuente = (Integer)item2.getKey();
                Simbolo simbol = (Simbolo)item2.getValue();
                
                if(this.simbolos.containsKey(simbol.name.toLowerCase())==false){
                    continue;
                }
                
                //Obtener la celda del estado fuente con el nombre del simbolo
                EstadoTAS estado_fuente = this.estadosTAS.get(iestado_fuente);                
                CeldaTAS celda = estado_fuente.celdas.get(simbol.name.toLowerCase());
                celda.guardarShiftTAS(new ShiftTAS(iestado_fuente, iproximo, simbol.name));
            }            
        }
    }
    
    
    public void pasarReducciones(){
        for(Estado estado: this.listaEstados.values()){
            int iestado_fuente = estado.id;
            HashMap<String, HashMap<Integer, Integer>> listaReducciones = estado.reducciones.listaReducciones;
                        
            if(listaReducciones==null){continue;}
            
            for (Map.Entry item2:listaReducciones.entrySet()){
                String simbol = item2.getKey().toString().toLowerCase();
                if(this.simbolos.containsKey(simbol.toLowerCase())==false){
                    continue;
                }
                
                //Obtener la celda del estado fuente con el nombre del simbolo
                EstadoTAS estado_fuente = this.estadosTAS.get(iestado_fuente);
                CeldaTAS celda = estado_fuente.celdas.get(simbol.toLowerCase());
                
                HashMap<Integer, Integer> listaireducciones = (HashMap<Integer, Integer>)item2.getValue();
                
                if(listaireducciones==null){continue;}
                for(Integer ireduccion:listaireducciones.values()){
                    celda.guardarReduceTAS(new ReduceTAS(iestado_fuente, ireduccion, simbol));
                }
            }
        }
    }
    
    
    public void soutTabla_CSV(){
        String salida="";
        for (int i = 0; i < datac.length; i++){            
            if(i>0){
                salida+=","+datac[i];
                continue;                
            }
            salida+=datac[i];
        }
        salida+="\n";
        
        for (int i = 0; i < listaEstados.size(); i++){
            salida+=i;
            for (int j = 0; j < this.listaTerminales.size()+this.listaNterminales.size(); j++){
                CeldaTAS celda = tabla[i][j];
                salida+=","+celda.getScontenido();
                
                data[i][j+1]=celda.getScontenido();
                data[i][0]=String.valueOf(i);
            }
            if(i+1!=listaEstados.size()){
                salida+="\n";
            }            
        }        
        
        //Creo el archivo CSV
        String sepa=System.getProperty("file.separator");
        String ruta = System.getProperty("user.home") + sepa +"Desktop"+sepa+"CMP2"+sepa+"Tablas"+sepa+"SLRv1.csv";
        creararchivo(ruta, salida);
        
        //Creo el model para el JTable
        dtm= new DefaultTableModel(data,datac); 
        
        JOptionPane.showMessageDialog(null, "Tabla de Analisis Sintactico creada","FiSintax",1);
    }
    
    public synchronized void creararchivo(String pfichero,String pcontenido){   
        FileWriter archivo = null;
   
        try{archivo = new FileWriter(pfichero);} 
        catch (IOException ex) 
        {Logger.getLogger(TAS2.class.getName()).log(Level.SEVERE, null, ex);}

        File a = new File(pfichero);        
        if (!a.exists()){return;}   
        
        try(PrintWriter printwriter = new PrintWriter(archivo)){
            printwriter.print(pcontenido);
            printwriter.close();
        }
    }
    
    public void soutTabla_CSV2(){
        StringBuilder salida = new StringBuilder();
        //String salida="";
        for (int i = 0; i < datac.length; i++){            
            if(i>0){
                salida.append(",").append(datac[i]);
                //salida+=","+datac[i];
                continue;                
            }
            salida.append(datac[i]);
        }
        salida.append("\n");
        
        for (int i = 0; i < listaEstados.size(); i++){
            salida.append(i);
            for (int j = 0; j < this.listaTerminales.size()+this.listaNterminales.size(); j++){
                CeldaTAS celda = tabla[i][j];
                salida.append(",").append(celda.getScontenido());                        
                data[i][j+1]=celda.getScontenido();
                data[i][0]=String.valueOf(i);
            }
            if(i+1!=listaEstados.size()){
                salida.append("\n");
            }            
        }        
        
        //Creo el archivo CSV
        String ruta = "C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\Tabla\\TablaSLR.csv";
        creararchivo(ruta, salida.toString());
        
        //Creo el model para el JTable
        dtm= new DefaultTableModel(data,datac); 
                
        JOptionPane.showMessageDialog(null, "Tabla de Analisis Sintactico creada","FiSintax",1);
    }
    
}
