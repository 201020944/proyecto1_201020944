/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

import Ejecucion.Simbolo;
import java.util.HashMap;

/**
 *
 * @author LuisAroldo
 */
public class Desplazamientos {
    
    public HashMap<Integer, Simbolo> listaDesplazamientos;
    public HashMap<String, String> simbolosShift;
    
    public Desplazamientos(){
        this.listaDesplazamientos=new HashMap<Integer, Simbolo>();
    }
    
    public void guardarShift(int estado_origen, Simbolo simbolo_transicion){
        if(this.listaDesplazamientos.containsKey(estado_origen)==false){
            this.listaDesplazamientos.put(estado_origen, simbolo_transicion);
        }
    }
    
    public void iniciarSimbolosShift(){
        if(this.simbolosShift!=null){return;}
        this.simbolosShift=new HashMap<String, String>();
        for(Simbolo simbolo: this.listaDesplazamientos.values()){
            if(this.simbolosShift.containsKey(simbolo.name)==false){
                this.simbolosShift.put(simbolo.name, simbolo.name);
            }
        }
    } 
    
}
