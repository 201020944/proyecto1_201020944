/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

import java.util.ArrayList;

/**
 *
 * @author LuisAroldo
 */
public class CeldaTAS {
    public ArrayList<ShiftTAS> desplazas;
    public ArrayList<ReduceTAS> reduces;
    public String simboloasociado;
    public int columna;
    
    public CeldaTAS(){
        this.desplazas=new ArrayList<ShiftTAS>();
        this.reduces=new ArrayList<ReduceTAS>();
        this.columna=0;
    }
    
    public CeldaTAS(String s_simboloasociado){
        this.desplazas=new ArrayList<ShiftTAS>();
        this.reduces=new ArrayList<ReduceTAS>();
        this.simboloasociado=s_simboloasociado;
        this.columna=0;
    }
    
    public void guardarShiftTAS(ShiftTAS shift){
        this.desplazas.add(shift);
    }
    
    public void guardarReduceTAS(ReduceTAS reduce)
    {
        this.reduces.add(reduce);
    }
    
    public String getScontenido()
    {       
        //original
        String reto =this.getSdesplazas()+this.getSReduces();
//        if(reto.length()==0)
//        {
//            reto="error";
//        }
        if(reto.contains("R") || reto.contains("D"))
        {
            reto=reto.replace(" ", "");
        }
        
        return reto;
    }
    
    public String getSdesplazas()
    {
        if(this.desplazas.isEmpty())
        {
            return " ";
        }
        String reto = "";        
        Boolean paso = false;
        for(ShiftTAS d:this.desplazas)
        {            
            if(paso==false)
            {
                paso = true;
                //original
                reto+="D"+d.numero_estado_proximo;
            }
            else
            {
                //original
                reto+="_D"+d.numero_estado_proximo;
            }
        }        
        return reto;
    }
    
    public String getSReduces()
    {
        if(this.reduces.isEmpty())
        {
            return " ";
        }
        String reto="";
        
        Boolean paso=false;
        for(ReduceTAS r:this.reduces)
        {
            //reto+="  R"+r.num_reduccion
            if(paso==false)
            {
                if(this.desplazas.isEmpty())
                {
                    reto+="R"+r.numero_reduccion;
                }
                else
                {
                    reto+="_R"+r.numero_reduccion;
                }
                paso = true;
            }
            else
            {
                reto+="_R"+r.numero_reduccion;
            }            
        }
        return reto;
    }
    
}
