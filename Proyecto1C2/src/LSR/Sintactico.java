/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

import Analizador.parser;
import Analizador.scanner;
import Ejecucion.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class Sintactico {
    
     //<editor-fold desc="Atributos de la clase">
    
    /****************** Viene de la Clase Gramatica */
    //Lista de Reglas
    public HashMap<Integer, Regla> listReglas;
    
    //Para el manejo de los terminales y no terminales
    public HashMap<String, Terminal> listaTerminales;
    public HashMap<String, NoTerminal> listaNterminales;    
    
    //String nombre del terminal
    public HashMap<String, Integer> listaPrecedencia;
    
    //Para manejar el tipo de asocitividad
    public HashMap<String, Terminal> listaIzq;
    public HashMap<String, Terminal> listaDer;
    
    /****************** Viene de la Clase TAS2 */
    //Tabla de Analisis Sintacticos
    public HashMap<Integer, EstadoTAS> estadosTAS;
    public String inicial;
    
    //Para saber si llamar al Ejecutador :D
    public Boolean aceptado;
    //Necesario para el Ejecutador
    public HashMap<String,Clases> listaClases;
    public HashMap<String,Variable> listaLocal;
    
    //Para imprimir los desplazamientos y reducciones
    private javax.swing.JTextPane txtcero;
    
    //Para nodo de tokens
    Nodo nodotoken;
    
    //para guardar los tokens
    public HashMap<Integer,Token> EntradaTokens;
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Constructores">
    
    public Sintactico(){}
    
    
    //</editor-fold>
        
    public DefaultTableModel dtm;
    ReporteTAS rtas;	
    //<editor-fold desc="Analizador Sintactico">
    
    public void Parte_A_Iniciar(String entrada, DefaultTableModel dtm){
        this.aceptado=false;
        this.dtm=dtm;
        if(Compilador.tablas){
            this.rtas = new ReporteTAS();
            rtas.setTabla(this.dtm);
            rtas.setVisible(true);
        }
        System.out.println("! ============================================================ Iniciando Analisis Sintactico");
        if(this.estadosTAS==null){
            JOptionPane.showMessageDialog(null,"No hay Tabla de Analisis Sintactico","FiSintax",1);
            return;
        }
        //Llena lista de tokens
        this.nodotoken = new Nodo();
        pilaentrada = new LinkedList<Token>();
        this.AnalizarAreaTokens(entrada);
        
        
        //Llena la pila de tokens
//        System.out.println("! ============================================================ Comenzando ParteA");
//        this.Parte_B_Tokens(entrada);
//        if(pilaentrada==null){return;}
//        
//        
        System.out.println("! ============================================================ Comenzando ParteB\n\n");
        this.Parte_C_Analisis();
    }
    //</editor-fold>
    
    //<editor-fold desc="Recorrer Arbol de tokens">
    public void AnalizarAreaTokens(String entrada){
        
        Reader reader = new StringReader(entrada);
        scanner lex = new scanner(reader);
        parser par = new parser(lex);
        
        try {
            par.parse();
            if(par.raizentrada != null){
                this.graficarAst(par.raizentrada);
                this.recorridoAreaTokens(par.raizentrada);
                
                // agregar el simbolo de fin
                Token tokenfin = new Token();
                tokenfin.convertir_a_fin();
                pilaentrada.addLast(tokenfin);
                System.out.println("! ============================================================ Tokens llenados");
            }
            
        } catch (Exception ex) {
            Logger.getLogger(Sintactico.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    private void recorridoAreaTokens(Nodo nodoactual){
        switch (nodoactual.name){
             case "tokens":
                 for(Nodo tempo:nodoactual.hijos)
                {this.recorridoAreaTokens(tempo);}
                break;
             
             case "token":
                 if(nodoactual.hijos.size() == 5){
                    String nom = nodoactual.hijos.get(0).hijos.get(0).name;
                    Object val;
                    if(nodoactual.hijos.get(1).hijos.size() == 2){
                        val = nodoactual.hijos.get(1).hijos.get(1).name;
                    }else{
                        val = nodoactual.hijos.get(1).hijos.get(0).name;
                    }
                    String tip = nodoactual.hijos.get(2).hijos.get(0).name;
                    String fil = nodoactual.hijos.get(3).hijos.get(0).name;
                    String col = nodoactual.hijos.get(4).hijos.get(0).name;
                    
                    this.capturarToken(nom, val, tip, fil, col);
                 }
                break;
             
                 
             default:
                System.out.println("!Default, recorridoDeclaraciones");
                break;  
            
            
        }
    }
    
    private void capturarToken(String nom,Object val, String tip, String fil, String col){        
        Token token = new Token();
        token.nametoken = nom;
        token.valor = val;
        token.actualizarTipo(tip);
        token.linea=(Integer.parseInt(fil));
        token.columna=(Integer.parseInt(col));
        token.esterminal=true;
        pilaentrada.addLast(token);
        Compilador.acciones += "Se agrego el token de entreda:  " + token.nametoken + " Con valor:  " + token.valor + "\n";
    }
    
    int contador; 
    String relacion="";
    public String graphviz="";
    public void graficarAst(Nodo raiz){
        relacion="";    
        contador=0; 
        graphviz="";
        graphviz+="\ndigraph G {\r\nnode [shape=doublecircle, style=filled, color=khaki1, fontcolor=black];\n";
        lNodos(raiz);
        eNodos(raiz);
        graphviz+="}";
        
        
        FileWriter archivo = null;
        @SuppressWarnings("UnusedAssignment")
        PrintWriter printwriter =null;
        try {    
            archivo = new FileWriter("C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\astentrada.dot");
            printwriter = new PrintWriter(archivo);    
            printwriter.println(graphviz);         
            printwriter.close();
            } catch (IOException ex) {
            }
        
        try{
            String dotPath="C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
            String fileInputPath ="C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\astentrada.dot";
            String fileOutputPath="C:\\Users\\LuisAroldo\\Documents\\NetBeansProjects\\Proyecto1C2\\graficas\\astentrada.jpg";

            String tParam = "-Tjpg";
            String tOParam = "-o";
        
            String[] cmd = new String[5];
            cmd[0] = dotPath;
            cmd[1] = tParam;
            cmd[2] = fileInputPath;
            cmd[3] = tOParam;
            cmd[4] = fileOutputPath;
                  
            Runtime rt = Runtime.getRuntime();
      
            rt.exec( cmd );
      
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
            }
    
    }
    
    public void lNodos(Nodo praiz){        
        graphviz += "node" + contador + "[label=\"" + praiz.name + "\"];\n";
        praiz.id = contador;  
        contador++;        
        for(Nodo temp:praiz.hijos){
            lNodos(temp);
        }
    } 
    
    public void eNodos(Nodo praiz){        
        for(Nodo temp:praiz.hijos){
            relacion = "\"node" + praiz.id + "\"->";
            relacion += "\"node" + temp.id + "\";";
            graphviz += relacion+"\n";
            eNodos(temp);
        }
    }
    
    //</editor-fold>
     
    //<editor-fold desc="Parte A: Llenar pila de Tokens">
    
    private void Parte_B_Tokens(String entrada){
        this.pilaentrada=null;
        if(entrada==null){
            JOptionPane.showMessageDialog(null,"No hay tokens","FiSintax",1);
            return;
        }
        if(entrada.contains("[:@vacio@:]") || entrada.length()==0){
            JOptionPane.showMessageDialog(null,"No hay tokens","FiSintax",1);
            return;
        } 
        
        pilaentrada = new LinkedList<Token>();
        
        
        
        
        String[] c1 = entrada.split("\\[|]");
        String[] c2 = c1[1].split("\\{|}");
        List<String> lineas = new ArrayList<String>();
        for(int i=0; i<c2.length; i++)
        {
            if(i%2!=0)
            {
                lineas.add(c2[i].replace(" ",""));
            }
        }
        String[] atributos, atributo;
        String auxAtributo;
        for (String linea : lineas) 
        {
            atributos = linea.split(",");
            Token token = new Token();
            for(int j=0; j<atributos.length; j++)
            {
                auxAtributo = atributos[j];
                atributo = auxAtributo.split(":");
                atributo[1] = atributo[1].replace(",","");
                switch(j)
                {
                    case 0:
                        token.nametoken =atributo[1];
                        break;
                    case 1:
                        String v1 =atributo[1].replace("@#cd1", "\"");
                        v1 = v1.replace("@#alla1", "{");
                        v1 = v1.replace("@#clla2", "}");
                        v1 = v1.replace("@#acor1", "[");
                        v1 = v1.replace("@#ccor2", "]");
                        v1 = v1.replace("@#coma1", ",");
                        v1 = v1.replace("@#dosp1", ":");
                        v1 = v1.replace("@#esp1", " ");
                        v1 = v1.replace("\\n", "\n");
                        v1 = v1.replace("\\t", "\t");
                        v1 = v1.replace("''", "\"");
                        token.valor=v1;
                        break;
                    case 2:
                        token.actualizarTipo(atributo[1]);
                        break;
                    case 3:
                        token.linea=(Integer.parseInt(atributo[1]));
                        break;
                    case 4: 
                        token.columna=(Integer.parseInt(atributo[1]));
                        break;
                }
            }
            token.esterminal=true;
            pilaentrada.addLast(token);
        }
        // agregar el simbolo de fin
        Token tokenfin = new Token();
        tokenfin.convertir_a_fin();
        pilaentrada.addLast(tokenfin);
        System.out.println("! ============================================================ Tokens llenados");
        //System.out.println(tokens.size());     
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Parte B: Moverse entre la pila">
            
    //public String entrada;
    // Pila de Lexemas -> Tokens
    private LinkedList<Token> pilaentrada;
    // Pila de Analisis
    private LinkedList<Opila> pilapila;
    // Para el Arbol de Analisis Sintactico
    public NodoS nodo_raiz;
    private LinkedList<NodoS> listaHojas;
    private LinkedList<Token> listaTokens;
        
    private void Parte_C_Analisis()
    {
        //Inicias las esctructuras del arbol
        this.listaHojas = new LinkedList<NodoS>();
        this.listaTokens = new LinkedList<Token>();
        
        //iniciar la pila
        this.pilapila = new LinkedList<Opila>();
        
        //crear el estado0
        Opila estado0 = new Opila(0);
        
        //establecer S0 en el tope de la pila
        this.pilapila.addFirst(estado0);
        
        while(true){
            //obtener el elemento en el tope de la pila tokens
            Token t_tope = this.pilaentrada.getFirst();
            
            //Obtener el numero de estado en el tope de la pila de analisis
            int iestadoactual= this.pilapila.getFirst().iestado;
            
            //Obtener estado segun el numero de esado en el tope de la pila de analis
            EstadoTAS estado_actual = this.estadosTAS.get(iestadoactual);
            
            //Obtener la accion a realizar
            int iaccion = this.obtenerAccion(estado_actual, t_tope);
            
            //ver que accion hacer
            if(iaccion==1){     //Desplazar            
                this.accionDesplazar(estado_actual, t_tope);
            }else if(iaccion==2){ //Reducir            
                this.accionReducir(estado_actual, t_tope);
            }else if(iaccion==3){ //aceptar                        
            
                if(this.pilaentrada.size()>1){
                    String descri="( '_')¡=* Warning Analisis Abortado: "+t_tope.nametoken+", Cadena Aceptada y ";
                    descri+="aun hay tokens";                    
                    this.guardarError(t_tope.valor.toString(),t_tope.linea,t_tope.columna,"Ejecucion",descri);
                  //  this.print2("["+t_tope.linea+":"+t_tope.columna+"] -> "+descri);
                    JOptionPane.showMessageDialog(null,"Analisis Sintactico Aceptado-Abortado","Generador",1);
                    this.aceptado=false;
                }else{
                    //Aqui debo poner un mensaje de mostrar el arbol de analisis Sintactico                    
                    JOptionPane.showMessageDialog(null,"Analisis Sintactico Terminado","Generador",1);
                    nodo_raiz = new NodoS(this.inicial,0,0);
                    nodo_raiz.hijos.add(this.listaHojas.getFirst());
                    Compilador g = new Compilador();
                    g.graficarAST2(nodo_raiz);
                    this.aceptado=true;
                }
                return;
            }else if(iaccion==4){ //error
            
                JOptionPane.showMessageDialog(null,"Analisis Sintactico Abortado -> Error (4)","Generador",1);
                String descri="( '_')¡=* Error Sintactico: "+t_tope.nametoken+", token no existe en estado"+iestadoactual;
//                this.print2("["+t_tope.linea+":"+t_tope.columna+"] -> "+descri);
                this.guardarError(t_tope.nametoken.toString(),t_tope.linea,t_tope.columna,"Ejecucion",descri);
                return;
                //Aqui seria el tratamiento de errores
                //Quitar de la pila hasta encontrar uno nuevo
            }else{
                JOptionPane.showMessageDialog(null,"Analisis Sintactico Abortado -> Error (Otro)","Generador",1);
                return;
            }
        }
        
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Desplazar">
    
    private void accionDesplazar(EstadoTAS estado_actual, Token token){
        if(token.esterminal==true || listaTerminales.containsKey(token.nametoken.toLowerCase()))
        {this.listaTokens.addFirst(token);}
        
        //Meter token en Pila de Analisis
        Opila opilat = new Opila(token);
        this.pilapila.addFirst(opilat);
        
        //Sacar token de pila de Entrada
        this.pilaentrada.removeFirst();
        
        //Meter el Estado en la Pila de Analisis        
            //obtener la accion del estado = estadofuturo
        ShiftTAS shift= celda.desplazas.get(0);
        Opila opilae = new Opila(shift.numero_estado_proximo);
        this.pilapila.addFirst(opilae);
        
        String saccion="Shift: Estado: "+estado_actual.id;
        saccion+=", Token: "+token.nametoken;
        if(token.valor!=null){
            saccion+=", Valor: "+token.valor;
        }
        saccion+=", D"+opilae.iestado;
        //this.print2("["+token.linea+":"+token.columna+"] -> "+saccion);
        
        if(Compilador.tablas){
            int fila = estado_actual.id;
            int columna = celda.columna;
            this.rtas.setTabla2(dtm, fila, columna);
        }
        
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Reducir">
    
   private void accionReducir(EstadoTAS estado_actual, Token token){
        int ireduces = celda.reduces.size();
        int numero_reduccion=0;
        //si hay mas de 1 reduccion, buscar la primera que aparesca        
        if(ireduces>1){
            String descri="( '_')¡=* Warning ReduceReduce: "+token.nametoken+",  ";
            //buscar la regla con menor numero
            for(ReduceTAS red:celda.reduces){
                if(red.numero_reduccion<numero_reduccion){
                    numero_reduccion=red.numero_reduccion;
                    descri+=", R"+red.numero_reduccion;
                }
            }
        }else{
            numero_reduccion=celda.reduces.get(0).numero_reduccion;
        }
        
        //***** area de reducir :D
        //R0: S-> A + B
            //R0: indica que 0 es el numero de regla
            //se quitan 6 simbolos (token y estado)
            //se agrega un token con S a la pila entrada
        
        //buscar la regla con numero;
        Regla regla = this.listReglas.get(numero_reduccion);
        
        //valuar la cantidad de simbolos a sacar
        //por 2, formato, simbolo+estadofuturo, guadado en pilapila
        int simbolos_de_salida = regla.simbolos.size()*2;
        
        //con un for quitar simbolos
        for (int i = 0; i < simbolos_de_salida; i++) {
            this.pilapila.removeFirst();
        }
        
        //crear un nuevo token y agregarlos a la entrada
        Token tokenNT = new Token();
        tokenNT.convertir_a_nt(regla.sizquierda, token.linea, token.columna, regla.ntasociado.tipo);
        this.pilaentrada.addFirst(tokenNT);
        
        String saccion="Reduce: Estado: "+estado_actual.id;
        saccion+=", Token: "+token.nametoken;
        if(token.valor!=null){
            saccion+=", Valor: "+token.valor;
        }
        saccion+=", R"+numero_reduccion;
//        this.print2("["+token.linea+":"+token.columna+"] -> "+saccion);
        
        //Instanciando Listas Inversas, Arboles, Tokens
        LinkedList<NodoS> listaInversa = new LinkedList<NodoS>();
        LinkedList<Token> listaInversaT = new LinkedList<Token>();
        
        //Construccion del Arbol
        NodoS nodo = new NodoS(regla.sizquierda, token.linea,token.columna);
        nodo.itipo = regla.ntasociado.tipo;
        
        for(Simbolo simbolo:regla.simbolos){
            if(this.listaNterminales.containsKey(simbolo.name.toLowerCase())){
                //es un no terminal, //agregar a la lista inversa
                listaInversa.addFirst(this.listaHojas.getFirst());
                this.listaHojas.removeFirst();
            }else{
                listaInversaT.addFirst(this.listaTokens.getFirst());
                this.listaTokens.removeFirst();
            }
        }
        
        //ahora llenar tokens :D
        for(Simbolo simbolo:regla.simbolos){
            if(this.listaTerminales.containsKey(simbolo.name.toLowerCase())){
                //Obteteniendo el valor que tendra el nodo hoja
                Token token_tope = listaInversaT.getFirst();
                //Obteniendo el tipo de valor defaul que tendra
                Terminal termi = this.listaTerminales.get(simbolo.name.toLowerCase());
                
                //es un terminal
                NodoS nodoterminal = new NodoS(simbolo.name.toLowerCase(),token.linea,token.columna);
                nodoterminal.esterminal=true;
                nodoterminal.accion=simbolo.accion;
                nodoterminal.accionPRIME=simbolo.accionprime;
                nodoterminal.valor=token_tope.valor;
                    //xxxxxx validar que los tipos sean correctos (termi.tipo y token_tope.tipo)
                nodoterminal.itipo=termi.tipo;
                nodoterminal.actualizarTipo();
                
                //agregarlos al nodo
                nodo.hijos.add(nodoterminal);                                              
                                
                listaInversaT.removeFirst();                
            }else{
                NodoS nodo_tn = listaInversa.getFirst();
                //agregar la accion
                nodo_tn.accion=simbolo.accion;
                nodo_tn.accionPRIME=simbolo.accionprime;
                //agregarlo al nodo
                nodo.hijos.add(nodo_tn);
                listaInversa.removeFirst();
            }
        }
        //agregar nodo a la lista hoja
        this.listaHojas.addFirst(nodo);
        
        if(Compilador.tablas){
            int fila = estado_actual.id;
            int columna = celda.columna;
            this.rtas.setTabla2(dtm, fila, columna);
        }
    }
    
    
    //</editor-fold>
    
        
    //<editor-fold desc="Busca, Analiza que accion tomar">
    
    private CeldaTAS celda;
    private int obtenerAccion(EstadoTAS estado, Token token)
    {
        //Validar que exista una celda del estado con el token encontrado 
        if(estado.celdas.containsKey(token.nametoken.toLowerCase())==false)
        {
            return 4;
        }
        int desplazas = 0;//celda.desplazas.size();
        int reduces = 0;//celda.reduces.size();
        try 
        {
            celda = estado.celdas.get(token.nametoken);
            desplazas = celda.desplazas.size();
            reduces = celda.reduces.size();
        }
        catch(Exception ex)
        {
            ////Error, simbolo no existe en la gramatica :D
            String descri="( '_')¡=* Warning Analisis Abortado: "+token.nametoken+"Simbolo no existe";
            this.guardarError(token.valor.toString(),token.linea,token.columna,"Ejecucion",descri);
            this.print2("["+token.linea+":"+token.columna+"] -> "+descri);
            return 4;
        }
        
        //Desplazamientos
        if(desplazas ==1 && reduces==0){return 1;}
        //Reducciones
        if(desplazas==0 && reduces ==1)
        {
            int numero_reduccion=celda.reduces.get(0).numero_reduccion;
            //Si la reducion es 0, acceptar = return 3
            if(numero_reduccion==0){return 3;}
            return 2;
        }
        //Multiples reducciones
        if(desplazas==0 && reduces>1)
        {
            int numero_reduccion=0;
            for(ReduceTAS red:celda.reduces)
            {
                if(red.numero_reduccion<numero_reduccion)
                {
                    numero_reduccion=red.numero_reduccion;
                }
            }
            if(numero_reduccion==0)
            {return 3;}
            String descri="［( '_')¡=* Warning ReduceReduce: "+token.nametoken+", Multiples Reducciones";
            this.print2("["+token.linea+":"+token.columna+"] -> "+descri);            
            return 2;
        }
        //Desplazamientos vs Reduccion
        if(desplazas==1 && reduces==1)
        {
            //se analiza precedencia
                //obtener la precendia del token
                //precedencia guardada de mayor a menor
            
            int preT=1; int preR=1;
            
            //public HashMap<String, Integer> listaPrecedencia;
            //como se guadar de mayor a menor, lo multiplico * -1
            //el que esta con numoero mayor guardado ahora sera el menor :D
            //si tenia precendia 4, es decir que 1,2,3 es mayor
            //al final queda -4, y -1,-2-3 son mayores :D
            if(this.listaPrecedencia.containsKey(token.nametoken.toLowerCase()))
            {               
                preT = this.listaPrecedencia.get(token.nametoken.toLowerCase())*(-1);
            }
            
            //para la regla, obtener la regla
                //obener el id de reduccion
                //como solo hay una reduccion index=0
            
            ReduceTAS reduccion =celda.reduces.get(0);
            Regla regla = this.listReglas.get(reduccion.numero_reduccion);
            
            //recorrer con un for hasta encontar el primer simbolo
            for(Simbolo simbolo_t:regla.simbolos)
            {
                if(simbolo_t.es_terminal==true)
                {
                    //obtener la precedencia de ese simbolo
                    if(this.listaPrecedencia.containsKey(simbolo_t.name.toLowerCase()))
                    {
                        preR=this.listaPrecedencia.get(simbolo_t.name.toLowerCase())*(-1);
                    }
                    break;
                }
            }
            //Si son iguales mirar asociatividad
            if(preT==preR)
            {
                //0=no tiene, 1=izq, 2=der
                int asocT=this.obtenerAsociatividad(token.nametoken.toLowerCase());
                switch (asocT) 
                {
                    case 1: return 2;
                    case 2: return 1;
                    default:
                        //error
                        String descri="( '_')¡=* Warning ShiftReduce: "+token.nametoken+", token no asociativo";
                        this.print2("["+token.linea+":"+token.columna+"] -> "+descri);
                        return 1;                        
                }
            }
            //si pre token mayor que precedencia regla, desplazar
            if(preT>preR){return 1;}
            //si pre token menor que precendcia regla, reducir
            if(preT<preR){return 2;}
            //por defecto desplaza
            return 1;
        }
        //Un Error :D
        return 4;
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Otros">
    
    private void guardarError(String t, int l, int c, String ti, String d){
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        Compilador.listaErrores.add(new Errores(t,l,c,ti,d));
    }            
    
    private int obtenerAsociatividad(String nametoken){
        if(this.listaIzq.containsKey(nametoken)){return 1;}
        if(this.listaDer.containsKey(nametoken)){return 2;}
        return 0;
    }
    
    private void print2(String stexto){
        String anterior = this.txtcero.getText();
        this.txtcero.setText(anterior+stexto+"\n");
    }
    
    //</editor-fold>
    
}
