/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

import Ejecucion.Errores;
import Ejecucion.Nodo;
import java.util.ArrayList;
import java.util.HashMap;
import proyecto1c2.Compilador;

/**
 *
 * @author LuisAroldo
 */
public class NodoS {
    
     //<editor-fold desc="Atributos de la clase">
    
    public String name, stipo;    
    public int id, linea, columna, itipo;
    public Boolean esterminal;
    
    public Nodo accion, accionPRIME;
    //public Nodo accion;
    public Object valor;
        
    public ArrayList<NodoS> hijos;
    public HashMap<String,Atributo> atributos;
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Constructores">
    
    public NodoS(String pvalor, int plin, int pcol)
    {
        this.name=pvalor;//.replace("\"", "").replace("'","");
        this.stipo="null";
        this.hijos=new ArrayList<>();
        this.linea=plin;
        this.columna=pcol; 
        this.esterminal=false;
        this.atributos=new HashMap<String,Atributo>();        
    }
    
    
    //</editor-fold>
    
    
    //<editor-fold desc="Ejecucion, Acciones">
    
    public void setPadreValor(Object ovalor)
    {
        if(this.correcto(itipo, ovalor))
        {
            if(this.itipo==1){this.valor=this.getInt(ovalor);}
            else if(this.itipo==2){this.valor=this.getDouble(ovalor);}
            else if(this.itipo==3){this.valor=ovalor.toString();}
            else if(this.itipo==4){this.valor=this.getChar(ovalor);}
            else if(this.itipo==5){this.valor =this.getBool(ovalor);} 
            return;
        }
        String descri = "Tipo de datos diferentes: Simbolo: "+this.stipo+" x Valor: "+this.stipoObjeto(ovalor);
        this.guardarError(this.name,this.linea,this.columna, "Ejecucion",descri);
    }
    
    public void setPadreAtributo(String nombre_atributo, Object ovalor)
    {
        nombre_atributo = nombre_atributo.toLowerCase();
        Atributo aaa = this.atributos.get(nombre_atributo);
        if(aaa==null)
        {
            Atributo nuevo = new Atributo();
            nuevo.id=nombre_atributo;
            nuevo.valor=ovalor;
            this.atributos.put(nuevo.id, nuevo);
            return;
        }        
        aaa.valor = ovalor;            
    }
    
    public void setHijoValor(int index_hijo, Object ovalor)
    {
        //restarle 1 al indice
        if(index_hijo>this.hijos.size())
        {
            String descri = "No hay simbolo con index = "+index_hijo;
            this.guardarError("$"+index_hijo,this.linea, this.columna, "Ejecucion", descri);
            return;
        }        
        NodoS hijo = this.hijos.get(index_hijo-1);        
        if(this.correcto(hijo.itipo, ovalor))
        {
            if(hijo.itipo==1){hijo.valor=hijo.getInt(ovalor);}
            else if(hijo.itipo==2){hijo.valor=hijo.getDouble(ovalor);}
            else if(hijo.itipo==3){hijo.valor=ovalor.toString();}
            else if(hijo.itipo==4){hijo.valor=hijo.getChar(ovalor);}
            else if(hijo.itipo==5){hijo.valor =hijo.getBool(ovalor);} 
            return;
        }
        String descri = "Tipo de datos diferentes: Simbolo: "+hijo.stipo+" x Valor: "+this.stipoObjeto(ovalor);
        this.guardarError(hijo.name, hijo.linea, hijo.columna, "Ejecucion", descri);
    }
    
    public void setHijoAtributo(int index_hijo, String nombre_atributo, Object ovalor)
    {
        nombre_atributo = nombre_atributo.toLowerCase();
        //restaler 1 al indice
        if(index_hijo>this.hijos.size())
        {
            String descri = "No hay atributo con index = "+index_hijo;
            this.guardarError("$"+nombre_atributo,this.linea,this.columna, "Ejecucion",descri);
            return;
        }
        
        NodoS hijo = this.hijos.get(index_hijo-1);
        
        Atributo atributo = hijo.atributos.get(nombre_atributo);
        if(atributo==null)
        {
            Atributo nuevo = new Atributo();
            nuevo.id=nombre_atributo;
            nuevo.valor=ovalor;
            hijo.atributos.put(nuevo.id, nuevo);
            //this.atributos.put(nuevo.id, nuevo);
            return;
        }
        atributo.valor = ovalor;
    }
    
    public Object getValPadre()
    {
        if(this.valor!=null)
        {
            return this.valor;
        }
        else
        {
            String descri = "Elemento sin valor";
            this.guardarError("$$",this.linea,this.columna, "Ejecucion",descri);
        }               
        return 1;
    }
    
    public Object getValPadreAtributos(String satributo)
    {
        Atributo atributo = this.atributos.get(satributo.toLowerCase());
        if(atributo!=null)
        {
            if(atributo.valor!=null)
            {
                return atributo.valor;
            }
            String descri = "Atributo, sin valor asignado";
            this.guardarError("$$"+satributo, this.linea, this.columna, "Ejecucion", descri);
            return 1;
        }
        else
        {
            String descri = "Atributo no existe, sin valor asignado (Padre)";
            this.guardarError("$$."+satributo,this.linea,this.columna, "Ejecucion",descri);            
        }                
        return 1;
    }
    
    public Object getValHijo(int index)
    {
        //restaler 1 al indice
        if(index>this.hijos.size())
        {
            String descri = "No hay atributo con index = "+index;
            this.guardarError("$"+index,this.linea,this.columna, "Ejecucion",descri);            
        }

        NodoS hijo = this.hijos.get(index-1);

        if(hijo.valor!=null)
        {
            return hijo.valor;
        }
        String descri = "No hay valor";
        this.guardarError("$"+index,this.linea,this.columna, "Ejecucion",descri);        
        return 1;
    }
    
    public Object getValHijoAtributos(int index, String satributo)
    {
        //restaler 1 al indice
        if(index>this.hijos.size())
        {
            String descri = "No hay atributo con index = "+index;
            this.guardarError("$"+satributo,this.linea,this.columna, "Ejecucion",descri);
        }

        NodoS hijo = this.hijos.get(index-1);

        Atributo atributo = hijo.atributos.get(satributo.toLowerCase());
        if(atributo!=null)
        {
            if(atributo.valor!=null)
            {
                return atributo.valor;
            }
            String descri = "Atributo, sin valor asignado";
            this.guardarError("$"+index+"."+satributo,this.linea,this.columna, "Ejecucion",descri);
            return 1;
        }
        else
        {
            String descri = "Atributo no existe, sin valor asignado (Hijo)";
            this.guardarError("$"+index+"."+satributo,this.linea,this.columna, "Ejecucion",descri);            
        }        
        return 1;
    }
    
    
    //</editor-fold>

    
    //<editor-fold desc="Otros">
    
    private void guardarError(String t, int l, int c, String ti, String d)
    {
        System.out.println("Simbol: "+t+", ["+l+":"+c +"], Tipo: "+ti+", Desc: "+d);
        Compilador.listaErrores.add(new Errores(t,l,c,ti,d));
    }
    
    public void actualizarTipo()
    {
        switch (this.itipo) 
        {
            case 1:
                this.stipo = "int";
                break;
            case 2:    
                this.stipo = "double";
                break;
            case 3:
                this.stipo = "string";
                break;
            case 4:
                this.stipo = "char";
                break;
            case 5:
                this.stipo = "bool";
                break;
            default:
                this.stipo = "null";
                this.itipo = 0;
                break;
        }
    }
    
    public boolean correcto(int tipo_variable, Object ob)
    {
        int tipo_objeto=this.tipoObjeto(ob);
        
        switch (tipo_variable) 
        {
            case 1:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int default
                    case 4: return true;    //char pero en modo ascii
                    case 5: return true;    //booleano -> logico
                    default:    break;
                }break;
            case 2:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int
                    case 2: return true;    //double default
                    default:    break;
                }break;
            case 3:
                if(tipo_objeto==3){return true;}    //string default
                break;
            case 4:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int, de ascii a char
                    case 4: return true;    //char default
                    default:    break;
                }break;
            case 5:
                switch (tipo_objeto) 
                {
                    case 1: return true;    //int
                    case 5: return true;    //bool default
                    default:    break;
                }break;
            default:    break;
        }
        return false;
    } 

    private int getInt(Object ob)
    {
       switch (tipoObjeto(ob))
       {
           case 1:
               return (int)ob;
           case 4:
               return toAscii((char)ob);
           case 5:
               if ((boolean)ob)
               {
                   return 1;
               }
               else
               {
                   return 0;
               }                    
           default:
               System.out.println("Error get INT");
               return 1;
       }
    }
    
    private double getDouble(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 1:
                return Double.parseDouble(ob.toString());
            case 2:
                return (double)ob;
            default:
                System.out.println("Error get Double");
                return 1.0;
        }
    }
    
    private char getChar(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 1:
                return toCaracter((int)ob);
            case 4:
                return (char)ob;
            case 5:
                if ((boolean)ob)
                {
                    return toCaracter(1);
                }
                else
                {
                    return toCaracter(0);
                }
            default:
                System.out.println("Error get Char");
                return 'c';
        }
    }
    
    private char toCaracter(int c)
    {
        return (char)c;
    }

    private int toAscii(   char c)
    {
        int ascii = (int)c;
        return ascii; 
    }
    
    private Boolean getBool(Object ob)
    {
        switch (tipoObjeto(ob))
        {
            case 5:
                return (boolean)ob;
            case 1:
                if ((int)ob == 1)
                {
                    return true;
                }
                else if ((int)ob == 0)
                {
                    return false;
                }
                else
                {
                    String descri = "1, 0 pueden ser logico y bool";
                    Errores eee = new Errores(ob.toString(),0,0, "Ejecucion",descri);
                    Compilador.listaErrores.add(eee);
                    
                }
                return false;
            default: return false;
        }
    }
    
    private int toLogico(Object ob)
    {
        if(this.tipoObjeto(ob)==5)
        {
            if((Boolean)ob== true)
            {
                return 1;
            }
            else if((Boolean)ob==false)
            {
                return 0;
            }
        }        
        return 0;
    }  

    private String stipoObjeto(Object ob)
    {
        if(ob instanceof Integer)
        {return "int";}
        else if(ob instanceof Double)
        {return "double";}
        else if(ob instanceof String)
        {return "string";}
        else if(ob instanceof Character)
        {return "char";}
        else if(ob instanceof Boolean)
        {return "bool";}
        return "null";
    }    
        
    private int tipoObjeto(Object ob)
    {
        if(ob instanceof Integer)
        {return 1;}
        else if(ob instanceof Double)
        {return 2;}
        else if(ob instanceof String)
        {return 3;}
        else if(ob instanceof Character)
        {return 4;}
        else if(ob instanceof Boolean)
        {return 5;}
        return 0;
    }    
    
//    public String stipoObjeto(int itipo)
//    {
//        switch (itipo) 
//        {
//            case 1: return "int";
//            case 2: return "double";
//            case 3: return "string";
//            case 4: return "char";
//            case 5: return "bool";
//            default:    return "null";
//        }
//    }
        
    
    
    
    //</editor-fold>
}
