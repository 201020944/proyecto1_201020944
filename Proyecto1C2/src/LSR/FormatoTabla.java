package LSR;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Joaquin
 */
public class FormatoTabla extends DefaultTableCellRenderer
{
    private int columna;
    private int fila;

    public FormatoTabla(int pfila, int pcolumna)
    {
        this.fila =pfila;
        this.columna = pcolumna;
        System.out.println(fila+"   "+columna);
    }

    @Override
    public Component getTableCellRendererComponent ( JTable table, Object value, boolean selected, boolean focused, int row, int column )
    {        
        setBackground(Color.WHITE);//color de fondo
        table.setForeground(Color.BLACK);//color de texto
              
        //System.out.println(row+" "+column);
        
        if(table.getValueAt(row,columna).equals(false))
        {
            setBackground(Color.RED);
        }
        
        if(this.fila==row&&this.columna==column)
        {
            System.out.println(row+" "+column);
            setBackground(Color.RED);
        }

        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        return this;
    }

}