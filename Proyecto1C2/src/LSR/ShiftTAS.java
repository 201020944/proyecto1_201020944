/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

/**
 *
 * @author LuisAroldo
 */
public class ShiftTAS {
    
    public int numero_estado_fuente;
    public int numero_estado_proximo;
    public String simbolo_transicion;
    
    public ShiftTAS(int fuente, int proximo, String sim)
    {
        this.numero_estado_fuente=fuente;
        this.numero_estado_proximo=proximo;
        this.simbolo_transicion=sim.toLowerCase();
    }
    
}
