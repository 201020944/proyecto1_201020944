/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LSR;

import Ejecucion.LookaHead;
import Ejecucion.Primeros;
import Ejecucion.Regla;
import Ejecucion.Simbolo;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author LuisAroldo
 */
public class Estado {
    
    public Integer id;
    public ArrayList<Regla> listaReglas;
    public HashMap<String, String> listaExtendidas;
    
    public HashMap<String, String> buscandoPrimeros;
    public HashMap<String, Primeros> listaPrimeros;
    
    public HashMap<String, String> buscandoSiguientes;
    public HashMap<String, LookaHead> listaSiguientes;
    
    public Desplazamientos desplazamientos;
    public Reducciones reducciones;
    public Boolean union;
    
    
    // <editor-fold desc="Constructores">
    
    public Estado(){
        this.id=-1;
        this.listaExtendidas = new HashMap<String, String>();
        this.listaReglas= new ArrayList<Regla>();
        this.desplazamientos=new Desplazamientos();
        this.union=false;
    }
    
    public void iniciaHashes_Cerradura(Integer pid){
        this.id=pid;
        if(this.listaExtendidas==null){
            this.listaExtendidas = new HashMap<String, String>();
        }
        if(this.listaReglas==null){
            this.listaReglas= new ArrayList<Regla>();
        }
        if(this.desplazamientos==null){
            this.desplazamientos=new Desplazamientos();
        }        
    }
    
    public void iniciaHashes_Primeros(){
        if(this.buscandoPrimeros==null){
            this.buscandoPrimeros= new HashMap<String, String>();
        }
        if(this.listaPrimeros==null){
            this.listaPrimeros = new HashMap<String, Primeros>();
        }        
    }
    
    public void iniciaHashes_LH(){        
        if(this.buscandoSiguientes==null){
            this.buscandoSiguientes= new HashMap<String, String>();
        }
        if(this.listaSiguientes==null){
            this.listaSiguientes = new HashMap<String, LookaHead>();   
        }        
    }
            
    public void guardarExtendida(String nameproduccion){
        if(this.listaExtendidas.containsKey(nameproduccion.toLowerCase())){
        }else{
            this.listaExtendidas.put(nameproduccion, nameproduccion);
        }        
    }
    
    
    // </editor-fold>
    
    
    // <editor-fold desc="Impresiones">
    
    public String getSestado(){
        String reto="Estado: "+this.id+":\n";
        
        for(Regla tempo:this.listaReglas){
            reto+=tempo.sizquierda+" ->\t";
            reto+=tempo.getSregla()+"\t\t\tkernel: "+tempo.kernel+"\n";
        }
        return reto;
    }
    
    public String getSestado_Primeros(){
        String reto="Estado: "+this.id+":\n";
        
        for(Regla tempo:this.listaReglas){
            reto+=tempo.sizquierda+" ->\t";
            reto+=tempo.getSregla_primeros(listaPrimeros)+"\t\t\tkernel: "+tempo.kernel+"\n";
        }
        return reto;
    }
    
    public String getSestado_Primeros_Siguientes(){
        String reto="Estado: "+this.id+":\n";
        
        for(Regla tempo:this.listaReglas){
            reto+=tempo.sizquierda+" ->\t";
            reto+=tempo.getSregla_Primeros_Siguientes(listaPrimeros,this.listaSiguientes);
            reto+="\t\tkernel: "+tempo.kernel+"\n";
        }
        return reto;
    }
    
   public String getSestado_Primeros_Siguientes_Reducciones(){
        String reto="Estado: "+this.id+":\n";
        
        for(Regla tempo:this.listaReglas){
            reto+=tempo.sizquierda+" ->\t";
            reto+=tempo.getSregla_Primeros_Siguientes(listaPrimeros,this.listaSiguientes);
            reto+="\t\tkernel: "+tempo.kernel;
            reto+=tempo.getSregla_Reduccion()+"\n";
        }
        return reto;
    }
    
    
    // </editor-fold>
    
    
    // <editor-fold desc="Primeros">
    
    public void calcularPrimeros(){
        this.iniciaHashes_Primeros();
        
        for(Regla regla_tempo: this.listaReglas){
            this.buscarPrimeros(regla_tempo);
        }
        
    }
    
    public void buscarPrimeros(Regla pregla){
        if(pregla.heredada==false && pregla.kernel==0){
            this.guardar_en_BuscandoPrimeros(pregla);
            this.guardar_en_PrimeroHash(pregla);            

            Simbolo simbolo_tempo = pregla.getSimbolo(0);
            if(simbolo_tempo.es_terminal){
                this.guardarPrimero(pregla, simbolo_tempo);
            }else{
                if(pregla.sizquierda.equals(simbolo_tempo.name)){
                    
                }else{                    
                    
                    if(this.buscandoPrimeros.containsKey(simbolo_tempo.name)){
                        this.copiarPrimeros(simbolo_tempo.name, pregla.sizquierda);
                    }else{
                        
                        for(Regla regla_tempo2:this.listaReglas){
                            if(regla_tempo2.sizquierda.equals(simbolo_tempo.name)){
                                this.buscarPrimeros(regla_tempo2);
                                this.copiarPrimeros(simbolo_tempo.name, pregla.sizquierda);
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    public void guardar_en_PrimeroHash(Regla pregla){
        if(this.listaPrimeros.containsKey(pregla.sizquierda)==false){
            this.listaPrimeros.put(pregla.sizquierda,new Primeros(pregla.sizquierda));
        }
    }
    
    public void guardar_en_BuscandoPrimeros(Regla pregla){
        if(this.buscandoPrimeros.containsKey(pregla.sizquierda)==false){
            this.buscandoPrimeros.put(pregla.sizquierda, pregla.sizquierda);
        }
    }
    
    public void guardarPrimero(Regla pregla, Simbolo psimbolo){
        if(this.listaPrimeros.containsKey(pregla.sizquierda)==true){
            Primeros primeros = this.listaPrimeros.get(pregla.sizquierda);
            primeros.guardarFirst(psimbolo.name);
        }
    }
    
    public void copiarPrimeros(String remi, String desti){
        Primeros remitente= this.listaPrimeros.get(remi);
        if(remitente!=null){
            Primeros destinatario = this.listaPrimeros.get(desti);
            if(destinatario!=null){
                for(String tempo:remitente.listaFirst.values()){
                    destinatario.guardarFirst(tempo);
                }
            }
        }
    }
    
    
    // </editor-fold>
    
    
    // <editor-fold desc="Look a Head">
    
    public void calcularSiguientesOriginal(){
        for(Regla regla:this.listaReglas){
            this.buscarSiguientesoriginal(regla);
        }
    }
    
    public void calcularSiguientesNuevo(){
        for(Regla regla:this.listaReglas){
            this.buscarSiguientesnuevo(regla);
        }
//        for(Regla regla:this.listaReglas){
//            this.buscarSiguientesoriginal(regla);
//        }
    }
               
    public void buscarSiguientesoriginal(Regla pregla){
        if(pregla.kernel==pregla.simbolos.size()){return;}
        
        Simbolo simbolo_quiere_siguientes = pregla.getSimbolo(pregla.kernel);
        
        if(simbolo_quiere_siguientes.es_terminal){return;}
        
        int kernel2 = pregla.kernel+1;
        
        this.guardar_en_BuscandoSiguiente(simbolo_quiere_siguientes.name);
        this.guardar_en_SiguienteHash(simbolo_quiere_siguientes.name);
                
        if(kernel2==pregla.simbolos.size()){           
            this.copiarSiguientes(pregla.sizquierda, simbolo_quiere_siguientes.name);
        }else{                        
            Simbolo simbolo_evaluar = pregla.getSimbolo(kernel2);                        
            if(simbolo_evaluar.es_terminal){
                this.guardaSiguiente(simbolo_evaluar.name, simbolo_quiere_siguientes.name);
            }else{
                Primeros primeros = this.listaPrimeros.get(simbolo_evaluar.name);
                if(primeros==null){            
                }else{
                    for(String sprimero:primeros.listaFirst.values()){
                        this.guardaSiguiente(sprimero, simbolo_quiere_siguientes.name);
                    }
                }
            }            
        }        
    }
    
    public void buscarSiguientesnuevo(Regla pregla){
        int contadorultimo=-1;
        
        for(Simbolo simbolo_quiere_siguientes:pregla.simbolos){
            contadorultimo++;
            if(simbolo_quiere_siguientes.es_terminal==true){continue;}
            
            this.guardar_en_BuscandoSiguiente(simbolo_quiere_siguientes.name);
            this.guardar_en_SiguienteHash(simbolo_quiere_siguientes.name);
        
            int kernel2 = contadorultimo+1;
        
            if(kernel2==pregla.simbolos.size()){
                this.copiarSiguientes(pregla.sizquierda, simbolo_quiere_siguientes.name);
            }else{
                Simbolo simbolo_evaluar = pregla.getSimbolo(kernel2);
                if(simbolo_evaluar.es_terminal){
                    this.guardaSiguiente(simbolo_evaluar.name, simbolo_quiere_siguientes.name);
                }else{
                    Primeros primeros = this.listaPrimeros.get(simbolo_evaluar.name);
                    if(primeros==null){                  
                    }else{
                        for(String sprimero:primeros.listaFirst.values()){
                            this.guardaSiguiente(sprimero, simbolo_quiere_siguientes.name);
                        }
                    }
                }
            }
        }
    }
    
    public void guardar_en_SiguienteHash(String nameSimbolo){
        if(this.listaSiguientes.containsKey(nameSimbolo)==false){
            this.listaSiguientes.put(nameSimbolo,new LookaHead(nameSimbolo));
        }
    }
           
    public void guardar_en_BuscandoSiguiente(String nameSimbolo){
        if(this.buscandoSiguientes.containsKey(nameSimbolo)==false){
            this.buscandoSiguientes.put(nameSimbolo, nameSimbolo);
        }
    }
    
    public void guardaSiguiente(String name_simbolo_siguiente, String name_no_terminal_asociado){
        if(this.listaSiguientes.containsKey(name_no_terminal_asociado)==true){
            LookaHead siguientes = this.listaSiguientes.get(name_no_terminal_asociado);
            siguientes.guardarFollow(name_simbolo_siguiente);
        }
    }
    
    public void copiarSiguientes(String remi, String desti){
        LookaHead remitente= this.listaSiguientes.get(remi);
        if(remitente!=null){
            LookaHead destinatario = this.listaSiguientes.get(desti);
            if(destinatario!=null){
                for(String tempo:remitente.listaFollow.values()){
                    destinatario.guardarFollow(tempo);
                }
            }
        }
    }
    
    public void copiarSiguientes(LookaHead remi, LookaHead desti){        
        if(remi!=null){
            if(desti!=null){
                for(String tempo:remi.listaFollow.values()){
                    desti.guardarFollow(tempo);
                }
            }
        }        
    }
    
    // </editor-fold>
    
    
    // <editor-fold desc="Estados, Conjuntos">
        
    public void limpiarSimilares(){
        for(Regla regla_tempo:this.listaReglas){
            regla_tempo.similar=false;
        }
    }
    
    public Boolean exitoSimilares(){
        for(Regla regla:this.listaReglas){
            if(!regla.similar){return false;}
        }
        return true;
    }
    
    
    // </editor-fold>
    
}
