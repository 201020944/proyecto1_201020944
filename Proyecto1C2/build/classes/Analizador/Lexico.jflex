/* ---==: COdigo de usuario :==---*/
package Analizador;
import java_cup.runtime.*;
import javax.swing.JOptionPane;
import Ejecucion.Errores;
import java.util.ArrayList;
%%

%{
    public static ArrayList<Errores> erroresL = new ArrayList<Errores>();
    public String temp="";
%}

%state COMENT
%state MULTI

/* ---==: Opciones y declaraciones :==---*/
%cupsym Simbolos 
%class lexico 
%cup 
%public
%line
%column
%char
%ignorecase
%class scanner
%unicode
%full


/*---==: Expresiones :==---*/

Entero = [0-9]+
Numero = ("-")?{Entero}("." {Entero})*
Letra =[a-zA-Z??]+
id = {Letra}({Letra}|{Entero}|"_")*
idchar = "'" ~ "'"
idstring = "\"" ~ "\""
idruta=([\\][\\]("_"|([A-Za-z]|[0-9][0-9]*))+)+(".")[A-Za-z]+([A-Za-z]|[0-9][0-9])*

/*---==: Estados :==---*/

%state COMENT_SIMPLE
%state COMENT_MULTI

%%
/*---==: Reglas lexicas :==---*/

//------- COMENTARIOS --------

<YYINITIAL> "/*"               {yybegin(COMENT_MULTI);  temp="";}
<COMENT_MULTI> "*/"            {yybegin(YYINITIAL); System.out.println("Reconocido: <<"+temp+">>, Comentario Multi");}
<COMENT_MULTI> .|[ \t\r\n\f]   {temp+=yytext();}

<YYINITIAL> "//"            {yybegin(COMENT_SIMPLE); temp="";}
<COMENT_SIMPLE> .|[ \t\f]   {temp+=yytext();}
<COMENT_SIMPLE> "\n"        {yybegin(YYINITIAL); System.out.println("Reconocido: <<"+temp+">>, Comentario Simple");}




//------- GENERAL -------

<YYINITIAL> ">"   { return new Symbol(Simbolos.mayq, yychar,yyline,new String(yytext()));}
<YYINITIAL> "<"   { return new Symbol(Simbolos.menq, yychar,yyline,new String(yytext()));}
<YYINITIAL> "</"  { return new Symbol(Simbolos.dimenq, yychar,yyline,new String(yytext()));}
<YYINITIAL> "{"   { return new Symbol(Simbolos.llavabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> "}"   { return new Symbol(Simbolos.llavcer, yychar,yyline,new String(yytext()));}

//------- DECLARACIONES -------

<YYINITIAL> "dec"           { return new Symbol(Simbolos.dec, yychar,yyline,new String(yytext())); }
<YYINITIAL> "terminal"      { return new Symbol(Simbolos.termi, yychar,yyline,new String(yytext())); }
<YYINITIAL> {idruta}        { return new Symbol(Simbolos.idruta, yychar,yyline,new String(yytext())); }

<YYINITIAL> "nonterminal"   { return new Symbol(Simbolos.notermi, yychar,yyline,new String(yytext())); }
<YYINITIAL> "tipo"          { return new Symbol(Simbolos.tip, yychar,yyline,new String(yytext())); }
<YYINITIAL> "lista"         { return new Symbol(Simbolos.lis, yychar,yyline,new String(yytext())); }
<YYINITIAL> "nombre"        { return new Symbol(Simbolos.nomb, yychar,yyline,new String(yytext())); }

<YYINITIAL> "precedencia"   { return new Symbol(Simbolos.prec, yychar,yyline,new String(yytext())); }

<YYINITIAL> "asociatividad" { return new Symbol(Simbolos.asociati, yychar,yyline,new String(yytext())); }
<YYINITIAL> "asociacion"    { return new Symbol(Simbolos.asociaci, yychar,yyline,new String(yytext())); }
<YYINITIAL> "izquierda"     { return new Symbol(Simbolos.izq, yychar,yyline,new String(yytext())); }
<YYINITIAL> "derecha"       { return new Symbol(Simbolos.der, yychar,yyline,new String(yytext())); }

<YYINITIAL> "inicio"        { return new Symbol(Simbolos.ini, yychar,yyline,new String(yytext())); }
<YYINITIAL> "sim"           { return new Symbol(Simbolos.sim, yychar,yyline,new String(yytext())); }

<YYINITIAL> "int"           { return new Symbol(Simbolos.intt, yychar,yyline,new String(yytext())); }
<YYINITIAL> "char"          { return new Symbol(Simbolos.charr, yychar,yyline,new String(yytext())); }
<YYINITIAL> "double"        { return new Symbol(Simbolos.doublee, yychar,yyline,new String(yytext())); }
<YYINITIAL> "string"        { return new Symbol(Simbolos.string, yychar,yyline,new String(yytext())); }
<YYINITIAL> "bool"          { return new Symbol(Simbolos.bool, yychar,yyline,new String(yytext())); }


//------- GRAMATICA -------

<YYINITIAL> "gram"    { return new Symbol(Simbolos.gram, yychar,yyline,new String(yytext())); }

<YYINITIAL> "="       { return new Symbol(Simbolos.igu, yychar,yyline,new String(yytext()));}
<YYINITIAL> ":"       { return new Symbol(Simbolos.dpun, yychar,yyline,new String(yytext()));}
<YYINITIAL> "::="     { return new Symbol(Simbolos.dpi, yychar,yyline,new String(yytext()));}
<YYINITIAL> "|"       { return new Symbol(Simbolos.pipe, yychar,yyline,new String(yytext()));}
<YYINITIAL> "<:"      { return new Symbol(Simbolos.acabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> ":>"      { return new Symbol(Simbolos.accer, yychar,yyline,new String(yytext()));}
<YYINITIAL> "["       { return new Symbol(Simbolos.corabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> "]"       { return new Symbol(Simbolos.corcer, yychar,yyline,new String(yytext()));}
<YYINITIAL> ","       { return new Symbol(Simbolos.coma, yychar,yyline,new String(yytext()));}
<YYINITIAL> "$"       { return new Symbol(Simbolos.dolar, yychar,yyline,new String(yytext()));}
<YYINITIAL> "$$"      { return new Symbol(Simbolos.ddolar, yychar,yyline,new String(yytext()));}
<YYINITIAL> "=>"      { return new Symbol(Simbolos.igm, yychar,yyline,new String(yytext()));}
<YYINITIAL> "."       { return new Symbol(Simbolos.pto, yychar,yyline,new String(yytext()));}
<YYINITIAL> "{:"      { return new Symbol(Simbolos.llpa, yychar,yyline,new String(yytext()));}
<YYINITIAL> ":}"      { return new Symbol(Simbolos.llpc, yychar,yyline,new String(yytext()));}

<YYINITIAL> "print"   { return new Symbol(Simbolos.print, yychar,yyline,new String(yytext()));}
<YYINITIAL> "if"      { return new Symbol(Simbolos.iff, yychar,yyline,new String(yytext()));}
<YYINITIAL> "elseif"  { return new Symbol(Simbolos.elseif, yychar,yyline,new String(yytext()));}
<YYINITIAL> "else"    { return new Symbol(Simbolos.elsee, yychar,yyline,new String(yytext()));}
<YYINITIAL> "switch"  { return new Symbol(Simbolos.switchh, yychar,yyline,new String(yytext()));}
<YYINITIAL> "case"    { return new Symbol(Simbolos.casee, yychar,yyline,new String(yytext()));}
<YYINITIAL> "break"   { return new Symbol(Simbolos.breakk, yychar,yyline,new String(yytext()));}
<YYINITIAL> "continue" { return new Symbol(Simbolos.continuee, yychar,yyline,new String(yytext()));}
<YYINITIAL> "default" { return new Symbol(Simbolos.defa, yychar,yyline,new String(yytext()));}
<YYINITIAL> "while"   { return new Symbol(Simbolos.whilee, yychar,yyline,new String(yytext()));}
<YYINITIAL> "do"      { return new Symbol(Simbolos.doo, yychar,yyline,new String(yytext()));}
<YYINITIAL> "repeat"  { return new Symbol(Simbolos.repeat, yychar,yyline,new String(yytext()));}
<YYINITIAL> "until"   { return new Symbol(Simbolos.until, yychar,yyline,new String(yytext()));}
<YYINITIAL> "for"     { return new Symbol(Simbolos.forr, yychar,yyline,new String(yytext()));}
<YYINITIAL> "loop"    { return new Symbol(Simbolos.loop, yychar,yyline,new String(yytext()));}
<YYINITIAL> "new"     { return new Symbol(Simbolos.neww, yychar,yyline,new String(yytext()));}


//------- CODIGO -------

<YYINITIAL> "cod"       { return new Symbol(Simbolos.cod, yychar,yyline,new String(yytext())); }
<YYINITIAL> "class"     { return new Symbol(Simbolos.cla, yychar,yyline,new String(yytext())); }
<YYINITIAL> "extends"   { return new Symbol(Simbolos.exte, yychar,yyline,new String(yytext())); }
<YYINITIAL> "import"    { return new Symbol(Simbolos.impor, yychar,yyline,new String(yytext())); }
<YYINITIAL> "void"      { return new Symbol(Simbolos.voi, yychar,yyline,new String(yytext())); }
<YYINITIAL> "return"    { return new Symbol(Simbolos.returnn, yychar,yyline,new String(yytext())); }
<YYINITIAL> "public"    { return new Symbol(Simbolos.publi, yychar,yyline,new String(yytext())); }
<YYINITIAL> "private"   { return new Symbol(Simbolos.priva, yychar,yyline,new String(yytext())); }
<YYINITIAL> "protected" { return new Symbol(Simbolos.protec, yychar,yyline,new String(yytext())); }
<YYINITIAL> "this"      { return new Symbol(Simbolos.thiss, yychar,yyline,new String(yytext())); }
<YYINITIAL> "<!override!>"  { return new Symbol(Simbolos.over, yychar,yyline,new String(yytext())); }
<YYINITIAL> "super" { return new Symbol(Simbolos.supe, yychar,yyline,new String(yytext())); }

<YYINITIAL> "parseint"    { return new Symbol(Simbolos.parseint, yychar,yyline,new String(yytext())); }
<YYINITIAL> "parsedouble" { return new Symbol(Simbolos.parsedou, yychar,yyline,new String(yytext())); }
<YYINITIAL> "inttostr"    { return new Symbol(Simbolos.inttostr, yychar,yyline,new String(yytext())); }
<YYINITIAL> "doubletostr" { return new Symbol(Simbolos.doutostr, yychar,yyline,new String(yytext())); }
<YYINITIAL> "doubletoint" { return new Symbol(Simbolos.doutoint, yychar,yyline,new String(yytext())); }



//------- ARCHIVO Y ENTRADA -------

<YYINITIAL> "tokens"        { return new Symbol(Simbolos.tokens, yychar,yyline,new String(yytext())); }
<YYINITIAL> "token"         { return new Symbol(Simbolos.token, yychar,yyline,new String(yytext())); }
<YYINITIAL> "listatoken"    { return new Symbol(Simbolos.listoken, yychar,yyline,new String(yytext())); }
<YYINITIAL> "valor"         { return new Symbol(Simbolos.valo, yychar,yyline,new String(yytext())); }
<YYINITIAL> "fila"          { return new Symbol(Simbolos.fil, yychar,yyline,new String(yytext())); }
<YYINITIAL> "columna"       { return new Symbol(Simbolos.col, yychar,yyline,new String(yytext())); }


//------- UNIVERSALES -------


<YYINITIAL> "+"        { return new Symbol(Simbolos.mas, yychar,yyline,new String(yytext()));}
<YYINITIAL> "-"        { return new Symbol(Simbolos.menos, yychar,yyline,new String(yytext()));}
<YYINITIAL> "*"        { return new Symbol(Simbolos.por, yychar,yyline,new String(yytext()));}
<YYINITIAL> "/"        { return new Symbol(Simbolos.div, yychar,yyline,new String(yytext()));}
<YYINITIAL> "^"        { return new Symbol(Simbolos.pot, yychar,yyline,new String(yytext()));}
<YYINITIAL> "++"       { return new Symbol(Simbolos.dmas, yychar,yyline,new String(yytext()));}
<YYINITIAL> "--"       { return new Symbol(Simbolos.dmenos, yychar,yyline,new String(yytext()));}
<YYINITIAL> "["        { return new Symbol(Simbolos.corabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> "]"        { return new Symbol(Simbolos.corcer, yychar,yyline,new String(yytext()));}
<YYINITIAL> "("        { return new Symbol(Simbolos.parabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> ")"        { return new Symbol(Simbolos.parcer, yychar,yyline,new String(yytext()));}
<YYINITIAL> ">="       { return new Symbol(Simbolos.mayigu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "<="       { return new Symbol(Simbolos.menigu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "=="       { return new Symbol(Simbolos.iguigu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "!="       { return new Symbol(Simbolos.noigu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "||"       { return new Symbol(Simbolos.or, yychar,yyline,new String(yytext()));}
<YYINITIAL> "??"       { return new Symbol(Simbolos.xor, yychar,yyline,new String(yytext()));}
<YYINITIAL> "&&"       { return new Symbol(Simbolos.and, yychar,yyline,new String(yytext()));}
<YYINITIAL> "!"        { return new Symbol(Simbolos.not, yychar,yyline,new String(yytext()));}
<YYINITIAL> "true"     { return new Symbol(Simbolos.verdadero, yychar,yyline,new String(yytext())); }
<YYINITIAL> "false"    { return new Symbol(Simbolos.falso, yychar,yyline,new String(yytext())); }
<YYINITIAL> {idchar}   { return new Symbol(Simbolos.idchar, yychar,yyline,new String(yytext())); }
<YYINITIAL> {idstring} { return new Symbol(Simbolos.idstring, yychar,yyline,new String(yytext())); }
<YYINITIAL> ";"        { return new Symbol(Simbolos.pcoma, yychar,yyline,new String(yytext()));}    
<YYINITIAL> ","        { return new Symbol(Simbolos.coma, yychar,yyline,new String(yytext()));}
<YYINITIAL> {Numero}   { return new Symbol(Simbolos.num, yychar,yyline,new String(yytext())); }
<YYINITIAL> {id}       { return new Symbol(Simbolos.id, yychar,yyline,new String(yytext())); }

//------- IGNORAR ESPACIOS -------
<YYINITIAL> [ \t\r\f\n]+        { /* Se ignoran */}


//------- CUALQUIER OTRO -------
<YYINITIAL> .  { JOptionPane.showMessageDialog(null, "Error lexico:  " + new String(yytext()) + " Columna: " + yychar + " Linea: " + yyline);
                 erroresL.add(new Errores(yytext(),yyline,yycolumn,"Error Lexico","Lexema Invalido"));} 